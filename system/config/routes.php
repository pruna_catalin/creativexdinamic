<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 15/02/2018
 * Time     : 3:59 PM
 */
use UMP\System\Core\Routes\Routes;


///////////////////////////START ROUTES/////////////////////////////////////////


Routes::getInstance()
	->folder("front")
	->keyRoute("game_key_get")
	->securityOff(false)
	->admin()
	->get("","IndexController@actionView","");

Routes::getInstance()
	->folder("")
	->keyRoute("login_post")
	->securityOff(false)
	->admin()
	->post("login","LoginController@actionLogin","XHR");

Routes::getInstance()
	->folder("front")
	->keyRoute("page_post")
	->securityOff(false)
	->admin()
	->get("page/{id}","PageController@actionView","");

Routes::getInstance()
	->folder("front")
	->keyRoute("page_chat")
	->securityOff(false)
	->admin()
	->get("chat","ChatController@actionView","XHR");

Routes::getInstance()
	->folder("front")
	->keyRoute("page_chat_post")
	->securityOff(false)
	->admin()
	->post("chat/message","ChatController@actionReceiveMessage","XHR");

/////////////////////////////////////ADMIN///////////////////////////////////////////
Routes::getInstance()
	->folder("admin")
	->keyRoute("user_edit")
	->admin(true)
	->get("user/edit/{id}","UserController@actionEdit","");
Routes::getInstance()
	->folder("admin")
	->keyRoute("user_list_post")
	->admin(true)
	->post("user/edit/{id}","UserController@actionEdit","");

Routes::getInstance()
	->folder("admin")
	->keyRoute("user_list")
	->admin(true)
	->get("user/list","UserController@actionList","");

Routes::getInstance()
	->folder("admin")
	->keyRoute("user_list_index")
	->securityOff(false)
	->admin(true)
	->get("index","UserController@actionList","");


/////////////////////////LOGIN STAGE/////////////////////////////////////////////
Routes::getInstance()
	->folder("front")
	->keyRoute("login")
	->securityOff(false)
	->admin()
	->post("login","IndexController@actionLogin","");

Routes::getInstance()
	->folder("")
	->keyRoute("index")
	->admin(true)
	->get("admin/login","LoginController@actionLogin","");


///////////////////////////STOP ROUTES/////////////////////////////////////////
