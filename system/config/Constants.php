<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 16/02/2018
 * Time     : 9:27 AM
 */

namespace UMP\System\Config;


class Constants
{
    const DEV_MODE                  = false;
    const APP_NAME                  = "UMP System";
    const DOMAIN_NAME               = "localhost";
    // COOKIE SETTINGS //
    const COOKIE_EXPIRE             = 0; // mean never
    const COOKIE_NAME               = "CreativeX_"; //
    const COOKIE_VALUE              = "CreativeX_Val_"; // mean never
    const COOKIE_PATH               = "/"; // mean never
    const COOKIE_DOMAIN             = "localhost"; // mean never
    const COOKIE_SECURE             = false; // mean never
    const COOKIE_HTTPONLY           = false; // mean never
    // COOKIE SETTINGS END//
    const LIVE_URL                  = "http://localhost/creativexdinamic/";
    const REDIRECT_URL              = "http://localhost/creativexdinamic/index.php/";
    const MAX_PERPAGE               = 10;
    const MAX_PSPAGE                = 5;
    const FRONT_THEME               = "company"; // name folder for THEME
    const FRONT_THEME_ASSETS        = self::LIVE_URL."system/views/assets"; // name folder for THEME
	const SYSTEM_CONTROLLER         = BASEPATH."/system/core/controller"; // name folder for THEME
	const SYSTEM_MODEL				= BASEPATH."/system/core/model"; // name folder for THEME
	const SYSTEM_FILES				= BASEPATH."/system/files"; // name folder for THEME
	const SYSTEM_VIEW				= "";
    const DEV_RENDER_SCOPE          = false;
}