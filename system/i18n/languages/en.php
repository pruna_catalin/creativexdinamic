<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 16/02/2018
 * Time     : 11:51 AM
 */

namespace UMP\System\I18N\Languages;


class en
{
    public $translate;
    public function __construct()
    {
        $this->translate = (object)array(
                                            "add_button"=>"Add",
                                            "edit_button"=>"Edit",
                                            "save_button"=>"Save",
                                            "delete_button"=>"Delete",
                                            "id_th"=>"ID",
                                            "name_th"=>"NAME",
                                            "menu_users"=>"Users",
                                            "list_users"=>"List Users",
                                            "login_failed"=>"User and password incorect",
                                            "login_success"=>"Login Success"
                                    );
    }
}