<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 7/22/2018
 * Time: 5:40 PM
 */

namespace UMP\System\I18N\Languages;


class ro
{
	public $translate;
	public function __construct()
	{
		$this->translate = (object)array(
			"add_button"=>"Adauga",
			"edit_button"=>"Editeaza",
			"save_button"=>"Salveaza",
			"delete_button"=>"Sterge",
			"id_th"=>"ID",
			"name_th"=>"NAME",
			"menu_users"=>"Users",
			"list_users"=>"List Users",
			"login_failed"=>"User and password incorect",
			"login_success"=>"Login Success"
		);
	}
}