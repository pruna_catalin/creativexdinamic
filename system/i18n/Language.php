<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 26/04/2018
 * Time     : 3:40 PM
 */
use UMP\System\I18N\Languages\en;
class Language
{
    public  function t($text){
        if(isset($_SESSION['language'])){
            $language = __NAMESPACE__."\\Languages\\".$_SESSION['language'];
            $translateFile = new $language();
            if(isset($translateFile->translate->{$text})){
                return $translateFile->translate->{$text};
            }else{
                return "Translation Not found ";
            }
        }else{
            $translateFile = new en();
            if(isset($translateFile->translate->{$text})){
                return $translateFile->translate->{$text};
            }else{
                return "Translation Not found ";
            }
        }

    }
}
