<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 13/02/2018
 * Time     : 1:18 PM
 */
class Utils extends \UMP\System\Config\Constants
{
	/**
	 * @param $location
	 * @param bool $headers
	 * @return array
	 */
	public static function download($location, $headers = false , $save_local = false){
		$content = ["filename"=>"","content"=>"", "size"=>0];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $location);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSLVERSION,6);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);
		($headers) ? curl_setopt($ch, CURLOPT_HTTPHEADER     , $headers) : "";
		curl_setopt($ch, CURLOPT_USERAGENT		, $_SERVER['HTTP_USER_AGENT']);
		$curl_response = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
		if(strlen($curl_response) > 0){
			$content['filename'] = time().".safeCX";
			$content['content'] = $curl_response;
			$content['size'] = self::formatBytes($info['size_download']);
			if($save_local) self::writeToFile(self::SYSTEM_FILES."/".$content['filename'],$curl_response);
		}
		return $content;
	}
	public static function upload($file = [],$location = [self::SYSTEM_FILES] ,$exception = [".*"]){
		$content = ["file"=>$file,"location"=>$location,"error"=>false,"success"=>true,"additional_status"=>""];
		if(sizeof($file) > 0){
			/*
			 * If we have exceptions will apply rules , return error true on faild + additional status
			 */
			if(sizeof($exception) > 0){
				$exp_list = [];
				if(!in_array(".*",$exception)){
					foreach($file as $key => $value){
						$ext = pathinfo($value['name']);
						$exp_list[$key] = $ext['extension'];
						if(!in_array($ext['extension'],$exception)){
							return ["file"=>[$key],"location"=>$location,"error"=>true,"success"=>false,"additional_status"=>"Exception was applied on this : ".$exception];
						}
					}
				}
			}
			/*
			 * Check Location is Valid
			 */
			foreach($location as $directory){
				if(!is_dir($directory)){
					return ["file"=>$file,"location"=>$location,"error"=>true,"success"=>false,"additional_status"=>"Location is not a falid directory : ".$directory];
				}
			}
			$count = 0;
			foreach ($file as $key => $value) {
				if(is_array($value)){
					if(sizeof($location) == 1){
						if (!move_uploaded_file($value['tmp_name'],$location[0]."/".basename($value['name']))) {
							return ["file"=>$file,"location"=>$location,"error"=>true,"success"=>false,"additional_status"=>"File can't upload on  : ".$location[0]."/".basename($value['name'])];
						}
					}else{
						if (!move_uploaded_file($value['tmp_name'],$location[$count]."/".basename($value['name']))) {
							return ["file"=>$file,"location"=>$location,"error"=>true,"success"=>false,"additional_status"=>"File can't upload on  : ".$location[$count]."/".basename($value['name'])];
						}
					}

					$count++;
				}else{
					return ["file"=>$file,"location"=>$location,"error"=>true,"success"=>false,"additional_status"=>"Invalid call upload function  : ".$file[$count]];
				}

			}

		}
		return $content;
	}
    public static function lastDir($path) {
        return basename(dirname($path));
    }
    public static function getDirectory($location,$required = "Controller",$extension = ["php"]){
        $directory_stack = array($location);
        $ignored_filename = array(
            '.git' => true,
            '.svn' => true,
            '.hg' => true,
            'index.php' => true,
            'Controller.php'=>true,
            'ReflectionController.php'=>true,
            'CodeController.php'=>true
        );
        $file_list = array();
        while (!empty($directory_stack)) {
            $current_directory = array_shift($directory_stack);
            $files = scandir($current_directory);
            foreach ($files as $filename) {
                //  Skip all files/directories with:
                //      - A starting '.'
                //      - A starting '_'
                //      - Ignore 'index.php' files
                $pathname = $current_directory . DIRECTORY_SEPARATOR . $filename;

                if (!empty($filename) &&
                    (   $filename[0] === '.' ||
                        $filename[0] === '_' ||
                        isset($ignored_filename[$filename]) ||
                        ( is_file($pathname) && !preg_match("/".$required."/",$filename))))
                {}
                else if (is_dir($pathname) === TRUE) {
                    $directory_stack[] = $pathname;
                } else if (in_array(pathinfo($pathname, PATHINFO_EXTENSION),$extension)) {
                    $file_list[] = $pathname;
                }
            }
        }
        return $file_list;
    }
    public static function writeToFile($file,$content,$append = false){
		$fp = fopen($file, $append ? "a+b" : "wb");
		if($fp !== FALSE){
			#fwrite($fp, pack("CCC",0xef,0xbb,0xbf));
			fwrite($fp, str_replace(" "," ",$content));
			fclose($fp);
		}
	}
	public static function readFile($file){
		$fp = fopen($file, "r");
		$content = "";
		if($fp){
			while (($line = fgets($fp)) !== false) {
				$content .= $line;
			}
			fclose($fp);
		}
		return $content;
	}
    public static function generateRandomString($length){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public static function generateRandomInteger($lenght){
        return rand(pow(10,$lenght),9 * pow(10,$lenght));
    }
    public static function debug($param) {
        echo '<pre>';
        print_r($param);
        echo '</pre>';
    }

    public static function dump($param) {
        echo '<pre>';
        print_r(var_dump($param));
        echo '</pre>';
    }

    public static function diffTime($date_end, $date_start) {
        $start = new DateTime($date_start);
        $end = new DateTime($date_end);
        $interval = date_diff($start, $end);
        return $interval->format('%a');
    }

    public static function formatBytes($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }
    public static function removeEmptyObjects($array){
        $array_return = [];
        foreach($array as $item => $value){
            if (!empty($value) || $value != "") {
                $array_return[$item] = $value;
            }
        }
        return $array_return;
    }
    public static function getBasePath($path, $root) {
        if(!is_dir($root) || $path == '/') return $path;

        $result = array_filter(explode("/", str_replace("\\", "/", $path)),
            function ($_) { return !empty($_); });
        $base = array_filter(explode("/", str_replace("\\", "/", realpath($root))),
            function ($_) { return !empty($_); });
        $result_idx = array_values($result);
        $base_idx = array_values($base);

        while(sizeof($result_idx) > 0 && sizeof($base_idx) > 0) {
            if($result_idx[0] == $base_idx[0]) { array_shift($result_idx); array_shift($base_idx); }
            else break;
        }
        return (sizeof($result_idx > 0) ? "" : "./") . implode("/", $result_idx);
    }
    public static function jsonTest($json){
        $content = false;
        if(is_string($json)){
            $content = json_decode($json);
            if($content == null) $content = false;
        }

        return $content;
    }

    public static function getIfExistsOrDefault($needle, $haystack, $default = null, $map = false) {
        if($map)
            return array_key_exists($needle, $haystack) ? $haystack[$needle] : $default;
        else
            return in_array($needle, $haystack, true) ? $needle : $default;
    }
    public static function hashPassword($string, $method = "MD5", $compare = null){
        if($string != ""){
            if($method == "MD5" && $compare != null){
                if($string == $compare) return true;
                else return false;
            }else if($method == "MD5"){
                return md5($string);
            }else if($method == "SHA1"){
                return sha1($string);
            }else if($method == "SQL"){
                $pass = strtoupper(
                    sha1(
                        sha1($string, true)
                    )
                );
                $pass = '*' . $pass;
                return $pass;
            }else{
                return false;
            }

        }
        return false;
    }
    public static function validateColumns($type,$null,$lenght,$value){
        $return  = true;
        $message = "";
        $typeOf = gettype($value);
        if($type == "int"){
            if(!is_int($value)){
                $message = 'Value must be int, '.$typeOf.' given <font color="yellow">'.$value.'</font>';
                return [false,$message];
            }
            if(!$null){
                if(is_null($value)){
                    $message = 'Value must be int, '.$typeOf.' given <font color="yellow">'.$value.'</font>';
                    return [false,$message];
                }
            }
            $max_val = intval(str_pad("1", $lenght,"0"));
            if ($max_val <= $value){
                $message = 'Lenght max be '.$max_val.', given <font color="yellow">'.$value.'</font>';
                return [false,$message];
            }
        }
        if($type  == "text"){
            if(!$null){
                if(is_null($value)){
                    $message = "Value can't  be null:".$value;
                    return [false,$message];
                }
            }
        }
        return [$return,$message];
    }
    public static function redirect($route,$secounds = 0){
        if($route != ""){
            header("refresh:".$secounds.";url= ".parent::REDIRECT_URL.$route);
            if($secounds == 0) exit;
        }
    }
    public static function getValueFromModel($model,$params){
        $valueArray =[];
        foreach($model as $key => $value){
            if(isset($params[$key]))
                $valueArray[$key] = $params[$key];
            else
                $valueArray[$key] = "";
        }
        return $valueArray;
    }
    public static function cleanString($string,$returnType){
        if($returnType == "FirstUp"){
            return ucfirst(preg_replace("/\W+|\_/", "", $string));
        }else{
            return preg_replace("/\W+|\_/", "", $string);
        }
    }
    public static function generateTabsSpace($nr = 0){
        $content = "";
        for($i=0;$i < $nr;$i++)
            $content .= "\t";
        return $content;
    }
    /**
     * self::searchArrayValueByKey($array,'key');
     * return value of find key;
     */
    public static  function searchArrayValueByKey(array $array, $search) {
        $arrayReturn = array();
        foreach (new RecursiveIteratorIterator(new RecursiveArrayIterator($array)) as $key => $value) {
            if ($search === $key)
                $arrayReturn[] = $value;
        }
        return $arrayReturn;
    }
    public static function jsonPrettyPrint( $json )
    {
        $result = '';
        $level = 0;
        $in_quotes = false;
        $in_escape = false;
        $ends_line_level = NULL;
        $json_length = strlen( $json );

        for( $i = 0; $i < $json_length; $i++ ) {
            $char = $json[$i];
            $new_line_level = NULL;
            $post = "";
            if( $ends_line_level !== NULL ) {
                $new_line_level = $ends_line_level;
                $ends_line_level = NULL;
            }
            if ( $in_escape ) {
                $in_escape = false;
            } else if( $char === '"' ) {
                $in_quotes = !$in_quotes;
            } else if( ! $in_quotes ) {
                switch( $char ) {
                    case '}':
                    case ']':
                        $level--;
                        $ends_line_level = NULL;
                        $new_line_level = $level;
                        break;

                    case '{':
                    case '[':
                        $level++;
                    case ',':
                        $ends_line_level = $level;
                        break;

                    case ':':
                        $post = " ";
                        break;

                    case " ":
                    case "\t":
                    case "\n":
                    case "\r":
                        $char = "";
                        $ends_line_level = $new_line_level;
                        $new_line_level = NULL;
                        break;
                }
            } else if ( $char === '\\' ) {
                $in_escape = true;
            }
            if( $new_line_level !== NULL ) {
                $result .= "\n".str_repeat( "\t", $new_line_level );
            }
            $result .= $char.$post;
        }

        return $result;
    }
}
