<?php

class Cx_Api_Google {

	/**
	 * This class will be loaded when someone calls the API library with the Google analytics module
	 */
	public function __construct() {
		spl_autoload_register( array( $this, 'autoload_api_google_files' ) );
	}

	/**
	 * Autoload the API Google class
	 *
	 * @param string $class_name - The class that should be loaded
	 */
	private function autoload_api_google_files( $class_name ) {
		$path        = dirname( __FILE__ );
		$class_name  = strtolower( $class_name );
		$oauth_files = array(
			// Main requires
			'Cx_google_client'          => 'google/Google_Client',
			'Cx_api_google_client'      => 'class-api-google-client',

			// Requires in classes
			'Cx_google_auth'            => 'google/auth/Google_Auth',
			'Cx_google_assertion'       => 'google/auth/Google_AssertionCredentials',
			'Cx_google_signer'          => 'google/auth/Google_Signer',
			'Cx_google_p12signer'       => 'google/auth/Google_P12Signer',
			'Cx_google_authnone'        => 'google/auth/Google_AuthNone',
			'Cx_google_oauth2'          => 'google/auth/Google_OAuth2',
			'Cx_google_verifier'        => 'google/auth/Google_Verifier',
			'Cx_google_loginticket'     => 'google/auth/Google_LoginTicket',
			'Cx_google_pemverifier'     => 'google/auth/Google_PemVerifier',
			'Cx_google_model'           => 'google/service/Google_Model',
			'Cx_google_service'         => 'google/service/Google_Service',
			'Cx_google_serviceresource' => 'google/service/Google_ServiceResource',
			'Cx_google_utils'           => 'google/service/Google_Utils',
			'Cx_google_batchrequest'    => 'google/service/Google_BatchRequest',
			'Cx_google_mediafileupload' => 'google/service/Google_MediaFileUpload',
			'Cx_google_uritemplate'     => 'google/external/URITemplateParser',
			'Cx_google_cache'           => 'google/cache/Google_Cache',

			// Requests
			'Cx_google_cacheparser'     => 'google/io/Google_CacheParser',
			'Cx_google_io'              => 'google/io/Google_IO',
			'Cx_google_httprequest'     => 'google/io/Google_HttpRequest',
			'Cx_google_rest'            => 'google/io/Google_REST',

			// Wordpress
			'Cx_google_wpio'            => 'google/io/Google_WPIO',
			'Cx_google_wpcache'         => 'google/cache/Google_WPCache',
		);

		if ( ! empty( $oauth_files[$class_name] ) ) {
			if ( file_exists( $path . '/' . $oauth_files[$class_name] . '.php' ) ) {
				require_once( $path . '/' . $oauth_files[$class_name] . '.php' );
			}

		}

	}

}