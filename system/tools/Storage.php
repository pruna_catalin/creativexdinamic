<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 20/02/2018
 * Time     : 9:44 AM
 */

namespace UMP\System\Tools;

use UMP\System\Config\Constants;
class session{
	public function getAll(){
		$seesionsArr = [];
		if(isset($_SESSION)){
			foreach($_SESSION as $session=>$val){
				$seesionsArr[$session] = $val;
			}
		}
		return $seesionsArr;
	}
    public function get($name){
        if(isset($_SESSION[$name]))
            return ["status"=>true,"name"=>$name,"value"=>$_SESSION[$name]];
        else
            return false;
    }

    public function set($params){
        if(is_array($params)){
            if(isset($params['name'])){
               $_SESSION[$params['name']] = $params['value'];
               if(isset($_SESSION[$params['name']])) return true;
               else return false;
            }
        }
    }
    public function delete($name){
        if(isset($_SESSION[$name])) {
          unset($_SESSION[$name]);
          return true;
        }else{
            return false;
        }
    }
	// flash session
	public function flashSession(){
		if(isset($_SESSION)){
			foreach($_SESSION as $session=>$val){
				unset($_SESSION[$session]);
			}
			return true;
		}
		return false;
	}
}
class cookie {
    private $_name      = Constants::COOKIE_NAME;
    private $_value     = Constants::COOKIE_VALUE;
    private $_expire    = Constants::COOKIE_EXPIRE;
    private $_path      = Constants::COOKIE_PATH;
    private $_domain    = Constants::COOKIE_DOMAIN;
    private $_isHttps   = Constants::COOKIE_SECURE;
    private $_httpOnly  = Constants::COOKIE_HTTPONLY;

    public function get($name){
        if(isset($_COOKIE[$name]))
            return ["status"=>true,"name"=>$name,"value"=>$_COOKIE[$name]];
        else
            return false;
    }
    public function set($params){
        if(is_array($params)){
            if(isset($params['name']))
                $this->_name = $params['name'];
            if(isset($params['value']))
                $this->_value = $params['value'];
            if(isset($params['expire']))
                $this->_expire = is_numeric($params['expire']) ? $params['expire'] : false;
            if(isset($params['path']))
                $this->_path = $params['path'];
            if(isset($params['domain']))
                $this->_domain = $params['domain'];
            if(isset($params['isHttps']))
                $this->_secure = is_bool($params['isHttps']) ? $params['isHttps'] : false;
            if(isset($params['httpOnly']))
                $this->_httpOnly = is_bool($params['httpOnly']) ? $params['httpOnly'] : false;
        }
        return (setcookie($this->_name,$this->_value,$this->_expire,$this->_path,$this->_domain,$this->_isHttps,$this->_httpOnly)) ?   true : false;
    }
}
class LocalFile{
	private $file = [];
	private $location = [Constants::SYSTEM_FILES];
	private $exception = [];

	public function get($file = [],$location = [],$external = false,$headers = false,$save_local = false){
		if(sizeof($file) > 0 ) $this->file = $file;
		if(sizeof($location) > 0) $this->location = $location;
		$result = ["files"=>"","content"=>"","location"=>$this->location];
		if($external){
			$info = \Utils::download($location,$headers,$save_local);
			$result =  ["files"=>$info['filename'],"content"=> $info['content'],"location"=>Constants::SYSTEM_FILES];
		}else{
			for($i = 0;$i < sizeof($this->file);$i++){
				if(sizeof($this->location) == 1 || sizeof($this->location) == 0)
					$result[] = ["files"=>$this->file[$i],"content"=>\Utils::readFile($this->location[0]."/".$this->file[$i]),"location"=>$this->location[0]];
				else
					$result[] = ["files"=>$this->file[$i],"content"=>\Utils::readFile($this->location[$i]."/".$this->file[$i]),"location"=>$this->location[$i]];
			}
		}
		return $result;
	}
	public function set($file = [], $location = [],$exception= []){
		$this->file = $file;
		$this->location = $location;
		$this->exception = $exception;
		return $this;
	}
	public function uploadFile(){
		return \Utils::upload($this->file,$this->location,$this->exception);
	}

}
class Storage
{
    public  $session  = [];
    public  $cookie   = [];
    public  $files 	  = [];
    public function __construct()
    {
        $this->session = new session();
        $this->cookie = new cookie();
        $this->files = new LocalFile();
    }

}