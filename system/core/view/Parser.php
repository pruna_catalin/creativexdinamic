<?php

class token
{
	const TKeyword_Op = 0;
	const TKeyword_Value = 1;
	const TIdentifier = 2;
	const TString = 3;
	const TNumber = 4;

	const TQuestionMark = 5;
	const TColon = 6;
	const TComma = 7;
	const TSemicolon = 8;
	const THash = 9;

	const TOpenParen = 10;
	const TCloseParen = 11;
	const TOpenBracket = 12;
	const TCloseBracket = 13;
	const TOpenBrace = 14;
	const TCloseBrace = 15;

	const TDash = 20;
	const TPlus = 21;
	const TAsterisk = 22;
	const TSlash = 23;
	const TPercent = 24;
	const TIncrement = 25;
	const TDecrement = 26;
	const TIncAssign = 27;
	const TDecAssign = 28;
	const TMulAssign = 29;
	const TDivAssign = 30;
	const TModAssign = 31;

	const TBitLS = 35;
	const TBitRS = 36;
	const TBitLSAssign = 37;
	const TBitRSAssign = 38;

	const TCmpEq = 40;
	const TCmpNEq = 41;
	const TCmpLT = 42;
	const TCmpLTEq = 43;
	const TCmpGT = 44;
	const TCmpGTEq = 45;

	const TBitAnd = 50;
	const TBitXor = 51;
	const TBitOr = 52;
	const TBitNot = 53;
	const TBitAndAssign = 54;
	const TBitOrAssign = 55;
	const TBitXorAssign = 56;

	const TAnd = 65;
	const TOr = 66;

	const TNot = 70;
	const TEqual = 71;

	const TSign = 75;
	const TValueOf = 76;
	const TValueIs = 77;
	const TGroup = 78;
	const TObjAccess = 79;
	const TArrAccess = 80;
	const TFctGlobalCall = 81;
	const TFctObjectCall = 82;
	const TFctArg = 83;

	const TIf = 85;
	const TElse = 86;
	const TFor = 87;
	const TWhile = 88;
	const TEnd = 89;

	const TPrint = 99;

	const TEndOfStream = 100;
	const TUnknown = 101;

	public $Type, $Symbol, $At;
}

class keyword
{
	const Op = ['if', 'else', 'for', 'while', 'end'];
	const Value = ['null', 'true', 'false'];
}

function IsEndOfLine($c)
{
	return $c == "\n" || $c == "\r";
}

function IsWhitespace($c)
{
	return ($c == ' ' || $c == "\t" || $c == "\v" ||
		$c == "\f" || IsEndOfLine($c));
}

function IsAlpha($c)
{
	return (ord('a') <= ord($c) && ord($c) <= ord('z')) ||
		(ord('A') <= ord($c) && ord($c) <= ord('Z')) || $c == '_';
}

function IsNumeric($c)
{
	return ord('0') <= ord($c) && ord($c) <= ord('9');
}

function EatAllWhitespace(&$input, $s = 0)
{
	$sPos = $s;
	$iLen = strlen($input);
	for (; $sPos < $iLen;) {
		if (IsWhitespace($input[$sPos])) {
			++$sPos;
		} else if ($input[$sPos] == '/' && $input[$sPos + 1] == '/') {
			$sPos += 2;
			while ($sPos < $iLen && !IsEndOfLine($input[$sPos])) {
				++$sPos;
			}
		} else if ($input[$sPos] == '/' && $input[$sPos + 1] == '*') {
			$sPos += 2;
			while ($sPos + 1 < $iLen && !($input[$sPos] == '*' && $input[$sPos + 1] == '/')) {
				++$sPos;
			}
			if ($input[$sPos] == '*' && $input[$sPos + 1] == '/') {
				$sPos += 2;
			}
		} else
			break;
	}
	return $sPos;
}

function GetToken(&$input, $s = 0)
{
	$sPos = EatAllWhitespace($input, $s);
	$iLen = strlen($input);
	$token = new token();

	if ($sPos < $iLen) {
		$token->Symbol = $input[$sPos];

		switch ($input[$sPos]) {
			case ':':
				{
					$token->Type = token::TColon;
					++$sPos;
				}
				break;
			case ',':
				{
					$token->Type = token::TComma;
					++$sPos;
				}
				break;
			case ';':
				{
					$token->Type = token::TSemicolon;
					++$sPos;
				}
				break;
			case '*':
				{
					$token->Type = token::TAsterisk;
					++$sPos;
					if ($input[$sPos] == '=') {
						$token->Symbol .= '=';
						$token->Type = token::TMulAssign;
						++$sPos;
					}
				}
				break;
			case '/':
				{
					$token->Type = token::TSlash;
					++$sPos;
					if ($input[$sPos] == '=') {
						$token->Symbol .= '=';
						$token->Type = token::TDivAssign;
						++$sPos;
					}
				}
				break;
			case '+':
				{
					$token->Type = token::TPlus;
					++$sPos;
					if ($input[$sPos] == '+') {
						$token->Symbol .= '+';
						$token->Type = token::TIncrement;
						++$sPos;
					} else if ($input[$sPos] == '=') {
						$token->Symbol .= '=';
						$token->Type = token::TIncAssign;
						++$sPos;
					}
				}
				break;
			case '-':
				{
					$token->Type = token::TDash;
					++$sPos;
					if ($input[$sPos] == '-') {
						$token->Symbol .= '-';
						$token->Type = token::TDecrement;
						++$sPos;
					} else if ($input[$sPos] == '=') {
						$token->Symbol .= '=';
						$token->Type = token::TDecAssign;
						++$sPos;
					} else if ($input[$sPos] == '>') {
						$token->Symbol .= '>';
						$token->Type = token::TObjAccess;
						++$sPos;
					}
				}
				break;
			case '&':
				{
					$token->Type = token::TBitAnd;
					++$sPos;
					if ($input[$sPos] == '=') {
						$token->Symbol .= '=';
						$token->Type = token::TBitAndAssign;
						++$sPos;
					} else if ($input[$sPos] == '&') {
						$token->Symbol .= '&';
						$token->Type = token::TAnd;
						++$sPos;
					}
				}
				break;
			case '|':
				{
					$token->Type = token::TBitOr;
					++$sPos;
					if ($input[$sPos] == '=') {
						$token->Symbol .= '=';
						$token->Type = token::TBitOrAssign;
						++$sPos;
					} else if ($input[$sPos] == '|') {
						$token->Symbol .= '|';
						$token->Type = token::TOr;
						++$sPos;
					}
				}
				break;
			case '~':
				{
					$token->Type = token::TBitNot;
					++$sPos;
				}
				break;
			case '%':
				{
					$token->Type = token::TPercent;
					++$sPos;
					if ($input[$sPos] == '=') {
						$token->Symbol .= '=';
						$token->Type = token::TModAssign;
						++$sPos;
					}
				}
				break;
			case '<':
				{
					$token->Type = token::TCmpLT;
					++$sPos;
					if ($input[$sPos] == '=') {
						$token->Symbol .= '=';
						$token->Type = token::TCmpLTEq;
						++$sPos;
					} else if ($input[$sPos] == '<') {
						$token->Symbol .= '<';
						$token->Type = token::TBitLS;
						++$sPos;
						if ($input[$sPos] == '=') {
							$token->Symbol .= '=';
							$token->Type = token::TBitLSAssign;
							++$sPos;
						}
					}
				}
				break;
			case '>':
				{
					$token->Type = token::TCmpGT;
					++$sPos;
					if ($input[$sPos] == '=') {
						$token->Symbol .= '=';
						$token->Type = token::TCmpGTEq;
						++$sPos;
					} else if ($input[$sPos] == '>') {
						$token->Symbol .= '>';
						$token->Type = token::TBitRS;
						++$sPos;
						if ($input[$sPos] == '=') {
							$token->Symbol .= '=';
							$token->Type = token::TBitRSAssign;
							++$sPos;
						}
					}
				}
				break;
			case '=':
				{
					$token->Type = token::TEqual;
					++$sPos;
					if ($input[$sPos] == '=') {
						$token->Symbol .= '=';
						$token->Type = token::TCmpEq;
						++$sPos;
					}
				}
				break;
			case '!':
				{
					$token->Type = token::TNot;
					++$sPos;
					if ($input[$sPos] == '=') {
						$token->Symbol .= '=';
						$token->Type = token::TCmpNEq;
						++$sPos;
					}
				}
				break;
			case '#':
				{
					$token->Type = token::THash;
					++$sPos;
					if ($input[$sPos] == '>') {
						$token->Symbol .= '>';
						$token->Type = token::TPrint;
						++$sPos;
					}
				}
				break;
			case '(':
				{
					$token->Type = token::TOpenParen;
					++$sPos;
				}
				break;
			case ')':
				{
					$token->Type = token::TCloseParen;
					++$sPos;
				}
				break;
			case '[':
				{
					$token->Type = token::TOpenBracket;
					++$sPos;
				}
				break;
			case ']':
				{
					$token->Type = token::TCloseBracket;
					++$sPos;
				}
				break;
			case '{':
				{
					$token->Type = token::TOpenBrace;
					++$sPos;
				}
				break;
			case '}':
				{
					$token->Type = token::TCloseBrace;
					++$sPos;
				}
				break;
			case '?':
				{
					$token->Type = token::TQuestionMark;
					++$sPos;
				}
				break;

			case '"':
				{
					++$sPos;

					$token->Type = token::TString;
					$token->Symbol = "";
					while ($sPos < $iLen && $input[$sPos] != '"') {
						if ($input[$sPos] == '\\' && $sPos + 1 < $iLen && $input[$sPos + 1] == '"') {
							++$sPos;
						}

						$token->Symbol .= $input[$sPos];
						++$sPos;
					}

					if ($sPos < $iLen && $input[$sPos] == '"') {
						++$sPos;
					} else throw new TException("Reached 'end of chunk' while parsing a string.", 1010101);
				}
				break;

			default:
				{

					if (IsAlpha($input[$sPos])) {
						$token->Type = token::TIdentifier;
						$token->Symbol = "";
						while ($sPos < $iLen && (IsAlpha($input[$sPos]) || IsNumeric($input[$sPos]))) {
							$token->Symbol .= $input[$sPos];
							++$sPos;
						}
						if (in_array($token->Symbol, keyword::Op)) {
							$token->Type = token::TKeyword_Op;
						} else if (in_array($token->Symbol, keyword::Value)) {
							$token->Type = token::TKeyword_Value;
						}
					} else if (IsNumeric($input[$sPos])) {
						$token->Type = token::TNumber;
						$token->Symbol = "";
						$decimals = false;

						while ($sPos < $iLen && (IsNumeric($input[$sPos]) || $input[$sPos] == '.')) {
							$token->Symbol .= $input[$sPos];

							if ($input[$sPos] == '.') {
								if ($decimals) throw new TException("Parsed value has too many decimal divisions: '" . $token->Symbol . "'", 1010101, ErrorLevel::EL_PARSE);
								else $decimals = true;
							}

							++$sPos;
						}
					} else {
						$token->Type = token::TUnknown;
						++$sPos;
					}
				}
				break;
		}
	} else {
		$token->Symbol = '';
		$token->Type = token::TEndOfStream;
	}

	$token->At = $sPos;
	return $token;
}

#///////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////
#///////////////////////////////////////////////////////////////////////////
class exp_node
{
	public $Op = 101, $Value = null, $LeftExp = null, $RightExp = null;
}

function MarkErrorStartingAt(&$input, $token)
{
	return "<font color='white'>" . substr($input, 0, $token->At - strlen($token->Symbol)) . "</font><font color='red'>" . $token->Symbol . "</font> <font color='cyan'>&#171;&#171;&#171;</font> <u><font color='white'>" . substr($input, $token->At) . "</font></u>";
}


//----------------------------------------------------------------
//----------------------------------------------------------------
//----------------------------------------------------------------
//----------------------------------------------------------------
function _PParams(&$input, &$token)
{
	$left_exp = _PCompoundOp($input, $token);

	if ($token->Type == token::TComma) {
		$node = new exp_node();
		$node->LeftExp = $left_exp;
		$node->Op = $token->Type;

		$token = GetToken($input, $token->At);
		$node->RightExp = _PParams($input, $token);
		return $node;
	}
	return $left_exp;
}

function _PVarObject(&$input, &$token)
{
	$node = null;

	if ($token->Type == token::TObjAccess) {
		$node = new exp_node();
		$node->Op = token::TObjAccess;

		$token = GetToken($input, $token->At);
		$node->LeftExp = _PVarObject($input, $token);
	} else if ($token->Type == token::TOpenBracket) {
		$node = new exp_node();
		$node->Op = token::TArrAccess;

		$token = GetToken($input, $token->At);
		$node->LeftExp = _PCompoundOp($input, $token);

		if ($token->Type == token::TCloseBracket) {
			$token = GetToken($input, $token->At);
			if ($token->Type == token::TOpenBracket ||
				$token->Type == token::TObjAccess) {
				$node->RightExp = _PVarObject($input, $token);
			}
		} else {
			throw new TException("Expected matching bracket, but found '" . $token->Symbol . "' when parsing '" . MarkErrorStartingAt($input, $token) . "'.", 101, ErrorLevel::EL_PARSE);
		}
	} else if ($token->Type == token::TIdentifier) {
		$node = new exp_node();
		$node->Op = token::TValueOf;
		$node->Value = $token->Symbol;

		$peek = GetToken($input, $token->At);
		if ($peek->Type == token::TOpenParen) {
			$node->Op = token::TFctObjectCall;

			$token = GetToken($input, $peek->At);
			if ($token->Type != token::TCloseParen) {
				$node->LeftExp = _PParams($input, $token);
			}

			if ($token->Type == token::TCloseParen) {
				$token = GetToken($input, $token->At);
			} else {
				throw new TException("Expected matching paren, but found '" . $token->Symbol . "' when parsing '" . MarkErrorStartingAt($input, $token) . "'.", 101, ErrorLevel::EL_PARSE);
			}
		} else {
			$token = $peek;
			$node->LeftExp = _PVarObject($input, $token);
		}
	}

	return $node;
}

function _PValue(&$input, &$token)
{
	$node = new exp_node();

	if ($token->Type == token::TString) {
		$node->Op = token::TValueIs;
		$node->Value = $token->Symbol;

		$token = GetToken($input, $token->At);
	} else if ($token->Type == token::TNumber) {
		$node->Op = token::TValueIs;
		$node->Value = floatval($token->Symbol);

		$token = GetToken($input, $token->At);
	} else if ($token->Type == token::TKeyword_Value) {
		$node->Op = token::TValueIs;
		if ($token->Symbol == "false") {
			$node->Value = false;

			$token = GetToken($input, $token->At);
		} else if ($token->Symbol == "true") {
			$node->Value = true;

			$token = GetToken($input, $token->At);
		} else if ($token->Symbol == "null") {
			$node->Value = null;

			$token = GetToken($input, $token->At);
		} else {
			throw new TException("Unknown keyword value '" . $token->Symbol . "'.",
				101, ErrorLevel::EL_PARSE);
		}
	} else if ($token->Type == token::TIdentifier) {
		$node->Op = token::TValueOf;
		$node->Value = $token->Symbol;

		$peek = GetToken($input, $token->At);
		if ($peek->Type == token::TOpenParen) {
			if (function_exists($node->Value)) {
				$node->Op = token::TFctGlobalCall;
			} else {
				$node->Op = token::TFctObjectCall;
			}

			$token = GetToken($input, $peek->At);
			if ($token->Type != token::TCloseParen) {
				$node->LeftExp = _PParams($input, $token);
			}

			if ($token->Type == token::TCloseParen) {
				$token = GetToken($input, $token->At);
			} else {
				throw new TException("Expected matching paren, but found '" . $token->Symbol . "' when parsing '" . MarkErrorStartingAt($input, $token) . "'.", 101, ErrorLevel::EL_PARSE);
			}
		} else {
			$token = $peek;
			$node->LeftExp = _PVarObject($input, $token);
		}
	} else if ($token->Type == token::TOpenParen) {
		$node->Op = token::TGroup;

		$token = GetToken($input, $token->At);
		$node->LeftExp = _PCompoundOp($input, $token);

		if ($token->Type == token::TCloseParen) {
			$token = GetToken($input, $token->At);
		} else {
			throw new TException("Expected matching paren, but found '" . $token->Symbol . "' when parsing '" . MarkErrorStartingAt($input, $token) . "'.", 101, ErrorLevel::EL_PARSE);
		}
	} else {
		throw new TException("This sign '" . $token->Symbol . "' isn't a value.",
			101, ErrorLevel::EL_PARSE);
	}
	return $node;
}

function _PTerme(&$input, &$token)
{
	if ($token->Type == token::TIncrement ||
		$token->Type == token::TDecrement) {

		$node = new exp_node();
		$node->Op = $token->Type;

		$token = GetToken($input, $token->At);
		if ($token->Type == token::TIdentifier) {
			$node->RightExp = _PValue($input, $token);
		} else {
			throw new TException("Expected incrementation/decrementation of a variable", 101, ErrorLevel::EL_PARSE);
		}

		return $node;
	} else {
		if ($token->Type == token::TIdentifier) {
			$inode = _PValue($input, $token);

			if ($token->Type == token::TIncrement ||
				$token->Type == token::TDecrement) {

				$node = new exp_node();
				$node->Op = $token->Type;
				$node->LeftExp = $inode;

				$token = GetToken($input, $token->At);
				return $node;
			} else {
				return $inode;
			}
		} else {
			return _PValue($input, $token);
		}
	}
}

function _PUnary(&$input, &$token)
{
	if ($token->Type == token::TNot || $token->Type == token::TBitNot) {
		$node = new exp_node();
		$node->Op = $token->Type;

		$token = GetToken($input, $token->At);
		$node->LeftExp = _PUnary($input, $token);
		return $node;
	} else if ($token->Type == token::TPlus || $token->Type == token::TDash) {
		$node = new exp_node();
		$node->Op = token::TSign;
		$node->Value = $token->Symbol;

		$token = GetToken($input, $token->At);
		$node->LeftExp = _PUnary($input, $token);
		return $node;
	} else {
		return _PTerme($input, $token);
	}
}

function _POpLevel0(&$input, &$token)
{
	$left_exp = _PUnary($input, $token);
	if ($token->Type == token::TAsterisk ||
		$token->Type == token::TSlash ||
		$token->Type == token::TPercent) {
		$node = new exp_node();
		$node->LeftExp = $left_exp;
		$node->Op = $token->Type;

		$token = GetToken($input, $token->At);
		$node->RightExp = _POpLevel0($input, $token);
		return $node;
	}
	return $left_exp;
}

function _POpLevel1(&$input, &$token)
{
	$left_exp = _POpLevel0($input, $token);

	if ($token->Type == token::TPlus ||
		$token->Type == token::TDash) {
		$node = new exp_node();
		$node->LeftExp = $left_exp;
		$node->Op = $token->Type;

		$token = GetToken($input, $token->At);
		$node->RightExp = _POpLevel1($input, $token);
		return $node;
	}
	return $left_exp;
}

function _POpLevel2(&$input, &$token)
{
	$left_exp = _POpLevel1($input, $token);

	if ($token->Type == token::TBitLS ||
		$token->Type == token::TBitRS) {
		$node = new exp_node();
		$node->LeftExp = $left_exp;
		$node->Op = $token->Type;

		$token = GetToken($input, $token->At);
		$node->RightExp = _POpLevel2($input, $token);
		return $node;
	}
	return $left_exp;
}

function _PRelation(&$input, &$token)
{
	$left_exp = _POpLevel2($input, $token);

	if ($token->Type == token::TCmpEq ||
		$token->Type == token::TCmpNEq ||
		$token->Type == token::TCmpLT ||
		$token->Type == token::TCmpGT ||
		$token->Type == token::TCmpLTEq ||
		$token->Type == token::TCmpGTEq) {

		$node = new exp_node();
		$node->Op = $token->Type;
		$node->LeftExp = $left_exp;

		$token = GetToken($input, $token->At);
		$node->RightExp = _PRelation($input, $token);

		return $node;
	}

	return $left_exp;
}

function _PBitwiseAndXorOr(&$input, &$token)
{
	$left_exp = _PRelation($input, $token);

	if ($token->Type == token::TBitAnd ||
		$token->Type == token::TBitXor ||
		$token->Type == token::TBitOr) {

		$node = new exp_node();
		$node->Op = $token->Type;
		$node->LeftExp = $left_exp;

		$token = GetToken($input, $token->At);
		$node->RightExp = _PBitwiseAndXorOr($input, $token);

		return $node;
	}

	return $left_exp;
}

function _PLogicalAnd(&$input, &$token)
{
	$left_exp = _PBitwiseAndXorOr($input, $token);

	if ($token->Type == token::TAnd) {
		$node = new exp_node();
		$node->Op = $token->Type;
		$node->LeftExp = $left_exp;

		$token = GetToken($input, $token->At);
		$node->RightExp = _PLogicalAnd($input, $token);

		return $node;
	}

	return $left_exp;
}

function _PLogicalOr(&$input, &$token)
{
	$left_exp = _PLogicalAnd($input, $token);

	if ($token->Type == token::TOr) {
		$node = new exp_node();
		$node->Op = $token->Type;
		$node->LeftExp = $left_exp;

		$token = GetToken($input, $token->At);
		$node->RightExp = _PLogicalOr($input, $token);

		return $node;
	}

	return $left_exp;
}


function _PCompoundOp(&$input, &$token)
{
	$left_exp = _PLogicalOr($input, $token);

	if ($token->Type == token::TEqual ||
		$token->Type == token::TIncAssign ||
		$token->Type == token::TDecAssign ||
		$token->Type == token::TMulAssign ||
		$token->Type == token::TDivAssign ||
		$token->Type == token::TModAssign ||
		$token->Type == token::TBitLSAssign ||
		$token->Type == token::TBitRSAssign ||
		$token->Type == token::TBitAndAssign ||
		$token->Type == token::TBitXorAssign ||
		$token->Type == token::TBitOrAssign) {
		$node = new exp_node();
		$node->Op = $token->Type;
		$node->LeftExp = $left_exp;

		$token = GetToken($input, $token->At);
		$node->RightExp = _PCompoundOp($input, $token);

		return $node;
	} else if ($token->Type == token::TQuestionMark) {
		$parent_node = new exp_node();
		$parent_node->Op = $token->Type;

		$parent_node->LeftExp = $left_exp;

		$child_node = new exp_node();
		$parent_node->RightExp = $child_node;

		$token = GetToken($input, $token->At);
		$child_node->LeftExp = _PCompoundOp($input, $token);

		if ($token->Type == token::TColon) {
			$child_node->Op = $token->Type;
			$token = GetToken($input, $token->At);
			$child_node->RightExp = _PCompoundOp($input, $token);
		} else {
			throw new TException("Expected colon and found symbol '" . $token->Symbol . "'",
				101, ErrorLevel::EL_PARSE);
		}

		return $parent_node;
	} else if ($token->Type == token::TCloseParen ||
		$token->Type == token::TCloseBracket ||
		$token->Type == token::TColon ||
		$token->Type == token::TSemicolon ||
		$token->Type == token::TComma ||
		$token->Type == token::TEndOfStream) {
	} else {
		throw new TException("Unexpected token '" . $token->Symbol . "' found.",
			101, ErrorLevel::EL_PARSE);
	}

	return $left_exp;
}

function PIf(&$input, &$token)
{
	if ($token->Type == token::TOpenParen) {
		$token = GetToken($input, $token->At);
		$node = _PCompoundOp($input, $token);

		if ($token->Type == token::TCloseParen) {
			$token = GetToken($input, $token->At);
			return $node;
		} else {
			throw new TException("Expecting ')' but for declaration never ended.", 101, ErrorLevel::EL_PARSE);
		}
	} else {
		throw new TException("Expected '('.", 101, ErrorLevel::EL_PARSE);
	}
}

function PForLoop(&$input, &$token)
{
	if ($token->Type == token::TOpenParen) {
		$node = new exp_node();

		$token = GetToken($input, $token->At);
		$node->LeftExp = _PCompoundOp($input, $token);

		if ($token->Type == token::TColon) {
			$node->Op = $token->Type;

			$token = GetToken($input, $token->At);
			if ($token->Type == token::TIdentifier) {
				$node->RightExp = _PCompoundOp($input, $token);
			} else {
				throw new TException("Expected an identifier.", 101, ErrorLevel::EL_PARSE);
			}
		} else if ($token->Type == token::TSemicolon) {
			$node->Op = $token->Type;
			$node2 = new exp_node();

			$token = GetToken($input, $token->At);
			$node2->LeftExp = _PCompoundOp($input, $token);

			if ($token->Type == token::TSemicolon) {
				$node2->Op = $token->Type;

				$token = GetToken($input, $token->At);
				$node2->RightExp = _PCompoundOp($input, $token);
			} else {
				throw new TException("Expected ';' delimiter.", 101, ErrorLevel::EL_PARSE);
			}

			$node->RightExp = $node2;
		} else {
			throw new TException("Expected ':'.", 101, ErrorLevel::EL_PARSE);
		}

		if ($token->Type == token::TCloseParen) {
			$token = GetToken($input, $token->At);
			return $node;
		} else {
			throw new TException("Expecting ')' but for declaration never ended.", 101, ErrorLevel::EL_PARSE);
		}
	} else {
		throw new TException("Expected '('.", 101, ErrorLevel::EL_PARSE);
	}

}

function PWhileLoop(&$input, &$token)
{
	if ($token->Type == token::TOpenParen) {
		$token = GetToken($input, $token->At);
		$node = _PCompoundOp($input, $token);

		if ($token->Type == token::TCloseParen) {
			$token = GetToken($input, $token->At);
			return $node;
		} else {
			throw new TException("Expecting ')' but for declaration never ended.", 101, ErrorLevel::EL_PARSE);
		}
	} else {
		throw new TException("Expected '('.", 101, ErrorLevel::EL_PARSE);
	}

}

function PEcho(&$input, &$token)
{
	if ($token->Type == token::TOpenParen) {
		$token = GetToken($input, $token->At);
		$node = _PCompoundOp($input, $token);

		if ($token->Type == token::TCloseParen) {
			$token = GetToken($input, $token->At);
			return $node;
		} else {
			throw new TException("Expected ')'.", 101, ErrorLevel::EL_PARSE);
		}
	} else {
		throw new TException("Expected '('.", 101, ErrorLevel::EL_PARSE);
	}
}

function PStatement(&$input, &$token)
{
	if ($token->Type == token::TEndOfStream) {
		$node = new exp_node();
		$node->Op = token::TEndOfStream;
		return $node;
	}

	if ($token->Type != token::TKeyword_Op &&
		$token->Type != token::TKeyword_Value &&
		$token->Type != token::TPrint &&
		$token->Type != token::TIdentifier &&
		$token->Type != token::TNumber) {
		throw new TException("Unimplemented path '" . $token->Symbol . "'.",
			101, ErrorLevel::EL_PARSE);
	}

	if ($token->Type == token::TKeyword_Op ||
		$token->Type == token::TPrint) {

		$node = new exp_node();
		if ($token->Symbol == "if") {
			$node->Op = token::TIf;

			$token = GetToken($input, $token->At);
			$node->LeftExp = PIf($input, $token);
		} else if ($token->Symbol == "else") {
			$node->Op = token::TElse;

			$token = GetToken($input, $token->At);
			if ($token->Symbol == "if") {
				$node->LeftExp = PStatement($input, $token);
			}
		} else if ($token->Symbol == "for") {
			$node->Op = token::TFor;

			$token = GetToken($input, $token->At);
			$node->LeftExp = PForLoop($input, $token);
		} else if ($token->Symbol == "while") {
			$node->Op = token::TWhile;

			$token = GetToken($input, $token->At);
			$node->LeftExp = PWhileLoop($input, $token);
		} else if ($token->Symbol == "#>") {
			$node->Op = token::TPrint;

			$token = GetToken($input, $token->At);
			$node->LeftExp = PEcho($input, $token);
		} else {
			throw new TException("Unimplemented keyword '" . $token->Symbol . "'.",
				101, ErrorLevel::EL_PARSE);
		}
		return $node;
	} else {
		return _PCompoundOp($input, $token);
	}
}


/*
===================================================================
*/

class section
{
	public $Type = 0, $Text = "", $AST = null;
	public $CSection = null, $NSection = null;
}

function SWith(&$input, $needle)
{
	return substr(trim($input), 0, strlen($needle)) == $needle;
}

function GetSections(&$input, $offset)
{
	$sections = array();

	$input_len = strlen($input);

	while ($offset < $input_len) {
		$sPos = strpos($input, "{@", $offset);
		$ePos = strpos($input, "@}", $offset);

		if ($sPos === false && $ePos === false) {
			$section = new section();
			$section->Text = substr($input, $offset);
			$offset += strlen($section->Text);
			array_push($sections, $section);
		} else if ($sPos === false || $ePos === false) {
			$err_message = "";
			if ($sPos === false) {
				$err_message = substr($input, max(0, $ePos - 15), $ePos < 15 ? $ePos + 2 : 17);
			}

			if ($ePos === false) {
				$err_message = substr($input, max(0, $sPos - 15), $sPos < 15 ? $sPos + 2 : 17);
			}
			throw new TException(
				"Unexpected delimiter '<font color=red>" . $err_message . "</font>'",
				101, ErrorLevel::EL_PARSE
			);
		} else {
			if ($sPos < $ePos) {
				$ext_sPos = strpos($input, "{@", $sPos + 2);
				if ($ext_sPos === false || $ext_sPos > $ePos) {
					if ($offset < $sPos) {
						$section = new section();
						$section->Text = substr($input, $offset, $sPos - $offset);
						array_push($sections, $section);
					}

					$stringDecl = substr($input, $sPos + 2, $ePos - ($sPos + 2));

					$arrayDecl = array();
					if (SWith($stringDecl, "if") || SWith($stringDecl, "else") ||
						SWith($stringDecl, "for") || SWith($stringDecl, "while") ||
						SWith($stringDecl, "end")) {
						array_push($arrayDecl, $stringDecl);
					} else {
						$arrayDecl = explode(";", $stringDecl);
					}

					foreach ($arrayDecl as $decl) {
						$section = new section();
						$section->Type = 1;
						$section->Text = $decl;
						if (SWith($section->Text, "else") || SWith($section->Text, "end")) {
							$esec = new section();
							$esec->Type = 1;
							$esec->Text = "###";
							array_push($sections, $esec);
						}
						array_push($sections, $section);
					}
					$offset = $ePos + 2;
				} else {
					$err_message_1 = str_replace(array("\r", "\n", "\t"), " ",
						substr($input, max(0, $sPos - 15), $sPos < 15 ? $sPos + 2 : 17)
					);
					$err_message_2 = str_replace(array("\r", "\n", "\t"), " ",
						substr($input, max(0, $ext_sPos - 15), $ext_sPos < 15 ? $ext_sPos + 2 : 17)
					);
					throw new TException(
						"Ambiguous delimiters '<font color=red>" . $err_message_1 . "</font>' and '<font color=red>" . $err_message_2 . "</font>' found.",
						101, ErrorLevel::EL_PARSE
					);
				}
			} else {
				$err_message = str_replace(array("\r", "\n", "\t"), " ",
					substr($input, max(0, $ePos - 10), $ePos < 10 ? $ePos + 2 : 10)
				);

				throw new TException(
					"Unexpected delimiter '<font color=red>" . $err_message . "</font>' found.",
					101, ErrorLevel::EL_PARSE
				);
			}
		}
	}

	if (empty($sections)) array_push($sections, new section());
	return $sections;
}

function MakeTplTree(&$sections, &$section)
{
	$csec = &$section;
	while (!empty($sections)) {
		$tsec = $sections[0];

		if ($tsec->Type) {
			if (SWith($tsec->Text, "if") || SWith($tsec->Text, "else") ||
				SWith($tsec->Text, "for") || SWith($tsec->Text, "while")) {
				$csec = array_shift($sections);

				$token = GetToken($csec->Text);
				$csec->AST = PStatement($csec->Text, $token);

				MakeTplTree($sections, $csec->CSection);
				$csec = &$csec->NSection;
			} else if (SWith($tsec->Text, "end")) {
				$csec = array_shift($sections);
				$csec = &$csec->NSection;
			} else if (SWith($tsec->Text, "#>")) {
				$csec = array_shift($sections);

				$token = GetToken($csec->Text);
				$csec->AST = PStatement($csec->Text, $token);

				$csec = &$csec->NSection;
			} else if (SWith($tsec->Text, "###")) {
				array_shift($sections);
				break;
			} else {
				// ## or #> or (anything)
				$csec = array_shift($sections);

				$token = GetToken($csec->Text);
				$csec->AST = PStatement($csec->Text, $token);

				$csec = &$csec->NSection;
			}
		} else {
			$csec = array_shift($sections);
			$csec = &$csec->NSection;
		}
	}
}

function EvalIfCompare(&$node, &$scope_values)
{
	$op = $node->Op;
	$left = $node->LeftExp;
	$right = $node->RightExp;

	if (!is_null($left) && !is_null($right)) {
		if ($op == token::TCmpEq) {
			return EvalIfOp($left, $scope_values) == EvalIfOp($right, $scope_values);
		} else if ($op == token::TCmpNEq) {
			return EvalIfOp($left, $scope_values) != EvalIfOp($right, $scope_values);
		} else if ($op == token::TCmpLTEq) {
			return EvalIfOp($left, $scope_values) <= EvalIfOp($right, $scope_values);
		} else if ($op == token::TCmpGTEq) {
			return EvalIfOp($left, $scope_values) >= EvalIfOp($right, $scope_values);
		} else if ($op == token::TCmpLT) {
			return EvalIfOp($left, $scope_values) < EvalIfOp($right, $scope_values);
		} else if ($op == token::TCmpGT) {
			return EvalIfOp($left, $scope_values) > EvalIfOp($right, $scope_values);
		} else {
			throw new TException(
				"Unknown '" . $op . "' comparision found.", 102, ErrorLevel::EL_PARSE
			);
		}
	} else {
		throw new TException(
			"Missing one of the comparision operands.", 102, ErrorLevel::EL_PARSE
		);
	}
}

function EvalFuncParams(&$node, &$local_scope)
{
	$Result = array();

	for ($it = $node; $it; $it = $it->RightExp) {
		if ($it->Op == token::TComma) {
			array_push($Result, EvalIfOp($it->LeftExp, $local_scope));
		} else {
			array_push($Result, EvalIfOp($it, $local_scope));
		}
	}

	return $Result;
}

function &FindVarInScope(&$node, &$original_local_scope, &$local_scope)
{
	if ($node->Op == token::TValueOf) {
		if (is_array($local_scope)) {
			if (array_key_exists($node->Value, $local_scope)) {
				if (is_null($node->LeftExp)) {
					return $local_scope[$node->Value];
				} else {
					return FindVarInScope($node->LeftExp, $original_local_scope,
						$local_scope[$node->Value]);
				}
			} else {
				throw new TException(
					"Member '" . $node->Value . "' not in scope.", 102, ErrorLevel::EL_PARSE
				);
			}

		} else if (is_object($local_scope)) {
			if (property_exists($local_scope, $node->Value)) {
				if (is_null($node->LeftExp)) {
					return $local_scope->{$node->Value};
				} else {
					return FindVarInScope($node->LeftExp, $original_local_scope, $local_scope->{$node->Value});
				}
			} else {
				throw new TException(
					"Object member '" . $node->Value . "' not in scope.", 102, ErrorLevel::EL_PARSE
				);
			}
		} else {
			throw new TException(
				"Unidentified container name '" . $node->Value . "'.", 102, ErrorLevel::EL_PARSE
			);
		}
	} else if ($node->Op == token::TValueIs) {
		return $node->Value;
	} else if ($node->Op == token::TObjAccess) {
		if (is_null($node->LeftExp)) {
			throw new TException(
				"Object member not specified.", 102, ErrorLevel::EL_PARSE
			);
		}
		return FindVarInScope($node->LeftExp, $original_local_scope, $local_scope);
	} else if ($node->Op == token::TArrAccess) {
		$var_base = EvalIfOp($node->LeftExp, $original_local_scope);

		if (is_array($local_scope) && isset($local_scope[$var_base])) {
			if (is_null($node->RightExp)) {
				return $local_scope[$var_base];
			} else {
				return FindVarInScope($node->RightExp, $original_local_scope, $local_scope[$var_base]);
			}
		} else if (is_object($local_scope)) {
			throw new TException(
				"Cannot access object as an array.", 102, ErrorLevel::EL_PARSE
			);
		} else {
			throw new TException(
				"Array member '" . $var_base . "' not in scope.", 102, ErrorLevel::EL_PARSE
			);
		}
	} else if ($node->Op == token::TFctObjectCall) {
		$fct = null;
		if (is_array($local_scope) && array_key_exists($node->Value, $local_scope)) {
			$fct = $local_scope[$node->Value];
		} else if (is_object($local_scope) && (property_exists($local_scope, $node->Value) || method_exists($local_scope, $node->Value))) {
			if ($local_scope instanceof stdClass) {
				$fct = $local_scope->{$node->Value};
			} else {
				$fct = array($local_scope, $node->Value);
			}
		} else {
			throw new TException(
				"Function '" . $node->Value . "' cannot be found in scope.", 102, ErrorLevel::EL_PARSE
			);
		}

		if (is_callable($fct, TRUE)) {
			$var_array = EvalFuncParams($node->LeftExp, $original_local_scope);
			$result = call_user_func_array($fct, $var_array);
			return $result;
		} else {
			throw new TException(
				"Function '" . $node->Value . "' not a valid callback.", 102, ErrorLevel::EL_PARSE
			);
		}
	} else {
		throw new TException(
			"Unknown var object access method.", 102, ErrorLevel::EL_PARSE
		);
	}
}

function EvalUnaryOp(&$node, &$local_scope)
{
	$order = (is_null($node->LeftExp) ? 0 : 1);
	$left = $node->LeftExp;
	$right = $node->RightExp;

	if ($node->Op == token::TIncrement) {
		if ($order == 0) {
			if (is_null($right)) {
				throw new TException(
					"Cannot increment null pointer.", 102, ErrorLevel::EL_PARSE
				);
			} else if ($right->Op == token::TValueOf) {
				$var = &FindVarInScope($right, $local_scope, $local_scope);
				$var++;
				return $var;
			} else {
				throw new TException(
					"Only variables can be incremented.", 102, ErrorLevel::EL_PARSE
				);
			}
		} else {
			if (is_null($left)) {
				throw new TException(
					"Cannot increment null pointer.", 102, ErrorLevel::EL_PARSE
				);
			} else if ($left->Op == token::TValueOf) {
				$var = &FindVarInScope($left, $local_scope, $local_scope);
				$value = $var;
				$var++;
				return $value;
			} else {
				throw new TException(
					"Only variables can be incremented.", 102, ErrorLevel::EL_PARSE
				);
			}
		}
	} else if ($node->Op == token::TDecrement) {
		if ($order == 0) {
			if (is_null($right)) {
				throw new TException(
					"Cannot increment null pointer.", 102, ErrorLevel::EL_PARSE
				);
			} else if ($right->Op == token::TValueOf) {
				$var = &FindVarInScope($right, $local_scope, $local_scope);
				$var--;
				return $var;
			} else {
				throw new TException(
					"Only variables can be decremented.", 102, ErrorLevel::EL_PARSE
				);
			}
		} else {
			if (is_null($left)) {
				throw new TException(
					"Cannot increment null pointer.", 102, ErrorLevel::EL_PARSE
				);
			} else if ($left->Op == token::TValueOf) {
				$var = &FindVarInScope($left, $local_scope, $local_scope);
				$value = $var;
				$var--;
				return $value;
			} else {
				throw new TException(
					"Only variables can be decremented.", 102, ErrorLevel::EL_PARSE
				);
			}
		}
	} else {
		throw new TException(
			"Unknown unary operation.", 102, ErrorLevel::EL_PARSE
		);
	}
}

function EvalIfVarModifier(&$node, &$scope_values)
{
	$op = $node->Op;
	if ($op == token::TIncAssign || $op == token::TDecAssign ||
		$op == token::TDivAssign || $op == token::TMulAssign ||
		$op == token::TModAssign || $op == token::TBitOrAssign ||
		$op == token::TBitAndAssign || $op == token::TBitXorAssign ||
		$op == token::TBitLSAssign || $op == token::TBitRSAssign) {

		$left = $node->LeftExp;

		if (is_null($left)) {
			throw new TException(
				"Cannot increment null pointer.", 102, ErrorLevel::EL_PARSE
			);
		} else {
			$right = $node->RightExp;
			if ($left->Op == token::TValueOf) {
				$var = &FindVarInScope($left, $scope_values, $scope_values);
				if ($op == token::TIncAssign) $var += EvalIfOp($right, $scope_values);
				else if ($op == token::TDecAssign) $var -= EvalIfOp($right, $scope_values);
				else if ($op == token::TDivAssign) $var /= EvalIfOp($right, $scope_values);
				else if ($op == token::TMulAssign) $var *= EvalIfOp($right, $scope_values);
				else if ($op == token::TModAssign) $var %= EvalIfOp($right, $scope_values);
				else if ($op == token::TBitOrAssign) $var |= EvalIfOp($right, $scope_values);
				else if ($op == token::TBitAndAssign) $var &= EvalIfOp($right, $scope_values);
				else if ($op == token::TBitXorAssign) $var ^= EvalIfOp($right, $scope_values);
				else if ($op == token::TBitLSAssign) $var <<= EvalIfOp($right, $scope_values);
				else if ($op == token::TBitRSAssign) $var >>= EvalIfOp($right, $scope_values);
				else {
					throw new TException("Unknown assign operation.", 102, ErrorLevel::EL_PARSE);
				}
				return $var;
			} else {
				throw new TException(
					"Only variables can be incremented.", 102, ErrorLevel::EL_PARSE
				);
			}
		}
	} else {
		throw new TException(
			"Non implemented var modifier '" . $op . "'.", 102, ErrorLevel::EL_PARSE
		);
	}
}

function EvalIfMathOp(&$node, &$scope_values)
{
	$op = $node->Op;
	$left = $node->LeftExp;
	$right = $node->RightExp;

	if (is_null($left)) {
		if ($op == token::TAsterisk || $op == token::TSlash || $op == token::TPercent) {
			throw new TException(
				"Unexpected operation.", 102, ErrorLevel::EL_PARSE
			);
		}

		if (is_null($right)) {
			throw new TException(
				"Unexpected null right operand when left is already null.", 102, ErrorLevel::EL_PARSE
			);
		} else {
			if ($op == token::TDash) {
				return -EvalIfOp($right, $scope_values);
			}
			return EvalIfOp($right, $scope_values);
		}
	} else {

		if (is_null($right)) {
			if ($op == token::TAsterisk || $op == token::TSlash || $op == token::TPercent ||
				$op == token::TPlus || $op == token::TDash) {
				throw new TException("Unexpected operation.", 102, ErrorLevel::EL_PARSE);
			}

			return EvalIfOp($left, $scope_values);
		} else {
			$right_operand = EvalIfOp($right, $scope_values);
			$left_operand = EvalIfOp($left, $scope_values);
			if (is_numeric($right_operand)) {
				$right_operand = intval($right_operand);
			} else {
				$right_operand = 0;
			}
			if (is_numeric($left_operand)) {
				$left_operand = intval($left_operand);
			} else {
				$left_operand = 0;
			}

			if ($op == token::TPlus) {
				return $left_operand + $right_operand;
			} else if ($op == token::TDash) {
				return $left_operand - $right_operand;
			} else if ($op == token::TSlash) {
				if ($right_operand == 0) {
					throw new TException(
						"A new black hole was created, you can't divide by 0.", 102, ErrorLevel::EL_PARSE
					);
				}
				return $left_operand / $right_operand;
			} else if ($op == token::TAsterisk) {
				return $left_operand * $right_operand;
			} else if ($op == token::TPercent) {
				if ($right_operand == 0) {
					throw new TException(
						"A new black hole was created, you can't mod by 0.", 102, ErrorLevel::EL_PARSE
					);
				}
				return $left_operand % $right_operand;
			} else {
				throw new TException(
					"Unhandled operation.", 102, ErrorLevel::EL_PARSE
				);
			}
		}
	}
}

function EvalIfOp(&$node, &$scope_values)
{
	if ($node->Op == token::TAnd) {
		return EvalIfOp($node->LeftExp, $scope_values) && EvalIfOp($node->RightExp, $scope_values);
	} else if ($node->Op == token::TOr) {
		return EvalIfOp($node->LeftExp, $scope_values) || EvalIfOp($node->RightExp, $scope_values);
	} else if ($node->Op == token::TAsterisk || $node->Op == token::TSlash ||
		$node->Op == token::TPercent || $node->Op == token::TPlus || $node->Op == token::TDash) {
		return EvalIfMathOp($node, $scope_values);
	} else if ($node->Op == token::TNot) {
		if (is_null($node->LeftExp)) {
			throw new TException(
				"Cannot negate a null ptr.", 102, ErrorLevel::EL_PARSE
			);
		}
		return !EvalIfOp($node->LeftExp, $scope_values);
	} else if ($node->Op == token::TBitNot) {
		if (is_null($node->LeftExp)) {
			throw new TException(
				"Cannot negate a null ptr.", 102, ErrorLevel::EL_PARSE
			);
		}
		$value = EvalIfOp($node->LeftExp, $scope_values);
		if (is_numeric($value)) {
			return ~floatval($value);
		} else {
			return 0;
		}
	} else if ($node->Op == token::TGroup) {
		if (is_null($node->LeftExp)) {
			throw new TException(
				"Cannot evaluate null expression.", 102, ErrorLevel::EL_PARSE
			);
		}
		return EvalIfOp($node->LeftExp, $scope_values);
	} else if ($node->Op == token::TCmpEq || $node->Op == token::TCmpNEq ||
		$node->Op == token::TCmpLTEq || $node->Op == token::TCmpGTEq ||
		$node->Op == token::TCmpLT || $node->Op == token::TCmpGT) {
		return EvalIfCompare($node, $scope_values);
	} else if ($node->Op == token::TIncrement || $node->Op == token::TDecrement) {
		return EvalUnaryOp($node, $scope_values);
	} else if ($node->Op == token::TIncAssign || $node->Op == token::TDecAssign ||
		$node->Op == token::TMulAssign || $node->Op == token::TDivAssign ||
		$node->Op == token::TModAssign || $node->Op == token::TBitXorAssign ||
		$node->Op == token::TBitOrAssign || $node->Op == token::TBitAndAssign ||
		$node->Op == token::TBitLSAssign || $node->Op == token::TBitRSAssign) {
		return EvalIfVarModifier($node, $scope_values);
	} else if ($node->Op == token::TQuestionMark) {
		if ($node->RightExp->Op == token::TColon) {
			return EvalIfOp($node->LeftExp, $scope_values) ?
				EvalIfOp($node->RightExp->LeftExp, $scope_values) :
				EvalIfOp($node->RightExp->RightExp, $scope_values);
		} else {
			throw new TException(
				"Conditional 'if' malformed.", 102, ErrorLevel::EL_PARSE
			);
		}
	} else if ($node->Op == token::TValueOf || $node->Op == token::TValueIs ||
		$node->Op == token::TFctObjectCall) {
		return FindVarInScope($node, $scope_values, $scope_values);
	} else if ($node->Op == token::TFctGlobalCall) {
		$var_array = EvalFuncParams($node->LeftExp, $scope_values);
		$result = call_user_func_array($node->Value, $var_array);
		return $result;
	} else if ($node->Op == token::TSign) {
		$value = EvalIfOp($node->LeftExp, $scope_values);
		return $node->Value == '-' ? -$value : $value;
	} else {
		$left_expr = is_null($node->LeftExp) ? '' : $node->LeftExp->Op . " " . $node->LeftExp->Value;
		$right_expr = is_null($node->RightExp) ? '' : $node->RightExp->Op . " " . $node->RightExp->Value;

		throw new TException(
			"Unhandled behavior of 'Op:" . $node->Op . "' using 'Value:" . $node->Value . "' having 'LeftLeaf:" . $left_expr . "' and 'RightLeaf:" . $right_expr . "'", 102, ErrorLevel::EL_PARSE
		);
	}
}

class for_statement
{
	public $Scope = array(), $UseForeach = 0, $Cond = null, $Modif = null;
}

function EvalForOp(&$node, &$local_scope)
{
	$Result = new for_statement();

	// FOR EACH
	if ($node->Op == token::TColon) {
		$Result->UseForeach = 1;

		$left_var = $node->LeftExp;
		if (!is_null($left_var) && $left_var->Op == token::TSign) {
			$left_var = $left_var->LeftExp;
		}

		$right_stack = $node->RightExp;
		if (!is_null($right_stack) && $right_stack->Op == token::TSign) {
			$right_stack = $right_stack->LeftExp;
		}

		$var_alert = false;
		try {
			FindVarInScope($left_var, $local_scope, $local_scope);
			$var_alert = true;
		} catch (TException $ex) {
			$item_scope = array();
			$scope = &FindVarInScope($right_stack, $local_scope, $local_scope);

			foreach ($scope as $key => $value) {
				$item = array('key' => $key, 'value' => $value);
				$item_scope[][$left_var->Value] = $item;
			}

			$Result->Scope = $item_scope;
		}

		if ($var_alert) {
			throw new TException(
				"Variable name already in a parent scope.", 102, ErrorLevel::EL_PARSE
			);
		}
	} else {
		EvalAssignment($node->LeftExp, $local_scope);
		$cond_modif = $node->RightExp;
		if (is_null($cond_modif)) {
			throw new TException(
				"Invalid 'for' loop definition. Make sure all 3 sections are present.", 102, ErrorLevel::EL_PARSE
			);
		} else {
			$Result->Cond = $cond_modif->LeftExp;
			$Result->Modif = $cond_modif->RightExp;
		}
	}

	return $Result;
}

function EvalAssignment(&$node, &$local_scope)
{
	if ($node->Op == token::TEqual) {
		$left_var = $node->LeftExp;
		$right_stack = $node->RightExp;

		if (!is_null($left_var) && $left_var->Op == token::TSign) {
			$left_var = $left_var->LeftExp;
		}
		if (!is_null($right_stack) && $right_stack->Op == token::TSign) {
			$right_stack = $right_stack->LeftExp;
		}

		$var_alert = false;
		$var = null;
		try {
			$var = &FindVarInScope($left_var, $local_scope, $local_scope);
			$var_alert = true;
		} catch (TException $ex) {
		}

		$value = EvalIfOp($right_stack, $local_scope);

		if ($var_alert) {
			$var = $value;
		} else {
			$local_scope[$left_var->Value] = $value;
		}
	}
}

function &array_ref(&$a)
{
	$Result = array();

	$i = &$a;
	reset($i);
	for (; $c = key($i); next($i)) {
		$Result[$c] = &$a[$c];
	}

	return $Result;
}

function EvalSection(&$section, &$parent_scope)
{
	$Result = "";
	$it = $section;

	$local_scope = &array_ref($parent_scope);
	while (!is_null($it)) {
		if ($it->Type) {
			if (SWith($it->Text, "end")) {
			} else {
				$section_AST = $it->AST;
				if ($section_AST->Op == token::TIf) {
					if (EvalIfOp($section_AST->LeftExp, $local_scope)) {
						if (!is_null($it->CSection)) {
							$Result .= EvalSection($it->CSection, $local_scope);
						}
						while (!SWith($it->Text, "end")) {
							$it = $it->NSection;
						}
					}
				} else if ($section_AST->Op == token::TElse) {
					$left = $section_AST->LeftExp;
					if (!is_null($left)) {
						if (EvalIfOp($left->LeftExp, $local_scope)) {
							if (!is_null($it->CSection)) {
								$Result .= EvalSection($it->CSection, $local_scope);
							}
							while (!SWith($it->Text, "end")) {
								$it = $it->NSection;
							}
						}
					} else {
						$Result .= EvalSection($it->CSection, $local_scope);
					}
				} else if ($section_AST->Op == token::TFor) {
					$for = EvalForOp($section_AST->LeftExp, $local_scope);

					if ($for->UseForeach) {
						foreach ($for->Scope as $scope) {
							$local_scope = array_merge($scope, $local_scope);
							$Result .= EvalSection($it->CSection, $local_scope);
							array_shift($local_scope);
						}
					} else {
						for (; EvalIfOp($for->Cond, $local_scope); EvalIfOp($for->Modif, $local_scope)) {
							$Result .= EvalSection($it->CSection, $local_scope);
						}
					}
				} else if ($section_AST->Op == token::TWhile) {
					$while_scope = array_ref($local_scope);
					while (EvalIfOp($section_AST->LeftExp, $while_scope)) {
						$Result .= EvalSection($it->CSection, $while_scope);
					}
				} else if ($section_AST->Op == token::TEqual) {
					EvalAssignment($section_AST, $local_scope);
				} else if ($section_AST->Op == token::TPrint) {
					$print_data = EvalIfOp($section_AST->LeftExp, $local_scope);
					$Result .= print_r($print_data, true);
				} else if ($section_AST->Op == token::TEndOfStream) {
				} else {
					EvalIfOp($section_AST, $local_scope);
				}
			}
		} else {
			$Result .= $it->Text;
		}
		$it = $it->NSection;
	}
	return $Result;
}
class Parser
{
    public static function Parse($template, &$scope_values,$dev_mode = false)
    {
        // pentru a verifica template si scope
        if($dev_mode){
            \Debugger::Log($scope_values);
            \Debugger::Log($template);
        }

        $sections = GetSections($template, 0);
        $tpl_tree = null;
        MakeTplTree($sections, $tpl_tree);
        $Result = EvalSection($tpl_tree, $scope_values);
        return $Result;
    }
}

//$vars = array(
//	"id"=> 3,
//	"Language" => (object) array("t"=> function() { return "12"; }),
//	"name" => array("A"=>"bizr",
//		"B"=>(object)array(
//			"D"=>"123",
//			"Z"=>"456",
//			"N"=>array("C" =>"d32")
//		)
//	)
//);
//
//
//set_time_limit(1);
//$template = file_get_contents("test.tpl");
//
//Fill(Parse($template, $vars));
