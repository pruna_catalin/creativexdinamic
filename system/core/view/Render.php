<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 16/02/2018
 * Time     : 9:17 AM
 */

namespace UMP\System\Core\View;
use UMP\System\Config\Constants;
class Render extends  Constants
{
    const DEFAULT_TEMPLATE = "default.umpv";

    protected $template = self::DEFAULT_TEMPLATE;
    protected $theme = parent::FRONT_THEME;
    public $fields = array();
    protected $addChilds = array();
    public function __construct($template,  $fields = []) {
        if (!empty($fields)) {
            foreach ($fields as $name => $value) {
                $this->$name = $value;
            }
        }
        $this->template = $template;
    }
    public function folder($theme = ""){
    	if($theme != "") $this->theme = $theme;
    	else return $this->theme;
    }

    public function render($theme = parent::FRONT_THEME) {
        #$application =  new application();
        $this->fields['Language'] = new \Language();
        $template = $this->template;
        $tmpRender = "./system/views/" . $this->theme . "/" . $template . ".umpv";
        if (!is_file($tmpRender) || !is_readable($tmpRender)) {
            \Debugger::log("use " . $tmpRender, "Warning");
        }
        $content = \Utils::ReadFile($tmpRender);
        return \Parser::Parse($content,  $this->fields,self::DEV_RENDER_SCOPE);
//        extract($this->fields);
//        ob_start();
//        include $this->evalSintax($tmpRender);
//        return ob_get_clean();
    }

    public function __set($name, $value) {
        $this->fields[$name] = $value;
        return $this;
    }

    public function __get($name) {
        if (!isset($this->fields[$name])) {
            \Debugger::log("Unable to get the field ". $this->fields[$name] , "Warning");
        }
        $field = $this->fields[$name];
        return $field instanceof Closure ? $field($this) : $field;
    }

    public function __isset($name) {
        return isset($this->fields[$name]);
    }

    public function __unset($name) {
        if (!isset($this->fields[$name])) {
            \Debugger::log("Unable to get the field ". $this->fields[$name] , "Warning");
        }
        unset($this->fields[$name]);
        return $this;
    }

    private function evalSintax($template){
        $parseTemplate = $template;
        #$application =  new application();
        $content = \Utils::ReadFile($template);

        $content = str_replace(array("{","}"), array("<?php echo "," ?>"), $content);
        $content = str_replace("@InjectHtmlElements", "<?php use CreativeX\modules\html\Html as Html;  ?>", $content);
        $content = str_replace("@ProtectCreativeX", "<?php (!defined('CreativeX')) ? exit() : '';  ?>", $content);
        \Utils::WriteToFile(parent::COMPILE_TMP."/Tmp.php", $content);
        return parent::COMPILE_TMP."/Tmp.php";

    }
}