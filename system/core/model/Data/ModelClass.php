<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 02/03/2018
 * Time     : 5:05 PM
 */



class ModelClass
{
    public static function __callStatic($nameModel, $arguments)
    {
        $modelString = "UMP\\System\\Core\\Model\\Data\\".$nameModel;
        return new $modelString();
    }
}