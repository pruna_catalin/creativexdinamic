<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 12/29/2018
 * Time: 10:45 PM
 */

namespace UMP\System\Core\Model;


class Menu
{
	private static $Instance = NULL;

	public function __construct()
	{
	}

	public static function Init()
	{
		if (!self::$Instance) {
			self::$Instance = new Menu();
		}
		return self::$Instance;
	}

	/**
	 * @param $name
	 * @param $value
	 * @return $this
	 */
	public function __set($name, $value)
	{
		$rules = $this->rules($name, $value);
		if ($rules['is_valid']) {
			$this->$name = $value;
			return $this;
		} else {
			\Debugger::Log("COLUMN <font color='red'>" . $name . "</font> <br>" . $rules['result'], "Error");
		}
	}

	public function __get($name)
	{
		return $this->$name;
	}
	/**
	 * Test my comments rules
	 */
	private function rules($column, $value)
	{
		if ($column == "id") {
			$testCase = \Utils::validateColumns("int", false, 11, $value);
			if (!$testCase[0]) {
				return ['is_valid' => false, 'result' => $testCase[1]];
			}
		}
		return ['is_valid' => true, 'result' => ""];
	}
}