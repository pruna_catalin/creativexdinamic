<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 3/4/2018
 * Time: 6:18 PM
 */

namespace UMP\System\Core\Model\DAO;


class DAOUsers
{
    public static $order = "id ASC";

    public static function Init($name,$args = [],$resultType = NULL){
        return self::{$name}($args,$resultType);
    }
    private static function TableName() {
        return "`admin_ci`.`users`";
    }
    /*
     * @params  condition , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
     * @ return false on rows 0/ SUCCESS RETURN MODEL WITH DATA
     */
    public static function Find($data, $resultType = NULL) {
        $db = new \AdvSql('');
        $model  =  \Model::Users();
        $result = [];
        if ($data instanceof $model) {
            $result = DAOTools::FindByModel($db, $data, self::tableName(), self::$order)->fetch($db::SwitchResult($resultType));
        } else {
            if ($data != "") {
                $db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->prepare();
                $request = $db->execute();
                $result = $request->fetch($db::SwitchResult($resultType));
            }else{
                $db->select('*')->from(self::tableName())->prepare();
                $request = $db->execute();
                $result = $request->fetch($db::SwitchResult($resultType));
            }
        }
        return $result;
    }
    /*
     * @params  condition , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
     * @ return false on rows 0/ SUCCESS RETURN MODEL WITH DATA
     */
    public static function FindAll($data, $resultType = NULL){
        $db     = new \AdvSql('');
        $model  =  \Model::Users();
        $result = [];
        $returnData = [];
        if ($data instanceof $model) {
            $result = DAOTools::FindByModel($db, $data, self::tableName(), self::$order)->fetchAll($db::SwitchResult($resultType));
        } else {
            if ($data != "") {
                $db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->prepare();
                $request = $db->execute();
                $result = $request->fetchAll($db::SwitchResult($resultType));
            }else{
                $db->select('*')->from(self::tableName())->prepare();
                $request = $db->execute();
                $result = $request->fetchAll($db::SwitchResult($resultType));
            }
        }
        foreach($result as $column){
            $model->id = $column->id;
            $model->username = $column->username;
            $model->password = $column->password;
            array_push($returnData,$model);
        }
        return $returnData;
    }

}