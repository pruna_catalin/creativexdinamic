<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 3/4/2018
 * Time: 6:26 PM
 */
class DAO
{
    public static function __callStatic($name, $arguments)
    {
        return call_user_func_array("UMP\\System\\Core\\Model\\DAO\\DAO".$name."::Init",$arguments);

    }
}