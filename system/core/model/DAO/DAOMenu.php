<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 12/29/2018
 * Time: 10:43 PM
 */

namespace UMP\System\Core\Model\DAO;
use UMP\System\Core\Model\Menu;

class DAOMenu
{
	public static $order = "id ASC";

	public static function Init($name,$args = [],$resultType = NULL){
		return self::{$name}($args,$resultType);
	}
	private static function TableName() {
		return "`company`.`n_menu`";
	}
	public static function OrderBy($data){
		self::$order = $data;
	}
	/*
	 * @params  condition , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
	 * @ return false on rows 0/ SUCCESS RETURN MODEL WITH DATA
	 */
	public static function Find($data, $resultType = NULL) {
		$db = new \AdvSql('');
		$model  =  \Model::Menu();
		$result = [];
		if ($data instanceof $model) {
			$result = DAOTools::FindByModel($db, $data, self::tableName(), self::$order)->fetch($db::SwitchResult($resultType));
		} else {
			if ($data != "") {
				$db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->prepare();
				$request = $db->execute();
				$result = $request->fetch($db::SwitchResult($resultType));
			}else{
				$db->select('*')->from(self::tableName())->prepare();
				$request = $db->execute();
				$result = $request->fetch($db::SwitchResult($resultType));
			}
		}
		return $result;
	}
	/*
	 * @params  condition , resultType  = 'assoc|num|object|lazy' , null or empty return OBJECT
	 * @ return false on rows 0/ SUCCESS RETURN MODEL WITH DATA
	 */
	public static function FindAll($data, $resultType = NULL){
		$db     = new \AdvSql('');
		$model  =  \Model::Menu();
		$result = [];
		$returnData = [];
		if ($data instanceof $model) {
			$result = DAOTools::FindByModel($db, $data, self::tableName(), self::$order)->fetchAll($db::SwitchResult($resultType));
		} else {
			if ($data != "") {
				$db->select('*')->from(self::tableName())->where($data)->orderby(self::$order)->prepare();
				$request = $db->execute();
				$result = $request->fetchAll($db::SwitchResult($resultType));
			}else{
				$db->select('*')->from(self::tableName())->prepare();
				$request = $db->execute();
				$result = $request->fetchAll($db::SwitchResult($resultType));
			}
		}
		foreach($result as $column){
			$model  = new Menu();
			$model->id = $column->id;
			$model->id_parent = $column->id_parent;
			$model->name = $column->name;
			$model->order_list = $column->order_list;
			$model->url_seo = $column->url_seo;
			$model->type_menu = $column->type_menu;
			$model->create_by = $column->create_by;
			$model->create_at = $column->create_at;
			$model->modified_by = $column->modified_by;
			$model->modified_at = $column->modified_at;
			$model->active = $column->active;
			array_push($returnData,$model);
		}
		return $returnData;
	}
}