<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 3/4/2018
 * Time: 3:33 PM
 */



class Model
{
    public static function __callStatic($name, $arguments)
    {
        return call_user_func_array("UMP\\System\\Core\\Model\\".$name."::Init",$arguments);

    }
}