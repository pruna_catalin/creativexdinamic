<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 16/02/2018
 * Time     : 9:02 AM
 */

namespace UMP\System\Core\Rules;


class Validation
{
    private $unique = [];

    public function __construct()
    {

    }

    public function formValidation($elements = [],$params = []){
        $valid = ["status"=>true,"field"=>""];
        $this->unique = array_values($params);

        foreach ($elements as $elementKey => $elementValue){
            $rule = $this->rule($elementValue,$params[$elementKey]);
            if(!$rule['status']){
                $valid = ["status"=>false,"Field"=>$elementKey,"Value"=>$params[$elementKey],"Rule"=>$rule['rule']];
            }
        }
        return $valid;
    }

    private function rule($rule,$value){
        $result = ["status"=>true,"rule"=>""];
        foreach($rule as $key => $valueRule){
                if($key == "required"){
                    if(empty($value)){
                        return ["status"=>false,"rule"=>$key];
                    }
                }
                if($key == "regex"){
                    if(!preg_match("/".$valueRule."/",  $value,$findMatch)){
                        return ["status"=>false,"rule"=>$valueRule];
                    }
                }
                if($key == "unique"){
                    if(!in_array($value, $this->unique)){
                        return ["status"=>false,"rule"=>$key];
                    }
                }
                if($key == "max"){
                    if(is_numeric($value)){
                        if($value > $valueRule){
                            return ["status"=>false,"rule"=>$key];
                        }
                    }else{
                        if(strlen($value) > $valueRule){
                            return ["status"=>false,"rule"=>$key];
                        }
                    }
                }
                if($key == "min"){
                    if(is_numeric($value)){
                        if($value < $valueRule){
                            return ["status"=>false,"rule"=>$key];
                        }
                    }else{
                        if(strlen($value) < intval($valueRule)){
                            return ["status"=>false,"rule"=>$key];
                        }
                    }
                }

            }
        $this->unique = array_diff($this->unique, [$value]);


        return $result;
    }

}