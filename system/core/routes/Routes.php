<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 14/02/2018
 * Time     : 5:18 PM
 */
namespace UMP\System\Core\Routes;
use UMP\System\Tools\Storage;
class request {
    public $Case = "";
    public $Request = "";
    public $Identifier = "";
    public $Controller = "";
    public $Action = "";
    public $Method = "";
    public $Args = [];
    public $Files = [];
    public $KeyList = [];
    public $Folder  = [];
    public $SecurityRoute = ["security"=>false,"route"=>'',"folder"=>'',"method"=>"GET","ajax"=>''];
    public $Admin = [false];

}
class Routes extends request{
    private $elements = [];
    private $key = "";
    private $folder;
    private $storage = [];
    private $FolderRoute = [];
	private $security = true;
	public $NoSecurity = [];
	public $Admin      = [];
    public function __construct(){
        $this->storage = new Storage();
    }
	private static $_instance = null;
	public static function getInstance (){
		if (self::$_instance === null) {
			self::$_instance = new self;
		}
		return self::$_instance;
	}
    public function keyRoute($key){
        $this->key = $key;
        return $this;
    }
    public function folder($name){
        $this->folder = $name;
        return $this;
    }
    public function admin($is_admin = false){
		$this->Admin[] = $is_admin;
		return $this;
	}
    public  function get($route, $methods,$ajax = ""){
        if($this->key) $this->KeyList[] = $this->key;
        $this->FolderRoute[] = ($this->folder != "") ?  $this->folder : "";
        array_push($this->elements,array($route, $methods,"GET","",$ajax ));
		$this->NoSecurity[] = ["security"=>$this->security,"route"=>$route,"folder"=>$this->folder,"method"=>"GET","ajax"=>$ajax,"methods"=>$methods];
		$this->securityOff();
    }
    public  function post($route, $methods,$ajax = ""){
        if($this->key) $this->KeyList[] = $this->key;
        $this->FolderRoute[] = ($this->folder != "") ?  $this->folder : "";
        array_push($this->elements, array($route, $methods, "POST", $_POST, $ajax));
		$this->NoSecurity[] = ["security"=>$this->security,"route"=>$route,"folder"=>$this->folder,"method"=>"POST","ajax"=>$ajax,"methods"=>$methods];
		$this->securityOff();
    }
	public function securityOff($security = true){
		$this->security = $security;
		return $this;
	}

    public function registerRoute(){
       return $this->routeCompile();
    }
    private function routeCompile(){
        $requestXHR = !is_null(\Utils::GetIfExistsOrDefault('HTTP_X_REQUESTED_WITH', $_SERVER, null,true)  );
        $controller = "";
        $action = "";
        $pattern = "";
        $patternGet = "";
        $folderStorage = $this->storage->session->get('Folder')['value'];
        for($i = 0; $i<sizeof($this->elements);$i++) {
            //if( in_array($this->FolderRoute[$i],(is_array($folderStorage) ? $folderStorage : array()))) {
                if (preg_match("/\{(.*)\}/", $this->elements[$i][0], $findKey)) {
                    $identifier = "\{" . $findKey[1] . "\}";
                    $identifier = str_replace("/","\/",$identifier);

                    if (preg_match("/(.*)" . $identifier . "$/", $this->elements[$i][0], $findPattern)) {
                        $pattern = $findPattern[1]; // path to case
                    }

                    if (preg_match("/(.*)(\/[^\/]+$)/", isset($_GET['case']) ? $_GET['case'] : "", $findPattern)) {
                        $patternGet = $findPattern[1] . "/";
                        $this->Identifier = str_replace("/", "", $findPattern[2]);
                    }

                } else {
                    $pattern = $this->elements[$i][0];
                    $patternGet = isset($_GET['case']) ? $_GET['case'] : "";
                }
                if ($pattern == $patternGet) {
                    if (preg_match("/(.*)@(.*)/", $this->elements[$i][1], $findMethods)) {
                        $controller = $findMethods[1];
                        $action = $findMethods[2];
                    }
                    if ($this->elements[$i][4] == "XHR" && !$requestXHR) {

                    	die;
					}
                    $this->Case = str_replace("\\", "", $this->elements[$i][0]);
                    $this->Request = (isset($_GET['case'])?$_GET['case']: "");
                    $this->Controller = $controller;
                    $this->Action = $action;
                    $this->Method = $this->elements[$i][2];
                    $this->Folder = $this->FolderRoute[$i];
                    $this->Key = $this->KeyList[$i];
                    $this->Args = ($this->elements[$i][2] == "POST") ? (!empty($this->elements[$i][3]) ? $this->elements[$i][3] : "") : [];
                    $this->Files = $_FILES;
                    $this->SecurityRoute = $this->NoSecurity[$i];
                    $this->Admin  = $this->Admin[$i];
                    if ($_SERVER['REQUEST_METHOD'] == $this->elements[$i][2]) {
                        unset($this->elements);
                        return $this;
                    }
                }
            //}
        }
        return $this;
    }
}

