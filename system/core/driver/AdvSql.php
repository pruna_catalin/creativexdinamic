<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 14/02/2018
 * Time     : 11:08 AM
 */

class AdvSql
{
    protected static $connection = NULL;

    public static function getConnection(){

        include(BASEPATH."/system/config/Database.php");
        if(self::$connection == NULL){
            $dsn = NULL;
            $config['database']['driver'] = "mysql";
            switch ($config['database']['driver']){
                case "dblib":
                    $dsn = "dblib:host=" . $config['database']['hostname'] .":". $config['database']['port'];
                    break;
                case "pgsql":
                    $dsn = "pgsql:host=" . $config['database']['hostname'] .":". $config['database']['port'];
                    break;
                case "sqlite":
                    $dsn = "sqlite:host=" . $config['database']['hostname'] .":". $config['database']['port'];
                    break;
                case "4D":
                    $dsn = "4D:host=" . $config['database']['hostname'] .":". $config['database']['port'];
                    break;
                case "sqlsrv":
                    $dsn = "sqlsrv:host=" .$config['database']['hostname'] .":". $config['database']['port'];
                    break;
                case "oci":
                    $dsn = "oci:host=" . $config['database']['hostname'] .":". $config['database']['port'];
                    break;
                default :
                    $dsn = "mysql:host=" . $config['database']['hostname'] .":". $config['database']['port'].";dbname=".$config['database']['dbname'];
                    break;
            }
            try{
                self::$connection = new \PDO( $dsn, $config['database']['username'], $config['database']['password'],$config['database']['init']) or die("BYE BYE");
            } catch (\PDOException $ex) {
            	throw new \TException("Can't connect to database contact support <a href='mailto:prunacatalin.costin@gmail.com'>Send error :</a>");

            }
        }
        return self::$connection;
    }

    private $_prepare = NULL, $_input = NULL, $_precise = NULL,
        $_array = NULL, $_connection;

    private $_whereCommands = array(), $_sqlCommands = array();

    public function __construct($data = NULL) {
        $this->reset();

        if(is_array($data)) {
            if(isset($data['prepare']) && is_array($data['prepare']))
                $this->_prepare = $data['prepare'];
            if(isset($data['input']) && is_array($data['input']))
                $this->_input = $data['input'];
            if(isset($data['precise']) && is_array($data['precise']))
                $this->_precise = $data['precise'];
            if(isset($data['array']) && is_array($data['array']))
                $this->_array = $data['array'];
        }


        $this->_connection = self::getConnection();
    }
    public function bind($data = NULL){
        if(isset($data) && is_array($data))
            $this->_input = $data;
    }
    public function reset() {
        $this->_sqlCommands = array();
    }

    public function select($select) {
        array_push($this->_sqlCommands, "SELECT ". $select);
        return $this;
    }

    public function update($update) {
        array_push($this->_sqlCommands,  "UPDATE ". $update);
        return $this;
    }

    public function insert($insert) {
        array_push($this->_sqlCommands,  "INSERT INTO ". $insert);
        return $this;
    }

    public function delete($data) {
        array_push($this->_sqlCommands,  "DELETE ".$data." ");
        return $this;
    }

    public function from($from) {
        array_push($this->_sqlCommands,  " FROM ".$from);
        return $this;
    }

    public function set($set) {
        array_push($this->_sqlCommands, " SET ". $set);
        return $this;
    }

    public function columns($columns) {
        array_push($this->_sqlCommands, " (". $columns.") ");
        return $this;
    }

    public function values($values) {
        array_push($this->_sqlCommands, " VALUES (". $values .") ");
        return $this;
    }
    public function where($where, $force = false) {
        if(!in_array("::WHERE::", $this->_sqlCommands)) {

            array_push($this->_sqlCommands, "::WHERE::");
            if($force)
                array_push($this->_whereCommands, "1=1");
            else
                array_push($this->_whereCommands, $where);
        }
        if(!$force && in_array("1=1", $this->_whereCommands)) {
            $this->_whereCommands = array($where);

        }
        return $this;
    }

    public function orWhere($where) {
        if(in_array("::WHERE::", $this->_sqlCommands) && !in_array("1=1", $this->_whereCommands))
            array_push($this->_whereCommands, " OR ".$where);
        return $this;
    }

    public function andWhere($where) {
        if(in_array("::WHERE::", $this->_sqlCommands) && !in_array("1=1", $this->_whereCommands))
            array_push($this->_whereCommands, " AND ".$where);
        return $this;
    }

    public function groupBy($group) {
        $this->where("", true);
        array_push($this->_sqlCommands, "  GROUP BY ".$group);
        return $this;
    }

    public function having($having) {
        array_push($this->_sqlCommands, "  HAVING ".$having);
        return $this;
    }

    public function orderBy($order) {
        $this->where("", true);
        array_push($this->_sqlCommands, "  ORDER BY ".$order);
        return $this;
    }

    public function limit($limit) {
        $this->where("", true);
        array_push($this->_sqlCommands, "  LIMIT ".$limit);
        return $this;
    }
    public function on($on) {
        array_push($this->_sqlCommands, "  ON ".$on);
        return $this;
    }
    public function rightJoin($rightJoin) {
        array_push($this->_sqlCommands, "  RIGHT JOIN ".$rightJoin);
        return $this;
    }
    public function leftJoin($leftJoin) {
        array_push($this->_sqlCommands, "  LEFT JOIN ".$leftJoin);
        return $this;
    }
    public function Join($Join) {
        array_push($this->_sqlCommands, "   JOIN ".$Join);
        return $this;
    }
    public function prepare() {

        if(is_null($this->_prepare)) return;

        foreach($this->_prepare as $index => $value) {
            switch ($index) {
                case ':SELECT': $this->select($value); break;
                case ':INSERT': $this->insert($value); break;
                case ':UPDATE': $this->update($value); break;
                case ':SET': $this->set($value); break;
                case ':COLUMNS': $this->columns($value); break;
                case ':VALUES': $this->values($value); break;
                case ':FROM': $this->from($value); break;

                case ':LEFTJOIN': $this->leftJoin($value); break;
                case ':RIGHTJOIN': $this->rightJoin($value); break;
                case ':ON': $this->on($value); break;

                case ':WHERE': $this->where($value); break;
                case ':ORWHERE': $this->orWhere($value); break;
                case ':ANDWHERE': $this->andWhere($value); break;
                case ':GROUPBY': $this->groupBy($value); break;
                case ':ORDERBY': $this->orderBy($value); break;
                case ':LIMIT': $this->limit($value); break;

            }
        }

    }

    public function sql() {
        $sql = "";
        foreach($this->_sqlCommands as $command) {

            if($command == '::WHERE::')
                $sql .= " WHERE ". implode(" ",$this->_whereCommands);
            else
                $sql .= $command. " ";
        }

        //echo $sql;
        return $sql;
    }

    public function execute() {
        $anon_class = new class { static function fetch ($fetch_style = null, $cursor_orientation = \PDO::FETCH_ORI_NEXT, $cursor_offset = 0) { return FALSE; } };

        try{
            $request = $this->_connection->prepare($this->sql());

            if(!empty($this->_input)) foreach($this->_input as $i => $value)
                $request->bindValue($i, $value, \PDO::PARAM_STR);
            if(!empty($this->_precise)) foreach($this->_precise as $i => $value)
                $request->bindValue($i, intval($value), \PDO::PARAM_INT);
            if(!empty($this->_array))
                for($i = 0; $i < sizeof($this->_array); $i++)
                    $request->bindParam($i+1, $this->_array[$i], \PDO::PARAM_INT);

            $request->execute();
            $errInfo = $request->errorInfo();

            if(intval($errInfo[0]) != 0) {
                \Debugger::Log($this->sql(), 'error',"SQL");
                \Debugger::Log($errInfo, 'error');
            }
        }catch (\Exception $ex ){
            $request = $anon_class;

            \Debugger::Log($this->sql(), 'error',"SQL");
            \Debugger::Log($ex->errorInfo, 'error');
        }

        return $request;


    }
    /**
     * Returns the ID of the last inserted row or sequence value
     * NOTE: This method may not return a meaningful or consistent result across different PDO drivers, because the underlying database may not even support the notion of auto-increment fields or sequences.
     *
     * @param 	string $name 	Name of the sequence object from which the ID should be returned.
     *
     * @return string	If a sequence name was not specified for the name parameter, PDO::lastInsertId returns a string representing the row ID of the last row that was inserted into the database.
     */
    public function lastInsertId() {
        return $this->_connection->lastInsertId();
    }
    /**
     * Returns the number of rows affected by the last SQL statement
     *
     *
     * @return int	Returns the number of rows.
     */
    public function rowCount(){
        return $this->_connection->rowCount();
    }
    public function getDatabase(){
        global $config;
        return $config['database']['name'];
    }
    public  static function SwitchResult($result){
        switch ($result) {
            case "lazy":
                return \PDO::FETCH_LAZY;
            case "object":
                return \PDO::FETCH_OBJ;
            case "num" :
                return \PDO::FETCH_NUM;
            case "assoc" :
                return \PDO::FETCH_ASSOC;
            default :
                return \PDO::FETCH_OBJ;
        }
    }
}