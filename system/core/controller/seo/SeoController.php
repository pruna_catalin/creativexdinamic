<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 11/24/2018
 * Time: 8:58 PM
 */
namespace UMP\System\Core\Controller\Seo;
use UMP\System\Core\View\Render;
use UMP\System\I18N\Language;
use UMP\System\Tools\Storage;
use UMP\System\Tools\Yoast\Api_Libs\Cx_Api_Libs;
class SeoController
{
	private $params = [];
	private $storage = [];
	private $client = [];
	public static $gsc = array(
		'application_name' => 'Yoast SEO',
		'client_id'        => '395430892738-ushj8aced0cji2j4bkq6bda6felaigb9.apps.googleusercontent.com',
		'client_secret'    => 'c2kYgOwMhk1emWxQ3NaA8wOi',
		'redirect_uri'     => 'urn:ietf:wg:oauth:2.0:oob',
		'scopes'           => array( 'https://www.googleapis.com/auth/webmasters' ),
	);
	public function __construct($params)
	{
		try {
			$gg = new Cx_Api_Libs( '2.0' );
		}catch ( TException $exception ) {
			\Debugger::Log($exception,"Cx Api Libs");
		}
		$this->params = $params;
		$this->storage = new Storage();
	}
	public function actionView(){

		$this->client = new Cx_google_client( self::$gsc, 'wpseo-gsc', 'https://www.googleapis.com/webmasters/v3/' );
		$view = new Render("seo/view_seo",["body_table"=>"","test1"=>"Val1"]);
		$view->folder("UMP");
		return  $view;
	}
}