<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 22/05/2018
 * Time     : 10:11 AM
 */

namespace UMP\System\Core\Controller;


class ReflectionController {
    private $method = NULL;
    private $class  = NULL;

    ///////////MAPPING CLASS PROPERTIES////////////
    private $_classPropertiesStatic     = array();
    private $_classPropertiesPublic     = array();
    private $_classPropertiesPrivate    = array();
    private $_classPropertiesProtected  = array();
    ///////////MAPPING ALL FUNCTIONS////////////
    private $_functionStatic            = array();
    private $_functionPublic            = array();
    private $_functionPrivate           = array();
    private $_functionFinal             = array();
    private $_functionAbstract          = array();
    private $_functionProtected         = array();
    private $_functionMagic				= array();
    ///////////// COMMENT FUNCTION ///////////////
    private $_functionComment           = array();
    //////////////////END//////////////////////

    public function __construct($class,$method = NULL){
        $this->method = $method;
        try{
            $this->class = new \ReflectionClass($class);

            $this->__ClassProperties();
        }catch (\ReflectionException $exception){
            throw  new \TException($$exception);
        }

    }
    public function getClassName(){
        return $this->class->getName();
    }
    public function actionView($type){
        if($type == "methods"){
            return [
            			[
            				"type"=>"public",
							"list"=>$this->_functionPublic,
						],
						[
            				"type"=>"static",
							"list"=>$this->_functionStatic
						],
						[
            				"type"=>"private",
							"list"=>$this->_functionPrivate
						],
						[
            				"type"=>"final",
							"list"=>$this->_functionFinal
						],
						[
            				"type"=>"abstract",
							"list"=>$this->_functionAbstract
						],
						[
            				"type"=>"protected",
							"list"=>$this->_functionProtected
						]
				];
        }
    }
    private function __ClassProperties(){
        $this->_classPropertiesStatic    = array($this->class->getName() => $this->class->getProperties(\ReflectionMethod::IS_STATIC));
        $this->_classPropertiesPublic    = array($this->class->getName() => $this->class->getProperties(\ReflectionMethod::IS_PUBLIC));
        $this->_classPropertiesPrivate   = array($this->class->getName() => $this->class->getProperties(\ReflectionMethod::IS_PRIVATE));
        $this->_classPropertiesProtected = array($this->class->getName() => $this->class->getProperties(\ReflectionMethod::IS_PROTECTED));
        $this->__FunctionsClass();
    }
    private function __FunctionsClass(){
        $this->_functionFinal         =   $this->class->getMethods(\ReflectionMethod::IS_FINAL);
        $this->_functionStatic        =   $this->class->getMethods(\ReflectionMethod::IS_STATIC);
        $this->_functionPublic        =   $this->class->getMethods(\ReflectionMethod::IS_PUBLIC);
        $this->_functionPrivate       =   $this->class->getMethods(\ReflectionMethod::IS_PRIVATE);
        $this->_functionAbstract	  =   $this->class->getMethods(\ReflectionMethod::IS_ABSTRACT);
        $this->_functionProtected     =   $this->class->getMethods(\ReflectionMethod::IS_PROTECTED);
        $this->__DocCommentFunctions();
    }
    private function __DocCommentFunctions(){
        $list_func_static       = $this->_functionStatic;
        $list_func_public       = $this->_functionPublic;
        $list_func_private      = $this->_functionPrivate;
        $list_func_final        = $this->_functionFinal;
        $list_func_abstract     = $this->_functionAbstract;
        $list_func_protected    = $this->_functionProtected;
        $this->_functionComment[$this->class->getName()] = $this->class->getDocComment();
        foreach($list_func_static as $key => $val){
            $method  = new \ReflectionMethod($this->class->getName(), $val->name);
            $comment =  $this->class->getMethod($val->name)->getDocComment();
            $this->_functionComment[$val->name] = array("params"=> $method->getParameters(),"comments"=>$comment);

        }
        foreach($list_func_public as $key => $val){
            $method  = new \ReflectionMethod($this->class->getName(), $val->name);
            $comment =  $this->class->getMethod($val->name)->getDocComment();
            $this->_functionComment[$val->name] = array("params"=> $method->getParameters(),"comments"=>$comment);
        }
        foreach($list_func_private as $key => $val){
            $method  = new \ReflectionMethod($this->class->getName(), $val->name);
            $comment =  $this->class->getMethod($val->name)->getDocComment();
            $this->_functionComment[$val->name] = array("params"=> $method->getParameters(),"comments"=>$comment);
        }
        foreach($list_func_final as $key => $val){
            $method  = new \ReflectionMethod($this->class->getName(), $val->name);
            $comment =  $this->class->getMethod($val->name)->getDocComment();
            $this->_functionComment[$val->name] = array("params"=> $method->getParameters(),"comments"=>$comment);
        }
        foreach($list_func_abstract as $key => $val){
            $method  = new \ReflectionMethod($this->class->getName(), $val->name);
            $comment =  $this->class->getMethod($val->name)->getDocComment();
            $this->_functionComment[$val->name] = array("params"=> $method->getParameters(),"comments"=>$comment);
        }
        foreach($list_func_protected as $key => $val){
            $method  = new \ReflectionMethod($this->class->getName(), $val->name);
            $comment = $this->class->getMethod($val->name)->getDocComment();
            $this->_functionComment[$val->name] = array("params"=> $method->getParameters(),"comments"=>$comment);
        }
        #\Debugger::Log($this->_functionComment);die;

    }
    public function getReflectionMethod($method,$return= false){
        $method  =  new \ReflectionMethod($this->class->getName(), $method);
        return $this->__getCodeBody($method);
    }
    private function __getCodeBody(\ReflectionMethod $reflectionMethod)
    {
        $reflectionClass = $reflectionMethod->getDeclaringClass();
        $length = $reflectionMethod->getEndLine() - $reflectionMethod->getStartLine();
        $lines = file($reflectionClass->getFileName());
        $code = join(PHP_EOL, array_slice($lines, $reflectionMethod->getStartLine() - 1, $length + 1));
        return  $code;
    }

//    public function test(){
//        if(){
//            \Debugger::Log("Yes is public");
//        }
//    }
}