<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 02/05/2018
 * Time     : 2:03 PM
 */
namespace UMP\System\Core\Controller\Hash;
use UMP\System\Core\View\Render;
use UMP\System\Tools\Storage;
defined('KEY') 	OR define('KEY', substr('602d9030244c85eb9176d04a284d08cf', 0, 24));
defined('IV')  	OR define('IV', sprintf("%c%c%c%c%c%c%c%c",0x01,0x02,0x03,0x04,0x05,0x06,0x01,0x02));
defined('PAD') 	OR define('PAD', 16);
defined('ALGO')   OR define('ALGO', "tripledes");
defined('MODE')   OR define('MODE', "cbc");

class HashController
{
    private $storage = "";
    private $params = [];
    public function __construct($params)
    {
        $this->storage = new Storage();
        $this->params = $params;

    }
    public function actionHash(){
        $result_encrypt = "";
//        \Debugger::Log("SELECT * FROM test as t1 left join test2 as t2 on t1.id = t2.id_parent where t1.id =1 ","TEst SQL","SQL");
        if($this->params->Method == "POST") {
            if(isset($this->params->Args['Model']['encrypt'])){
                $result_encrypt = $this->encrypt($this->params->Args['Model']['crypt_text']);
            }else{
                $result_encrypt = rtrim($this->decrypt($this->params->Args['Model']['crypt_text']));
            }
        }else{

        }
       // echo $result_encrypt;
        $view = new Render("crypt/encrypt_decrypt",["result_encrypt"=>$result_encrypt,"valueText"=>
			(isset($this->params->Args['Model']['crypt_text'])?$this->params->Args['Model']['crypt_text'] : "")
		]);
        return $view;
    }
    /*
    * Decrypt data for call
    */

  public function decrypt($message) {
        if(strlen($message) % 2 == 0){
            try{
                 $data = openssl_decrypt(hex2bin($message), 'des-ede3-cbc', KEY,  OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING, IV);
                if(strlen(trim($data)) > 0){
                    return trim($data);
                } else{
                    return $message;
                }
            }catch(Exception $x){}
           
        }else{
            return $message;
        }


    }
    /*
     * Encrypt data for update/insert database GDPR protocol
     */
    public function encrypt($message = '') {
        if (ctype_xdigit($message)) {
            $data = $message;
        }else{
            if (($l = (strlen($message) & 15)) > 0) { $message .= str_repeat(chr(0), 16 - $l); }
            $data = bin2hex(openssl_encrypt($message, 'des-ede3-cbc', KEY,  OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING, IV));
        }

        return $data;
    }
}