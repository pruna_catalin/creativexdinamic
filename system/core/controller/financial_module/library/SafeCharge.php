<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 26/04/2018
 * Time     : 4:41 PM
 */

namespace UMP\System\Core\Controller\FinancialModule\Library;

class SafeCharge
{
    private $errorResult = array(1001 => "Invalid expiration date.",1002 => "Expiration date too old.",1101 => "Invalid card number (alpha numeric).",1102 => "Invalid card number (digits count).",1103 => "Invalid card number (MOD 10).",1201 => "Invalid amount.",1202 => "Invalid currency.",1104 => "Invalid CVV2.",1105 => "Auth Code/Trans ID/Credit card number mismatch.",1106 => "Credit amount exceed total charges.",1107 => "Cannot credit this credit card company.",1108 => "Illegal interval between auth and force.",1109 => "Not allowed to process this credit card company.",1110 => "Unrecognized credit card company.",1111 => "This transaction was charged back.",1112 => "Sale/Settle was already credited.",1113 => "Terminal is not ready for this credit card company.",1114 => "Black listed card number.",1115 => "Illegal BIN number.",1116 => "<Custom Fraud Screen Filter>.",1118 => "'N' cannot be a positive CVV2 Reply.",1121 => "CVV2 check is not allowed in Credit/Settle/Void.",1124 => "Credits total amount exceeds restriction.",1125 => "Format Error.",1126 => "Credit amount exceeds ceiling.",1127 => "Limit exceeding amount.",1128 => "Invalid transaction type code.",1129 => "General filter error.",1130 => "Bank required fields are missing or incorrect.",1131 => "This transaction type is not allowed for this bank.",1132 => "Amount exceeds bank limit.",1133 => "GW required fields are missing.",1135 => "Only one credit per sale is allowed.",1136 => "Mandatory fields are missing.",1137 => "Credit count exceeded credit card restriction.",1138 => "Invalid credit type.",1139 => "This card is not supported in the OCT program.",1140 => "Card must be processed in the Gateway.",1141 => "Transaction type is not allowed.",1143 => "Country does not match ISO Code.",1144 => "Must provide UserID in a Rebill transaction.",1145 => "Your Rebill profile does not supprt this transaction type.",1146 => "Void is not allowed due to customer credit card restriction.",1147 => "Invalid account number.",1148 => "Invalid cheque number.",1149 => "Account Number/Trans ID mismatch.",1150 => "UserID/Trans Type /Trans ID mismatch.",1151 => "Transaction does not exist in the rebill system.",1152 => "Transaction was already canceled.",1153 => "Invalid bank code(digits count).",1154 => "Invalid bank code (alpha numeric).",1156 => "Debit card required fields are missing or incorrect.",1157 => "No update parameters were supplied.",1155 => "3D Related transaction is missing or incorrect.",1158 => "3D PARes value is incorrect.",1159 => "State does not match ISO Code.",1160 => "Invalid bank code (checksum digit).",1161 => "This bank allows only 3 digits in CVV2.",1162 => "Age verification failed.",1163 => "Transaction must contain a Card/Token/Account.",1164 => "Invalid token.",1165 => "Token mismatch.",1166 => "Invalid email address.",1167 => "Transaction already settled.",1168 => "Transaction already voided.",1169 => "sg_ResponseFormat field is not valid.",1170 => "Version field is missing or incorrect 1171 Issuing country is invalid.",1172 => "Phone is missing or format error.",1173 => "Check number is missing or incorrect.",1174 => "Birth date format error.",1175 => "Zip code format error.",1180 => "Invalid start date.",1176 => "Cannot void an auth transaction.",1177 => "Cannot void a credit transaction.",1178 => "Cannot void a void transaction.",1179 => "Cannot perform this void.",1181 => "Merchant Name is too long (>25).",1182 => "Transaction must be sent as 3D Secure.",1183 => "Account is not 3D enabled.",1184 => "Transaction 3D status is incorrect.",1185 => "Related transaction must be of type 'AUTH3D'.",1186 => "Related transaction must be 3D authenticated.",1187 => "Country does not support the CFT program.",1188 => "Invalid token ID.",1190 => "Risk processor error.",1191 => "RiskOnly is supported from version 4.",1192 => "AGV processor error.",1193 => "AGVOnly is supported from version 4.",1194 => "Age verification mismatch.",1195 => "Requested acquirer is unrecognized.",1196 => "sale transaction isn't processed on acquirer side yet.",1204 => "Payment method is not supported.",1203 => "IBAN does not match country code.",1200 => "invalid issuing bank country code.",1199 => "Envoy payout transaction requires credit transaction of type 1.",1198 => "Credit card details are not allowed for the given payment method.",1197 => "Input data is not sufficient.",2001 => "The requested acquirer bank is temporarily unavailable.",2002 => "ApmRisk is disabled.",1205 => "APM risk processor error");

    private $transactionMessage = array("APPROVED","DECLINED","ERROR");



    /*
     * Mandatory params for login
     */
    public  $devMode            = false;
    private $ClientLoginID      = "";
    private $ClientPassword     = "";
    private $_urlSandbox        = "https://process.sandbox.safecharge.com/service.asmx/Process?";
    private $_urlLive           = "https://process.safecharge.com/service.asmx/Process?";

    /*
    * Reponse Format
    */
    private $_reponseFormat     = 4;
    private $_version           = "4.1.0";
    private $_typeCredit        = "Credit";
    private $_creditType        = 1; // 1 Payout ,2 refound
    private $_data               = [];
    
    /*
     * Mandatory params for client data
     */

    public $firstName       = NULL;
    public $lastName        = NULL;
    public $country         = NULL;
    public $email           = NULL;
    public $cardNumber      = NULL;
    public $expMonth        = NULL;
    public $expYear         = NULL;
    public $ccToken         = NULL;
    public $amount          = NULL;
    public $currency        = NULL;
    public $transactionId   = NULL;


    /*
     *
     * */


    public function callSafeCharge(){
        $result = ["status"=>true,"message"=>"Success","transasctionID"=>$this->transactionId,"reason"=>""];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, ($this->devMode) ? $this->_urlSandbox : $this->_urlLive);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->_data);
        $page = curl_exec($ch);
        $info = curl_getinfo($ch);
        $response = $info['http_code'];
        if ($response == '200' && $info["content_type"] == "text/xml; charset=utf-8") {
            $response_xml = simplexml_load_string($page);
            $code = (string)($response_xml->ErrCode);
            if(array_key_exists($code,$this->errorResult)){
                return $result = ["status"=>false,"message"=>$this->errorResult[$code],"transasctionID"=>0,"reason"=>""];
            }else{
                if($code == "APPROVED")
                    return $result;
                else if($code == "DECLINED")
                    return $result = ["status"=>false,"message"=>"DECLINED","transasctionID"=>0,"reason"=>$response_xml->ReasonCodes[0]->Reason];
                else if($code == "ERROR")
                    return $result = ["status"=>false,"message"=>"DECLINED","transasctionID"=>0,"reason"=>$response_xml->Reason];
            }
        }
        return $result;
    }



}