<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 27/04/2018
 * Time     : 9:27 AM
 */

namespace UMP\System\Core\Controller\FinancialModule;


class Config
{
    protected static $connection = NULL;
    public $_connection = NULL;


    public static function getConnection(){
        include(BASEPATH . "/config/Database.php");
        if(self::$connection == NULL){
            $dsn = "dblib:host=" . $config['database']['hostname'] .":". $config['database']['port'];
            try{
                self::$connection = new \PDO( $dsn, $config['database']['username'], $config['database']['password'],array(
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                    \PDO::ATTR_EMULATE_PREPARES => false,
                    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'
                )) or die("BYE BYE");
            } catch (\PDOException $ex) {
                throw new \TException("Can't connect to database contact support <a href='mailto:prunacatalin.costin@gmail.com'>Send error :</a>");

            }
        }
        return self::$connection;
    }
}