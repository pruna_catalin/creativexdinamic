<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 27/04/2018
 * Time     : 9:33 AM
 */

namespace UMP\System\Core\Controller\FinancialModule;
use UMP\System\Core\Controller\FinancialModule\Library\SafeCharge;

defined('KEY') 	OR define('KEY', substr('602d9030244c85eb9176d04a284d08cf', 0, 24));
defined('IV')  	OR define('IV', sprintf("%c%c%c%c%c%c%c%c",0x01,0x02,0x03,0x04,0x05,0x06,0x01,0x02));
defined('PAD') 	OR define('PAD', 16);
defined('ALGO')   OR define('ALGO', MCRYPT_TRIPLEDES);
defined('MODE')   OR define('MODE', MCRYPT_MODE_CBC);

class FinancialModule extends Config
{
    public  $params = array("method"=>"","data"=>array());
    private $_connection = NULL;
    private $mandatoryParams = array(
                                        "id_utente"=>NULL,"ip"=>NULL,"id_movimento"=>NULL,"id_metodo"=>NULL,
                                        "id_protocollo"=>1,"identificativo"=>NULL,"identificativo_carta"=>'000000000000000000000000000000',
                                        "importo"=>NULL,"scalato_saldo_prelevabile"=>NULL,"scalato_bonus"=>NULL,
                                        "data"=>NULL,"id_stato_movimento"=>NULL,"DA_PROCESSARE"=>0,"USED_TO_UNLOCK"=>0
                                        );
    /*
     * $object = new FinancialModule();
     * $object->params['method'] = 'safecharge';
     * $object->params['data'] = array("name"=>"nume");
     * $result = $object->controllerFinancial();
     *
     */
    //type of params $params['method']['data'] = []
    public function __construct(){
        $this->_connection = parent::getConnection();
    }

    private function validateMandatory($params = array()){
        $result = "Passed";
        foreach($params as $k => $v){
            if(array_key_exists($k,$this->mandatoryParams)){
                $this->mandatoryParams[$k] = $v;
            }
        }
        $check = $this->validate_mandatoryParams();
        if(!$check['status']){
            return "Column : ".$check['param']. " can't be empty or null";
        }
        return $result;
    }

    private function validate_mandatoryParams(){
        $result = true;
        foreach($this->mandatoryParams  as $key => $value){
            if(is_null($value) || $value == ""){
                return array("status"=>false,"param"=>$key);
            }
        }
    }

    public function controllerFinancial(){
        if(isset($this->params['data'])){
            $result = $this->validateMandatory($this->params['data']);
            if($result == "Passed"){
                $insertTransaction = false;
                if(isset($this->params['method'])){
                    //ALL METHODS (ADD HERE Methods Payment )
                    if($this->params['method'] == "safecharge"){
                        $method = new SafeCharge();
                        $method->firstName      = $this->params['data']['firstname'];
                        $method->lastName       = $this->params['data']['lastname'];
                        $method->country        = $this->params['data']['country'];
                        $method->email          = $this->params['data']['email'];
                        $method->cardNumber     = $this->params['data']['cardNumber'];
                        $method->expMonth       = $this->params['data']['exp_month'];
                        $method->expYear        = $this->params['data']['exp_year'];
                        $method->ccToken        = $this->params['data']['cc_token'];
                        $method->amount         = $this->params['data']['amount'];
                        $method->currency       = $this->params['data']['currency'];
                        $method->transactionId  = $this->params['data']['transaction_id'];
                        // ["status"=>true,"message"=>"Success","transasctionID"=>$this->transactionId,"reason"=>""]
                        $result = $method->callSafeCharge();
                        if($result['status']){
                            $insertTransaction = true;
                        }else{
                            return array("status"=>false,"message"=>$result['message']);
                        }
                    }
                    //END METHODS
                }
                //IF $insertTransaction = true try insert in web_movimenti
                if($insertTransaction){
                    $idTransaction = $this->insertTransaction($this->params['data']);
                    if($idTransaction){
                        return array("status"=>true,"message"=>"Transaction Success");
                    }else{
                        return array("status"=>false,"message"=>"Insert Transaction Faild");
                    }
                }
            }else{
                return $result;
            }
        }else{
            return "No data";
        }
    }

    private function insertTransaction($player = []){
        if(sizeof($player) != 0){
            $valueParams = "";

            $query  = "INSERT INTO `netbet_pgad`.`web_movimenti` (id_utente,ip,id_movimento,id_metodo,id_protocollo,id_gad_bonus,id_sito_operazione,id_transazione,id_saldo,identificativo,identificativo_carta,importo,scalato_saldo_prelevabile,scalato_bonus,data,id_stato_movimento,id_substato_movimento,data_ricezione,identificativo_rit,id_operatore,note_interne,note,fk_bonus,DA_PROCESSARE,USED_TO_UNLOCK,ricevuta_aams,id_transazione_aams) VALUES(".$valueParams.")";


        }
    }

    /*
    * Decrypt data for call
    */
    private function decrypthHash($what = '') {
        $aoe = base64_decode($what);
        $asx = mcrypt_decrypt(MCRYPT_TRIPLEDES, KEY, $aoe, MCRYPT_MODE_CBC, IV);
        $axe = rtrim($asx, chr(0x00));
        return $asx;
    }
    /*
     * Encrypt data for update/insert database GDPR protocol
     */
    private function encrypt($what = '') {
        $what = mb_strtoupper($what, 'utf-8');
        $rpad = PAD - strlen($what);
        // if the number is more than zero
        if($rpad > 0)
        {
            $what .= str_repeat(chr(0x00), $rpad);
        }
        $crypt = mcrypt_encrypt(MCRYPT_TRIPLEDES, KEY, $what, MCRYPT_MODE_CBC, IV);
        return base64_encode($crypt);
    }
}