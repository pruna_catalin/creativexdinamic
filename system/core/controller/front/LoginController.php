<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 12/23/2018
 * Time: 4:41 PM
 */

namespace UMP\System\Core\Controller\Front;
use UMP\System\Core\View\Render;
use UMP\System\I18N\Language;
use UMP\System\Tools\Storage;
use UMP\System\Config\Constants;
use UMP\System\Core\Rules\Validation;

class LoginController
{
	private $storage = "";
	private $params = [];
	public function __construct($params)
	{
		$this->storage = new Storage();
		$this->params = $params;

	}
	private function rules(){
		$rules = new Validation();
		$element = [
			"username"=>["required"=>true],
			"name"=>["required"=>true,"unique"=>true],
			"password"=>["required"=>true,"min"=>5,"regex"=>"\w+"]
		];
		return $rules->formValidation($element,$this->params->Args['Model']);
	}

	public  function IsLogin()	{
		return ($check = $this->storage->session->get("Login")) ? (($check == "XYZ") ? true : false) : false;
	}
	public  function actionLogin(){
		$language = new \Language();
		$checkRule = $this->rules();
		if($checkRule['status']){
			\Utils::redirect("index",10);
		}
	}
}