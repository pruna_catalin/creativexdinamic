<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 12/29/2018
 * Time: 4:13 PM
 */

namespace UMP\system\core\controller\front;
use UMP\System\Config\Constants;
use UMP\System\Tools\Storage;
use UMP\System\Core\View\Render;

class ChatController
{
	private $params = [];
	private $storage = [];
	public function __construct($params)
	{
		$this->params = $params;
		$this->storage = new Storage();
	}
	public function actionView(){
		$message = "Little robot is here to help you  please type <font color='red'>!help</font> to start  speak with him";
		$view =  new Render("chat/chat",["front_theme_assets"=>Constants::FRONT_THEME_ASSETS,	"live_url" => Constants::LIVE_URL,"action" => $this->params->Action,
			"controller" => $this->params->Controller]);
		FillJSON(["success"=>true,"content"=>$view->render(),"message"=>$message]);
	}
	private function helper($typeMessage){
		$message = "";
		if($typeMessage == "!help"){
			$message = "Available commands <br><font color='red'>!packs</font> <br> <font color='red'>!faq</font>  ";
		}
		return $message;

	}
	public function actionReceiveMessage(){
		//$this->params->Args['Model']['message']
		$automaticMessage = $this->helper($this->params->Args['Model']['message']);
		if($automaticMessage != "")
			FillJSON(["success"=>true,"message"=>$automaticMessage]);exit;
	}
}