<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 7/22/2018
 * Time: 5:42 PM
 */
namespace UMP\System\Core\Controller\Front;
use UMP\System\Config\Constants;
use UMP\System\Tools\Storage;
use UMP\System\Core\View\Render;
class IndexController
{
	private $params = [];
	private $storage = [];
	public function __construct($params)
	{
		$this->params = $params;
		$this->storage = new Storage();
	}
	private function MenuTop(){
		\DAO::Menu("OrderBy","order_list ASC");
		$list = \DAO::Menu("FindAll","type_menu='header'");
		$menu = "";
		foreach($list as $menu_item){
			$view = new Render("menu/header", ["live_url" => Constants::LIVE_URL,"active"=>"","page_url"=>$menu_item->url_seo,"page_name"=>$menu_item->name]);
			$menu .= $view->render();
		}

		return $menu;
	}
	private function MenuBottom(){
		\DAO::Menu("OrderBy","order_list ASC");
		$list = \DAO::Menu("FindAll","type_menu='footer'");
		$menu = "";
		foreach($list as $menu_item){
			$view = new Render("menu/footer", ["live_url" => Constants::LIVE_URL,"active"=>"","page_url"=>$menu_item->url_seo,"page_name"=>$menu_item->name]);
			$menu .= $view->render();
		}

		return $menu;
	}
	public function Init($content){
		$timer = new \Timer();
		if ($content instanceof Render) { // if is return view instance echo HTML content
			$index = new Render("index_parser", [
				"front_theme_assets" => Constants::FRONT_THEME_ASSETS,
				"live_url" => Constants::LIVE_URL,
				"page_time" => $timer->FormatTimer(),
				"body" => $content->render(),
				"menuTop"=>$this->MenuTop(),
				"menuBottom"=>$this->MenuBottom(),
				"login" => $this->loginForm(),
				"action" => $this->params->Action,
				"controller" => $this->params->Controller,
				"keyList" => $this->params->KeyList,
				"storage"=>$this->storage
			]);
			$index->folder($content->folder());
			Fill($index->render()); //  HTML content
		} else {
			if (\Utils::JsonTest($content)) { // if response is json will send output JSON
				FillJSON($content);
			} else {
				$index = new Render("index_parser", [
					"front_theme_assets" => Constants::FRONT_THEME_ASSETS,
					"live_url" => Constants::LIVE_URL,
					"page_time" => $timer->FormatTimer(),
					"body" => $content,
					"menuTop"=>$this->MenuTop(),
					"menuBottom"=>$this->MenuBottom(),
					"login" => $this->loginForm(),
					"action" => $this->params->Action,
					"controller" => $this->params->Controller,
					"keyList" => $this->params->KeyList,
					"storage"=>$this->storage
				]);
				Fill($index->render()); //  HTML content
			}
		}
	}
	private function loginForm(){
		$content = "";
		if(!$this->storage->session->get('Login')){
			$view =  new Render("login/login",["front_theme_assets"=>Constants::FRONT_THEME_ASSETS]);
			$content .= $view->render();
		}
		return $content;
	}
	public function actionView(){
		//$this->generateMenu();
		$content = "";
		//\Utils::download("https://jquery.org/jquery-wp-content/themes/jquery/images/stackpath.png",false,true);
		//\Utils::upload($_FILES,["test/","mata/",""])
		//$this->storage->session->set(["name"=>"Login","value"=>"letmein"]);
		//$this->storage->session->set(["name"=>"Username","value"=>"Vasile Mure"]);
		$this->storage->session->delete('Login');

		$view =  new Render("dashboard/slider",["front_theme_assets"=>Constants::FRONT_THEME_ASSETS]);
		$content .= $view->render();
		$view =  new Render("dashboard/banner",["front_theme_assets"=>Constants::FRONT_THEME_ASSETS]);
		$content .= $view->render();
		$view =  new Render("dashboard/popular",["front_theme_assets"=>Constants::FRONT_THEME_ASSETS]);
		$content .= $view->render();


		return $content;
	}

}