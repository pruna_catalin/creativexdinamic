<?php
/**
 * Created by PhpStorm.
 * User: eugen.b
 * Date: 24/05/2018
 * Time: 2:44 PM
 */
namespace UMP\System\Core\Controller\Game;
use UMP\System\Core\View\Render;
use UMP\System\Tools\Storage;

class GameController{
    private $params = [];
    private $storage = [];
    public function __construct($params)
    {
        $this->params = $params;
        $this->storage = new Storage();
    }
    public function actionView(){
//        $dao = \DAO::Users('FindAll',"");

        $titleText = "Title Text";
        return new Render("game/game",["titleText"=>"","data"=>"","data2"=>""]);
    }
    private function renderTable($rows){
        $viewData = "";
        foreach ($rows as $date){
            $tmp = new Render("game/elements",["user"=>$date]);
            $viewData .=$tmp->render();
        }
        return $viewData;
    }
}