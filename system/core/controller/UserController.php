<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 14/02/2018
 * Time     : 2:55 PM
 */

namespace UMP\System\Core\Controller;
use UMP\System\Core\View\Render;
use UMP\System\Tools\Storage;
class UserController
{
    private $params = [];
    private $storage = [];
    public function __construct($params)
    {
        $this->params = $params;
        $this->storage = new Storage();
    }

    /**
     * @return Render
     */
    public function actionList(){



      # $objecLogin = new \LoginAdmin();
        #\Debugger::Log($objecLogin->IsLogin());
       # \Debugger::Log($this->params);
       	$model = \Model::Users('');
        $objectReflection = new ReflectionController($model);

        #	$dao = \DAO::Users('Find',[]);
        #$model->id = 20;
        #$model->username = "test my";
        #\Debugger::Log($model);
        $cookie = ["name"=>"TestName","value"=>"Test new value without Refresh _1","expire"=>time()+3600];
       # $this->storage->setCookie($cookie);
        #echo AAAA;
        $cookie = ["name"=>"TestName_2","value"=>"TestValue____2","expire"=>time()+3600,"path"=>"/","domain"=>"localhost","secure"=>false,"httpOnly"=>false];
        #$this->storage->setCookie($cookie);
        $cookie = ["name"=>"TestName_4","value"=>"TestValue_____3","expire"=>time()+3600,"path"=>"/","domain"=>"localhost","isHttps"=>false,"httpOnly"=>false];
        #$this->storage->setCookie($cookie);
       # $_SESSION['test'] = "new minimo";
        #\Debugger::Log(session_get_cookie_params());

        $this->storage->cookie->set($cookie);
        #\Debugger::Log($this->storage->cookie->get('TestName_4'));
//        if($check = $this->storage->cookie->get('test')){
//            \Debugger::Log($check['value']);die;
//        }

        #$this->storage->deleteCookie('TestName');
        #\Debugger::Log("Test");

        $list[] = (object)["id"=>1,"name"=>"vasilescu"];
        $list[] = (object)["id"=>2,"name"=>"ion"];
        $list[] = (object)["id"=>3,"name"=>"vas"];
        $list[] = (object)["id"=>4,"name"=>"eugen"];
        $rows_table = "";
        $timer = new \Profile();
        foreach($list as $item){
            $view = new Render("users/rows_table",["id"=>$item->id,"name"=>$item->name]);
           # $view->folder('UMP22');
            $rows_table .= $view->render();
        }
        \Debugger::Log($timer->R());
        $view = new Render("users/list_users",["body_table"=>$rows_table,"test1"=>"Val1"]);
       # \Debugger::Log("SELECT * FROM USERS left join profile on users.id= profile.id WHERE id=1 ","Query 1","SQL");
        #\Debugger::Email("","","Test 1",\Debugger::Log("SELECT * FROM USERS left join profile on users.id= profile.id WHERE id=1 ","Query 1","SQL"));
        #\Debugger::Log("SELECT * FROM USERS left join profile on users.id= profile.id WHERE id=1 ","Query 2","SQL");
        #\Debugger::Email("","","Test new debug","SELECT * FROM USERS left join profile on users.id= profile.id WHERE id=1 ","Query 1","SQL");
        return  $view;
       # \Debugger::Log($this->params->Args);
    }

    public function actionEdit(){
        #Fill("test me");
        #$timer = new \Profile();
        #
        #Fill(AAAAAA);
        #FillJSON(["success","test","tets1"]);
        #FillJSON(["params"=>$this->params]);
		FillJSON(["target"=>[
								//["type"=>"text","data"=>"hello","index"=>"listUsers"],
								["type"=>"html_append","data"=>"Adu ce vreau dupa ce pagina este incarcTA HTML .../........","index"=>"testTitle"]
							]
				]);

		#Fill(true, "success");
		#Fill(["valueTest","aaa","aaaa","aaaa"], "RAW");
		#Fill("<div>aaaa</div>", "aloo");
		#$model = \Model::Users('');
		#$dao = \DAO::Users('Find',[]);
		//return json_encode(["success"=>true,"RAW"=>["valueTest","aaa","aaaa","aaaa"],"aloo"=>"<div>aaaa</div>"]);
        if($this->params->Method == "POST") {
           # \Debugger::Log($this->params->Args);
        }else{

        }
        $view = new Render("users/list_users",["body_table"=>"","test1"=>"Val1"]);
        return $view;
       # \Debugger::Log($timer->D());
    }
    public function actionDelete(){
        if($this->params->Method == "POST"){
            \Debugger::Log($this->params->Args);
        }

    }
}