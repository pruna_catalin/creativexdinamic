<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 14/02/2018
 * Time     : 11:01 AM
 */

namespace UMP\System\Core\Controller;

use UMP\System\Core\Routes\Routes;
use UMP\System\Core\View\Render;
use UMP\System\Config\Constants;
use UMP\System\Tools\Storage;

require_once(BASEPATH . "/system/config/routes.php");

class Controller extends SecurityController
{
	private $params = [];
	private $storage = [];
	private $security = [];

	public function __construct()
	{
		$this->storage = new Storage();
		$this->params = Routes::getInstance()->registerRoute();
		$this->security = new SecurityController($this->params);

	}

	/*
	 * This function will be call by API and receive params json format to send in view
	 * @params = will return by request
	 * @return  if(instanceof of View) ? HTML : (isJson) ? JsonFormat : result of function
	 *
	 */

	public function callController()
	{
		//$this->params = ; // take all routes
		#parent::__construct("controller", $this->params,$this->storage->session->all()); // first param folder controller, secound all session
		if ($this->params->Controller != "" && $_SERVER['REQUEST_METHOD'] == $this->params->Method) { //check exists controller
			if (!empty($this->params->Folder) && $this->params->Folder != "")
				$controllerString = __NAMESPACE__ . "\\" . ucfirst($this->params->Folder) . "\\" . $this->params->Controller; // try to create Class
			else
				$controllerString = __NAMESPACE__ . "\\" . $this->params->Controller; // try to create Class

			$response = new $controllerString($this->params); // try to use action from controller
			$content = $response->{$this->params->Action}(); // call function and return
			if($this->params->Admin){
				$admin = new Admin\IndexController($this->params);
				$admin->Init($content);
			}else{
				$front = new Front\IndexController($this->params);
				$front->Init($content);
			}
		} else {
			$index = new Render("404/page", ["front_theme_assets" => Constants::FRONT_THEME_ASSETS,"live_url" => Constants::LIVE_URL,]);
			Fill($index->render()); // return when route not exists
		}

	}
}

