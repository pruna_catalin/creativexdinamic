<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 19/02/2018
 * Time     : 1:33 PM
 */

namespace UMP\System\Core\Controller;
use UMP\System\Config\Constants;
use UMP\System\Tools\Storage;
use UMP\System\Core\Routes\Routes;
class SecurityController
{
    private $permisions 	= [];
    private $storage 		= [];
    private $params		 	= NULL;
	private $user_agent 	= NULL;
	private $post_fields 	= [];
	public  $headers     	= [];
	private $client_id 		= "creativex";
	private $client_secret 	= "creativex";
	private $url 			= "http://localhost/creativexdinamic/server_api/api.php";
    public function __construct($params){
        $this->storage = new Storage();
        $this->permisions = $this->storage->session->get("permisions");
        $this->params = $params;
		$this->user_agent               = $_SERVER['HTTP_USER_AGENT'];
        $this->checkPermision();
    }
    private function checkPermision(){
        if(isset($this->params)){
        	if(!$this->params->SecurityRoute["security"]) return true;
        	$permision = json_decode($this->callApi()['content']);
            if(isset($this->params->Case)){
                if($permision->error){
                    Fill("Access Denied : ".(!empty($this->params->Case) ? $this->params->Case : "Route Empty"));die;
                }
            }else{
                Fill("Access Denied no case defined");die;
            }
        }else{
            Fill("Access Denied  no route defined");die;
        }
    }
    private function callApi(){
    	$this->post = 1;
		$this->post_fields['params'] = $this->params;
		$this->post_fields['permision'] = $this->permisions;
    	$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT,$this->user_agent);
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSLVERSION, 6);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, $this->client_id.":".$this->client_secret);
		if($this->post){
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->post_fields));

		}
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
		$result = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
		return ['content'=>$result, 'info'=>$info, 'request'=>$this->post_fields];
	}
}