<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 14/02/2018
 * Time     : 2:55 PM
 */

namespace UMP\System\Core\Controller\Admin;
use UMP\System\Core\View\Render;
use UMP\System\I18N\Language;
use UMP\System\Tools\Storage;

class UserController extends \UMP\System\Config\Constants
{
    private $params = [];
    private $storage = [];
    public function __construct($params)
    {
        $this->params = $params;
        $this->storage = new Storage();
    }

    /**
     * @return Render
     */
    public function actionList(){
		$list = [];
//		$model = \Model::Users();
//		$model->id = "asdasd";
		//echo BBBB;
		$list[] = (object)["id"=>1,"name"=>"vasilescu"];
		$list[] = (object)["id"=>2,"name"=>"ion"];
		$list[] = (object)["id"=>3,"name"=>"vas"];
		$list[] = (object)["id"=>4,"name"=>"eugen"];
		$list[] = (object)["id"=>5,"name"=>"beatrice"];

		//phpinfo();die;
//		$this->storage->files->set(
//				[
//					$this->params->file1,
//					$this->params->file2,
//
//				],
//				[
//					"C:\\upload1",
//					"C:\\upload2",
//				],["jpeg","php"]);
//		$this->storage->files->uploadFile();
//


        $rows_table = "";
        foreach($list as $item){
            $view = new Render("users/rows_table",["id"=>$item->id,"name"=>$item->name]);
            $view->folder("UMP");
            $rows_table .= $view->render();
        }
        $view = new Render("users/list_users",["body_table"=>$rows_table,"test1"=>"Val1"]);
		$view->folder("UMP");
        return  $view;
    }

    public function actionEdit(){
		$view = new Render("users/list_users",["body_table"=>"","test1"=>"Val1"]);
		$view->folder("UMP");
		return  $view;
    	//\Debugger::Log('asdasd');\
        #Fill("test me");
        #$timer = new \Profile();
        #
        #Fill(AAAAAA);
        #FillJSON(["success","test","tets1"]);
		//echo(["DUMNEZEU"]);
//		FillJSON(["target"=>[
//								//["type"=>"text","data"=>"hello","index"=>"listUsers"],
//								["type"=>"html_append","data"=>"Adu ce vreau dupa ce pagina este incarcTA HTML .../........","index"=>"testTitle"]
//							]
//				]);

		#Fill(true, "success");
		#Fill(["valueTest","aaa","aaaa","aaaa"], "RAW");
		#Fill("<div>aaaa</div>", "aloo");
		#$model = \Model::Users('');
		#$dao = \DAO::Users('Find',[]);
		//return json_encode(["success"=>true,"RAW"=>["valueTest","aaa","aaaa","aaaa"],"aloo"=>"<div>aaaa</div>"]);
        if($this->params->Method == "POST") {
           # \Debugger::Log($this->params->Args);
        }else{

        }
        //return "asdasd";
//        $view = new Render("users/list_users",["body_table"=>"","test1"=>"Val1"]);
//        return $view;
       # \Debugger::Log($timer->D());
    }
    public function actionDelete(){
        if($this->params->Method == "POST"){
            \Debugger::Log($this->params->Args);
        }

    }
}