<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 5/22/2018
 * Time: 9:00 PM
 */

namespace UMP\System\Core\Controller\Admin;

use UMP\System\Tools\Storage;
use UMP\System\Core\View\Render;

use UMP\System\Core\Controller\ReflectionController;
use UMP\System\Config\Constants;
class CodeController{

    private $params = [];
    private $storage = [];
    private $_listMethods = [];
    public function __construct($params)
    {
        $this->params = $params;
        $this->storage   = new Storage();
    }

    public function actionView(){
       # $timer = new \Profile();
        $classes = \Utils::getDirectory(Constants::SYSTEM_CONTROLLER);
		#\Debugger::Log($this->params);die;
        $viewListItemController = "";

        for($i=0;$i<sizeof($classes);$i++){
            $lastFolder = "";
            $contentClass = \Utils::readFile($classes[$i]);
            if(preg_match_all("/^class\s+(\w+)(\s+|)\{/m", $contentClass,$find)){
                for($j=0;$j<sizeof($find[1]);$j++){
                    if(preg_match("/^namespace\s+(.*?);/m",  $contentClass,$findNameSpace)){
                        $lastFolder = \Utils::lastDir($classes[$i]);
						$objectReflection = new ReflectionController($findNameSpace[1]."\\".$find[1][$j]);
						$this->setListMethods($lastFolder."\\\\".$find[1][$j],$objectReflection->actionView('methods'),$findNameSpace[1]);
					}else{
                        include $classes[$i];
                        $lastFolder = \Utils::lastDir($classes[$i]);
                        $objectReflection = new ReflectionController($find[1][$j]);
                        $this->setListMethods($lastFolder."\\\\".$objectReflection->getClassName(),$objectReflection->actionView('methods'));
                    }
                }
            }
        }

		#timer = new \Profile();
        ksort($this->_listMethods);
        #$timer = new \Profile();
        $view = new Render("code_bubble/classes_controller",["classes"=>$this->_listMethods,"startId"=>0]);
        $viewListItemController .= $view->render();
       # $timer->R();
        $view = new Render("code_bubble/view",["classes_controller"=>$viewListItemController,"test1"=>"Val1"]);
        return $view;
    }
    public function actionViewBodyMethod(){
        if(isset($this->params->Args['Model'])){
            if(isset($this->params->Args['Model']['class']) && isset($this->params->Args['Model']['method'])){
				$this->params->Args['Model']['class'] = str_replace("\\\\","\\",$this->params->Args['Model']['class']);
                $objectReflection = new ReflectionController($this->params->Args['Model']['class']);
                $tryme = $objectReflection->getReflectionMethod($this->params->Args['Model']['method'], true);

                FillJSON(["code"=>$tryme]);
            }
        }
    }
    private function setListMethods($key,$listMethods){
		$cleanMethods = [];
    	for($i=0;$i<sizeof($listMethods);$i++){
			$listClass = [];
			for($j=0;$j<sizeof($listMethods[$i]['list']);$j++){
				$listClass[] = (object)[
							"name"=>$listMethods[$i]['list'][$j]->name,
							"class"=>str_replace("\\","\\\\",$listMethods[$i]['list'][$j]->class)
						];
			}
			$cleanMethods[] = [
				"type"=>$listMethods[$i]['type'],
				"list"=>$listClass
			];
		}
        $this->_listMethods[$key] = $cleanMethods;
    }


}