<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 12/30/2018
 * Time: 12:21 AM
 */

namespace UMP\System\Core\Controller\Admin;
use UMP\System\Core\View\Render;
use UMP\System\Tools\Storage;
use UMP\System\Config\Constants;
class IndexController
{
	private $params = [];
	private $storage = [];
	public function __construct($params){
		$this->params = $params;
		$this->storage = new Storage();
	}

	public function Init($content){
		$timer = new \Timer();
		if ($content instanceof Render) { // if is return view instance echo HTML content
			$index = new Render("index_parser", [
				"front_theme_assets" => Constants::FRONT_THEME_ASSETS,
				"live_url" => Constants::LIVE_URL,
				"page_time" => $timer->FormatTimer(),
				"body" => $content->render(),
				"action" => $this->params->Action,
				"controller" => $this->params->Controller,
				"keyList" => $this->params->KeyList,
				"storage"=>$this->storage
			]);
			$index->folder($content->folder());
			Fill($index->render()); //  HTML content
		} else {
			if (\Utils::JsonTest($content)) { // if response is json will send output JSON
				FillJSON($content);
			} else {
				$index = new Render("index_parser", [
					"front_theme_assets" => Constants::FRONT_THEME_ASSETS,
					"live_url" => Constants::LIVE_URL,
					"page_time" => $timer->FormatTimer(),
					"body" => $content,
					"action" => $this->params->Action,
					"controller" => $this->params->Controller,
					"keyList" => $this->params->KeyList,
					"storage"=>$this->storage
				]);
				Fill($index->render()); //  HTML content
			}
		}
	}
}