<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 7/9/2018
 * Time: 8:11 PM
 */
namespace UMP\System\Core\Controller\Bots;
use UMP\System\Core\View\Render;
use UMP\System\Tools\Storage;
use UMP\System\Config\Constants;
class BotsController extends  Constants
{
	private $storage = "";
	private $params = [];
	private $checkSecound = false;
	private $agents = [];
	private $secoundList = [];
	public function __construct($params)
	{
		$this->storage = new Storage();
		$this->params = $params;
	}
	private function getAgents(){
		$content = \Utils::readFile(self::FRONT_THEME_ASSETS."/javascripts/bots/list.json");
		$this->agents = json_decode($content);
	}
	public function actionStart(){
		$this->getAgents();
		$urlText = "";
		$keyList = "";
		$type = "";
		$ssl = false;
		$secoundUrl = "";
		if(isset($this->params->Args['Model']['url'])){
			$urlText  = $this->params->Args['Model']['url'];
			$keyList  = $this->params->Args['Model']['keylist'];
			$type =  $this->params->Args['Model']['type'];
			$agent = "";
			if($type == "Web"){
				$agent = $this->agents->webbrowser[mt_rand(0,count($this->agents->webbrowser)-1)];
			}
			if($this->checkSsl($urlText)){
				$ssl = true;
			}
			$page = $this->requestData($urlText,($this->checkSecound ? $secoundUrl  : $urlText), $ssl,$agent);
			Fill($page[0]);die;
		}
		$view = new Render("bots/bots_view",[
			"urlText"=>$urlText,
			"keylist"=> $keyList,
			"checkWeb"=>(($type == "Web" ? "checked" : "") || $type == "" ? "checked" : ""),
			"checkPhone"=>($type == "Phone" ? "checked" : "")
		]);
		return $view;
	}
	private function checkSsl($url){
		return (preg_match("/https/",$url,$find));
	}
	private function requestData($url,$secoundPage,$ssl = false,$agent,$keyList ){

		$headers = array();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL            , $secoundPage);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER     , $headers);
		curl_setopt($ch, CURLOPT_REFERER			, "https://google.com/");
		curl_setopt($ch, CURLINFO_HEADER_OUT		, true);
		curl_setopt($ch, CURLOPT_USERAGENT		, $agent);
		if($ssl){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER , true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST ,  2 );
			curl_setopt($ch, CURLOPT_SSLVERSION     , 6);
		}
		curl_setopt($ch, CURLOPT_FRESH_CONNECT  , 1);
		curl_setopt($ch, CURLOPT_FORBID_REUSE   , 1);

		$curl_response = curl_exec($ch);
		$curl_response = htmlentities($curl_response);
		if(preg_match_all("/href\=\"(.*?)\"|href\=\'(.*?)\'/m",$curl_response,$findAll)){
			Fill($findAll);
			if(sizeof($findAll) == 2){
				array_push($this->secoundList,$findAll[1]);
			}
			if(sizeof($findAll) == 3){
				array_push($this->secoundList,$findAll[1]);
				array_push($this->secoundList,$findAll[2]);
			}
		}
		Fill($this->secoundList);die;
		$curl_info = curl_getinfo($ch);
		$this->checkSecound = true;
		return [$curl_response,$secoundPage];
	}
}