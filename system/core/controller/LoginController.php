<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 30/03/2018
 * Time     : 12:49 PM
 */
namespace UMP\System\Core\Controller;
use UMP\System\Core\View\Render;
use UMP\System\I18N\Language;
use UMP\System\Tools\Storage;
use UMP\System\Config\Constants;
use UMP\System\Core\Rules\Validation;
class LoginController
{
    private $storage = "";
    private $params = [];
    public function __construct($params){
        $this->storage = new Storage();
        $this->params = $params;
    }
    private function rules(){
        $rules = new Validation();
        $element = [
            "username"=>["required"=>true],
            "name"=>["required"=>true,"unique"=>true],
            "password"=>["required"=>true,"min"=>5,"regex"=>"\w+"]
        ];
        return $rules->formValidation($element,$this->params->Args['Model']);
    }
    public  function IsLogin()
    {
        return ($check = $this->storage->getSession("Login")) ? (($check == "XYZ") ? true : false) : false;
    }
    public  function actionLogin(){
        $login = false;
        $errors = [];
        $messages = [];
        $status = "";
        $language = new \Language();
        return FillJSON(["message"=>"plm123","error"=>false]);



        if($this->params->Method == "POST"){
            $checkRule = $this->rules();
            if($checkRule['status']){
                $db = new \AdvSql();
                $db->select("userInfo.* ,
                                    userInfo.username as userName,
                                    userGroup.name as groupName,
                                    userGroup.folder as groupFolder,
                                    userPermision.permision_list as permisionList
                                    ")->from("
                                                    `db_test`.`s_user` as userInfo 
                                                    left join `db_test`.`s_group` as userGroup  on userInfo.id_group = userGroup.id
                                                    left join `db_test`.`s_permision` as userPermision on userGroup.id_permision =  userPermision.id ")
                                    ->where("userInfo.username=:username and userInfo.password=:password");

                $db->bind([":username" => $this->params->Args['Model']['username'], ":password" => md5($this->params->Args['Model']['password'])]);
                $result = $db->execute();
                $userData = $result->fetchObject();
                if($userData) {

                    $this->storage->session->set(["name"=>"Username", "value"=>$userData->userName]);
                    $this->storage->session->set(["name"=>"Permision","value"=>$userData->permisionList]);
                    $this->storage->session->set(["name"=>"Folder","value"=>$userData->groupFolder]);
                    $this->storage->session->set(["name"=>"Group","value"=>$userData->groupName]);
                    $messages['success'] = "Login Success";
                    $status  = "success";
                    \Utils::redirect("index",10);
                }else{
                    $status  = "failed";
                }

            }else{
                $status  = "failed";
                $messages['failed'] = $checkRule['Field']." ".$checkRule['Rule'] ;
            }

        }
        $view = new Render("login/login",[ "front_theme_assets"=>Constants::FRONT_THEME_ASSETS,
            "title"=>Constants::APP_NAME,
            "live_url"=>Constants::LIVE_URL,
            "params"=>$this->params,
            "messages"=>$messages,
            "status"=> $status,
            "lang"=>$language,
            "action"=>$this->params->Action,
            "controller"=>$this->params->Controller,
        ]);
        Fill($view->render());
        #$db = new \AdvSql();
        #$db->select("*")
    }
}