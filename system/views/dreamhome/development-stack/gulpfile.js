'use strict';
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var csso = require('gulp-csso');
var argv = require('yargs').argv;
var gulpif = require('gulp-if');
var inquirer = require('inquirer');
var minifyHTML = require('gulp-minify-html');
var browserSync = require('browser-sync');
var del = require('del');
var reload = browserSync.reload;
var runSequence = require('run-sequence');
var requireDir = require('require-dir');
var dir = requireDir('gulp-task');
// variables
var production = !!(argv.production);  
var dev = !!(argv.dev);  
var move = !!(argv.move); 
var app = 'app';
var dist = 'dist';

var src = {
  scss : ['app/style.scss'],  
  scripts:{
  modernizr:['bower_components/modernizr/modernizr.js'
],
  vendor:[
  'bower_components/bower-webfontloader/webfont.js',
  'bower_components/jquery/dist/jquery.js',
  'bower_components/wowjs/dist/wow.js',
  'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js',
  'bower_components/OwlCarousel/owl-carousel/owl.carousel.js',
  'bower_components/isotope/dist/isotope.pkgd.js',
  'bower_components/fitvids/jquery.fitvids.js',
  'bower_components/nouislider/distribute/jquery.nouislider.all.js',
  'bower_components/parsleyjs/dist/parsley.min.js'
  ],
  main:'app/scripts/main.js'
  }
};

gulp.task('styles', function () {
  return gulp.src(src.scss)    
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      outputStyle: 'nested', // libsass doesn't support expanded yet
      precision: 10,
      includePaths: ['.'],
      onError: console.error.bind(console, 'Sass error:')
    }))
    .pipe($.postcss([
      require('autoprefixer-core')({browsers: ['last 2 version']})
    ]))
    .pipe(gulpif(dev,$.sourcemaps.write()))
    .pipe(gulpif(dev,gulp.dest(app+'/')))
    .pipe(gulpif(production,gulp.dest(dist+'/')))
    .pipe(reload({stream: true}))   
    .pipe(gulpif(production,csso()))    
    .pipe(gulpif(production,rename('style.min.css')))
    .pipe(gulpif(production,gulp.dest(dist+'/'))); 
});

gulp.task('images', function () {
  return gulp.src(app+'/images/**/*')
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true,
      // don't remove IDs from SVGs, they are often used
      // as hooks for embedding and styling
      svgoPlugins: [{cleanupIDs: false}]
    })))
    .pipe(gulp.dest(dist+'/images'));
});

// Lint JS
gulp.task('lint', function() {
  return gulp.src(app+'/scripts/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// Main Js
gulp.task('mainScripts', function(){
  return gulp.src(src.scripts.main)
    .pipe(concat('main.js'))    
    .pipe(gulpif(dev,gulp.dest(app+'/js/')))
    .pipe(gulpif(production,gulp.dest(dist+'/js/')))
    .pipe(rename('main.min.js'))
    .pipe(uglify())
    .pipe(gulpif(dev,gulp.dest(app+'/js/')))
    .pipe(gulpif(production,gulp.dest(dist+'/js/')));
});

// Vendor js
gulp.task('vendorScripts', function(){
  return gulp.src(src.scripts.vendor)
    .pipe(concat('vendor.js'))    
    .pipe(gulpif(dev,gulp.dest(app+'/js/vendor/')))
    .pipe(gulpif(production,gulp.dest(dist+'/js/vendor/')))
    .pipe(rename('vendor.min.js'))
    .pipe(uglify())
    .pipe(gulpif(dev,gulp.dest(app+'/js/vendor/')))
    .pipe(gulpif(production,gulp.dest(dist+'/js/vendor/')));
});


// modernizr
gulp.task('modernizr', function(){
  return gulp.src(src.scripts.modernizr)   
    .pipe(uglify())
    .pipe(gulpif(dev,gulp.dest(app+'/js/vendor/')))
    .pipe(gulpif(production,gulp.dest(dist+'/js/vendor/')));
});



// HTML and  others Copy
gulp.task('htmlCopy',function(){
  var opts = {
    conditionals: true,
    spare:true,
    empty :true
  };
 return gulp.src([app+'/*.html'])
 .pipe(gulpif(production,minifyHTML(opts))) 
 .pipe(gulp.dest(dist));
});


gulp.task('extras', function () {
  return gulp.src([
    app+'/*.*',
    app+'/skins/',
    '!'+app+'/*.html',
    '!'+app+'/*.scss'
  ], {
    dot: true
  }).pipe(gulp.dest(dist));
});
gulp.task('extras', function () {
  return gulp.src([
    app+'/*.*',
    '!'+app+'/skins/*.css',
    '!'+app+'/*.html',
    '!'+app+'/*.scss'
  ], {
    dot: true
  }).pipe(gulp.dest(dist));
});
gulp.task('skins-style', function () {
  return gulp.src([    
    app+'/skins/*.css'
  ], {
    dot: true
  }).pipe(gulp.dest(dist+'/skins'));
});
gulp.task('phpScripts', function () {
  return gulp.src([    
    app+'/php/*.php'
  ], {
    dot: true
  }).pipe(gulp.dest(dist+'/php'));
});

gulp.task('fonts', function () {
  return gulp.src([
    'app/fonts/*.*'
  ], {
    dot: true
  }).pipe(gulp.dest('dist/fonts'));
});



gulp.task('serve', ['styles','modernizr','vendorScripts','mainScripts'], function () {
  browserSync({
    notify: false,
    port: 9000,
     server: {
      baseDir: [app],      
      routes: {
        '/bower_components': 'bower_components'
      }
    }
  });

  // watch for changes
  gulp.watch([
    app+'/*.html',
    app+'/scripts/**/*.js',
    app+'/images/**/*',
  ]).on('change', reload);

  gulp.watch(app+'/**/*.scss', ['styles']);
  gulp.watch(app+'/scripts/**/*.js', ['mainScripts']);
});

gulp.task('clean', function(){
  del([
    dist+'/',
    app+'/js',
    app+'/style.css',
    'main/','dist/','main.zip'
    ])
});

gulp.task('build',['modernizr','vendorScripts','mainScripts','styles','images','extras','htmlCopy', 'fonts', 'skins-style', 'phpScripts'],function(){
  return gulp.src(dist+'/**/*').pipe($.size({title: 'build', gzip: true}));
});


gulp.task('live-serve', function () {
  browserSync({
    notify: false,
    port: 8000,
     server: {
      baseDir: [dist+'/'],
    },
    browser: ["google chrome"],   
  });

});



gulp.task('default',['clean'],function(){
console.log('Check readme.md');
});




