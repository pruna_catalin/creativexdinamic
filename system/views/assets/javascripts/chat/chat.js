$(document).ready(function () {
    $('<li class="replies"><img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" /><p>Little robot is here to help you  please type <font color=\'red\'>!help</font> to start  speak with him</p></li>').appendTo($('.messages ul'));
});

function newMessage() {


    message = $(".message-input input").val();
    if($.trim(message) == '') {
        return false;
    }
    $.ajax({
        url: LiveURL + "index.php/chat/message",
        type: 'POST',
        data : "Model[message]="+message,
        dataType:'json',
        headers: {
            'X-Requested-With': 'XMLHttpRequest'
        },
        success : function(data) {
            if (data.success) {
                $('<li class="sent"><img src="http://emilcarlsson.se/assets/mikeross.png" alt="" /><p>' + message + '</p></li>').appendTo($('.messages ul'));
                $('<li class="replies"><img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" /><p>' + data.message + '</p></li>').appendTo($('.messages ul'));
            }
        }
    });

    $('.message-input input').val(null);
    $(".messages").animate({ scrollTop: 1000 }, "fast");
};

$('.submit').click(function() {
    newMessage();
});

$(window).on('keydown', function(e) {
    if (e.which == 13) {
        newMessage();
        return false;
    }
});