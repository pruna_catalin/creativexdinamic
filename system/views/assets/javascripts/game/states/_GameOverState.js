_GameOverState = {
    init: function () {
        alert("You scored: " + score)
    },
    preload: function () { },
    create: function () {
        //  Scaled + Anchor
        var game_over = game.add.sprite(game.width / 2, 100, 'game_over');
        game_over.anchor.set(0.5);
        game_over.scale.set(0.1);
        //  The score made in time
        scoreString = 'Your Score is : ';
        scoreText = game.add.text(game.world.centerX - 100, game.world.centerY + 100, scoreString + score);
        scoreText.fill = "#ffffff";
        scoreText.font = "Luckiest Guy";
        
        playButton = game.add.button(game.width / 2, game.height / 2, "playbutton", this.playTheGame, this);
        
       
        playButton.anchor.set(0.5);
        playButton.scale.set(0.5);
    },
    update: function () { },
    render: function () { },

    playTheGame: function (button) {

        // did we improved our stars in current level?
        if (game.global.starsArray.level[game.global.level] < button.frame) {
            game.global.starsArray.level[game.global.level].star = button.frame;
           
        }
        
        // if we completed a level and next level is locked - and exists - then unlock it
        if (button.frame > 0 && game.global.starsArray.level[game.global.level] == 4 && game.global.level < game.global.starsArray.level.length) {
            game.global.starsArray.level[game.global.level].star = 0;
           
        }
        _LevelSelectState.callBack_Overgame(score,game.global.starsArray.level[game.global.level]);
        game.state.start("LevelSelectState");
    }

}
listObject.push({ "GameOverState": _GameOverState });