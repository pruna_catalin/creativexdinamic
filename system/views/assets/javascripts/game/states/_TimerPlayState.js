_TimerPlayState = {
    init: function () { },
    preload: function () { },
    create: function () {

        // First we are going to create some variables to hold the current state of the timer, 
        // which will allow us to calculate how much time has passed and also set how much time is available. 
        // We are also calling a method that will handle setting up the timer display and creating a loop that will run every 100ms to check the time. 
        // It doesn?t matter how often this update function runs, the time will still be calculated accurately.
        var me = this;

        me.startTime = new Date();
        me.totalTime = 5;
        me.timeElapsed = 0;
        me.createTimer();
        me.gameTimer = game.time.events.loop(100, function () {
            me.updateTimer();
        });

    },
    // are going to define the createTimer function now, which will handle setting up the text label for the timer on the screen.
    createTimer: function () {

        var me = this;

        me.timeLabel = me.game.add.text(me.game.world.centerX, 100, "00:00", { font: "20px Arial", fill: "#fff" });
        me.timeLabel.anchor.setTo(0.5, 0);
        me.timeLabel.align = 'center';

    },
    updateTimer: function () {

        var me = this;

        var currentTime = new Date();
        var timeDifference = me.startTime.getTime() - currentTime.getTime();

        //Time elapsed in seconds
        me.timeElapsed = Math.abs(timeDifference / 1000);

        //Time remaining in seconds
        var timeRemaining = me.totalTime - me.timeElapsed;

        //Convert seconds into minutes and seconds
        var minutes = Math.floor(timeRemaining / 60);
        var seconds = Math.floor(timeRemaining) - (60 * minutes);

        //Display minutes, add a 0 to the start if less than 10
        var result = (minutes < 10) ? "0" + minutes : minutes;
        //Display seconds, add a 0 to the start if less than 10
        result += (seconds < 10) ? ":0" + seconds : ":" + seconds;
        // if counter is finish do the magic
        if (parseFloat(me.timeElapsed) >= parseFloat(me.totalTime)) {
            game.time.events.remove(me.gameTimer);
            return;
        }

        me.timeLabel.text = result;

    
    },

    update: function () {},
    render: function () {},

}
listObject.push({ "TimerPlayState": _TimerPlayState });