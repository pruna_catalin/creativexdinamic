// for music
var hudBtnMute;
var music;
var AUDIO_MUSIC = "music";
_LoadingState = {
    init: function () {
        // going fullscreen
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.setScreenSize = true;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.stage.backgroundColor = "#444";




    },

    preload: function () {

        // preloading varius sounds
        game.load.audio(AUDIO_MUSIC, [LivePathLoader + 'audio/dreamculture.mp3', LivePathLoader + 'audio/dreamculture.ogg']);

        // preloading various spritesheet
        game.load.spritesheet("levels", LivePathLoader + "images/game/levels.png", game.global.thumbWidth, game.global.thumbHeight);
        game.load.spritesheet("level_arrows", LivePathLoader + "images/game/level_arrows.png", 48, 48);
        game.load.spritesheet("game", LivePathLoader + "images/game/game.png", 200, 80);
        game.load.spritesheet(IMAGE_BTN_MUTE, LivePathLoader + 'images/game/sprites/' + IMAGE_BTN_MUTE + '.png', 34, 34);

        // preload MainMenuState images
        game.load.image('gametitle', LivePathLoader + 'images/game/sprites/gametitle.png');
        game.load.image('gridedition', LivePathLoader + 'images/game/sprites/gridedition.png');
        game.load.image('playbutton', LivePathLoader + 'images/game/sprites/playbutton.png');
        game.load.image('menubutton', LivePathLoader + 'images/game/sprites/menubutton.png');
        game.load.image('resetgame', LivePathLoader + 'images/game/sprites/resetgame.png');
        game.load.image('thankyou', LivePathLoader + 'images/game/sprites/thankyou.png');

        // test load image fior a game hog
        game.load.image('hidden_1', LivePathLoader + 'images/game/objects/hidden_1.png');
        game.load.image('hidden_1_a', LivePathLoader + 'images/game/objects/hidden_1_a.png');
        game.load.image('hidden_2', LivePathLoader + 'images/game/objects/hidden_2.png');
        game.load.image('hidden_2_a', LivePathLoader + 'images/game/objects/hidden_2_a.png');
        game.load.image('hidden_3', LivePathLoader + 'images/game/objects/hidden_3.png');
        game.load.image('hidden_3_a', LivePathLoader + 'images/game/objects/hidden_3_a.png');
        game.load.image('hidden_4', LivePathLoader + 'images/game/objects/hidden_4.png');
        game.load.image('hidden_4_a', LivePathLoader + 'images/game/objects/hidden_4_a.png');
        game.load.image('hidden_5', LivePathLoader + 'images/game/objects/hidden_5.png');
        game.load.image('hidden_5_a', LivePathLoader + 'images/game/objects/hidden_5_a.png');
        // preload game over sprite
        game.load.image('game_over', LivePathLoader + 'images/game/sprites/game_over.png');



        // preload bakground
        //game.load.image('background', LivePathLoader + 'images/game/background/bg.png');

        // preloading bar
        label2 = game.add.text(game.world.centerX, game.world.centerY, 'loading...', {
            font: '30px Arial',
            fill: '#fff'
        });
        label2.anchor.setTo(0.5);
        preloading3 = game.add.sprite(game.world.centerX, game.world.centerY + 35, 'loading2');
        preloading3.anchor.setTo(0.5);
        preloading4 = game.add.sprite(game.world.centerX, game.world.centerY + 35, 'loading');
        preloading4.anchor.setTo(0.5);
        game.load.setPreloadSprite(preloading4);
    },
    uppdate: function () {
        game.scale.setShowAll();
        game.scale.refresh();
    },
    create: function () {
        // music
        if (music == null) {
            music = game.add.audio(AUDIO_MUSIC, 1.0, true);
            music.play('', 0, 1, true);
        }
        // going to level select state
        game.state.start("MainMenuState");

        // only for test game gog
        // game.state.start("PlayGameHogState");
        // game.state.start("TimerPlayState");
    },
}


listObject.push({ "LoadingState": _LoadingState });