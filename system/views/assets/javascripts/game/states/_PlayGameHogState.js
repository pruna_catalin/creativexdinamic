var score = 0;
var scoreString = '';
var scoreText;


_PlayGameHogState = {
    init: function () { },
    preload: function () { },
    create: function () {


        
        //  The score
        // scoreString = 'Score : ';
        // scoreText = game.add.text(10, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });

        scoreString = 'Score : ';
        scoreText = game.add.text(10, 10, scoreString + score);
        scoreText.fill = "#ffffff";
        scoreText.font ="Luckiest Guy";


        // hidden_1 = game.add.sprite(game.width / 2, 60, "hidden_1");
        // hidden_1.anchor.set(0.5);
        // hidden_1.scale.set(0.5);

        // hidden_1_a = game.add.sprite(game.width / 2, 120, "hidden_1_a");
        // hidden_1_a.anchor.set(0.5);
        // hidden_1_a.scale.set(0.5);


        // // create a sprite, random frame 0..4
        // hidden_1 = game.add.sprite(0, 0, 'hidden_1');
        // hidden_2 = game.add.sprite(100, 0, 'hidden_2');
        // hidden_1.frame = game.rnd.integerInRange(0,4);
        // hidden_2.frame = game.rnd.integerInRange(0,4);
        // // create a group
        // mysprites = game.add.group(); // <- function call
        // mysprites.add(hidden_1);
        // mysprites.add(hidden_2);
        // // scale entire group and reposition group
        // mysprites.scale.set(0.5);
        // // notice that the sprite position is relative to the group position
        // mysprites.x = 10;
        // mysprites.y = 20;
        var object = ["hidden_1", "hidden_2", "hidden_3", "hidden_3", "hidden_5"];
        var i;
        for (i = 0; i < object.length; i++) {
            var mx = game.width - game.cache.getImage(object[i]).width;
            var my = game.height - game.cache.getImage(object[i]).height;
            var sprite = game.add.sprite(game.rnd.integerInRange(0, mx), game.rnd.integerInRange(0, my), object[i]);
            sprite.scale.set(0.5);
            sprite.inputEnabled = true;
            sprite.input.useHandCursor = true;
            sprite.events.onInputDown.add(scoreSprite, this);
        };

        // First we are going to create some variables to hold the current state of the timer, 
        // which will allow us to calculate how much time has passed and also set how much time is available. 
        // We are also calling a method that will handle setting up the timer display and creating a loop that will run every 100ms to check the time. 
        // It doesn?t matter how often this update function runs, the time will still be calculated accurately.
        var timer = this;
        game.startTime = new Date();
        game.totalTime = 5;
        game.timeElapsed = 0;
        timer.createTimer();
        game.gameTimer = game.time.events.loop(100, function () {
            timer.updateTimer();
        });

    },
    // are going to define the createTimer function now, which will handle setting up the text label for the timer on the screen.
    createTimer: function () {

        // hudBtnMute = game.add.button(game.world.centerX + 140, game.world.centerY - 220, IMAGE_BTN_MUTE, onBtnMute, this, 1, 0, 1);
        // hudBtnMute.anchor.set(0.5);

        // game.timeLabel = game.add.text(game.world.centerX + 110, game.world.centerY - 210, "00:00", { font: "34px Arial", fill: "#fff" });
        // game.timeLabel.anchor.setTo(0.5);

        
        game.timeLabel = game.add.text(game.world.centerX + 110, game.world.centerY - 210, "00:00");
        game.timeLabel.fill = "#ffffff";
        game.timeLabel.font ="Luckiest Guy";
        game.timeLabel.anchor.setTo(0.5);
        //game.timeLabel.align = 'center';

    },
    updateTimer: function () {
        var currentTime = new Date();
        var timeDifference = game.startTime.getTime() - currentTime.getTime();

        //Time elapsed in seconds
        game.timeElapsed = Math.abs(timeDifference / 1000);

        //Time remaining in seconds
        var timeRemaining = game.totalTime - game.timeElapsed;

        //Convert seconds into minutes and seconds
        var minutes = Math.floor(timeRemaining / 60);
        var seconds = Math.floor(timeRemaining) - (60 * minutes);

        //Display minutes, add a 0 to the start if less than 10
        var result = (minutes < 10) ? "0" + minutes : minutes;
        //Display seconds, add a 0 to the start if less than 10
        result += (seconds < 10) ? ":0" + seconds : ":" + seconds;

        // if counter is finish do the magic

        if (score >= 50) {
            game.state.start("GameOverState");

        } else if (parseFloat(game.timeElapsed) >= parseFloat(game.totalTime)) {
            // console.log("Prima conditie: " + score);
            // console.log(game.global.starsArray_collection[game.global.level - 1]);
            game.time.events.remove(game.gameTimer);
            game.state.start("GameOverState");
            //return;
        }

        game.timeLabel.text = result;


    },
    update: function () { },
    render: function () { }
}

listObject.push({ "PlayGameHogState": _PlayGameHogState });