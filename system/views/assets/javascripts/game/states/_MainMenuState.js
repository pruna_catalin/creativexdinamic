var IMAGE_BTN_MUTE = "btnMute";
_MainMenuState = {
    init: function () {
    },
    preload: function () {
    },
    create: function () {
        // sound sprite
        hudBtnMute = game.add.button(game.world.centerX + 140, game.world.centerY - 220, IMAGE_BTN_MUTE, onBtnMute, this, 1, 0, 1);
        hudBtnMute.anchor.set(0.5);
        // rest of the page sprite
        title = game.add.sprite(game.width / 2, 60, "gametitle");
        title.anchor.set(0.5);
        title.scale.set(0.5);
        grid = game.add.sprite(game.width / 2, 130, "gridedition");
        grid.anchor.set(0.5);
        grid.scale.set(0.5);
        playButton = game.add.button(game.width / 2, game.height / 2 + 100, "playbutton", actionOnClick, function () {
        });
        playButton.anchor.set(0.5);
        playButton.scale.set(0.5);
        menuGroup = game.add.group();
        menuButton = game.add.button(game.width / 2, game.height - 30, "menubutton", toggleMenu);
        menuButton.anchor.set(0.5);
        // menuButton.scale.set(0.5);
        menuGroup.add(menuButton);
        resetGame = game.add.button(game.width / 2, game.height + 50, "resetgame", function () {
        });
        resetGame.anchor.set(0.5);
        // resetGame.scale.set(0.5);
        menuGroup.add(resetGame);
        thankYou = game.add.button(game.width / 2, game.height + 130, "thankyou", function () {
        });
        thankYou.anchor.set(0.5);
        // thankYou.scale.set(0.5);
        menuGroup.add(thankYou);
    }
}
listObject.push({ "MainMenuState": _MainMenuState });