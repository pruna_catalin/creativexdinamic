var ControllerInit = [
    {
        "BootState"                     :  ["_BootState"                ,   "game/states/_BootState.js", true]
    },
    {
        "LoadingState"                  :  ["_LoadingState"             ,   "game/states/_LoadingState.js", false]
    },
    {
        "LevelSelectState"              :  ["_LevelSelectState"         ,   "game/states/_LevelSelectState.js", false]
    },
    {
        "PlayLevelState"                :  ["_PlayLevelState"           ,   "game/states/_PlayLevelState.js", false]
    },
    {
        "MainMenuState"                 :  ["_MainMenuState"            ,   "game/states/_MainMenuState.js", false]
    },
    {
        "PlayGameHogState"              :  ["_PlayGameHogState"         ,   "game/states/_PlayGameHogState.js", false]
    },
    {
        "GameOverState"              :  ["_GameOverState"         ,   "game/states/_GameOverState.js", false]
    }
];
