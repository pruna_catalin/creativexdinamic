// mute.play music
function onBtnMute() {
	if (music.isPlaying) {
		music.pause();
	} else {
		music.resume();
	}
}

// for game hog
function scoreSprite (sprite) {
    sprite.destroy();
    //  Increase the score
	score += 10;
    scoreText.text = scoreString + score;
}