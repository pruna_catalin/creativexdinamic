$(document).ready(function(){
    $('.sidebar .panel-collapse').on('hide.bs.collapse', function () {
        $(this).parent().find('.toggle-icon').removeClass('in');
    });
    $('.sidebar .panel-collapse').on('show.bs.collapse', function () {
        $(this).parent().find('.toggle-icon').addClass('in');
    });
    var elemt =  document.getElementsByClassName("contentBubble");
    for(var i=0;i<elemt.length;i++){
        dragElement(elemt[i],i);
    }
});

function dragElement(elmnt,count) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    elmnt.onmousedown = dragMouseDown;
    function dragMouseDown(e) {

        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }
    function elementDrag(e) {
        e = e || window.event;
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";

    }
    function closeDragElement() {
        /* stop moving when mouse button is released:*/
        document.onmouseup = null;
        document.onmousemove = null;
    }
}

function viewMethod(method,classMethod){
    console.log(classMethod.toString());
    $.ajax({
        url: LiveURL + "index.php/code_bubble/viewBodyMethod",
        type: 'POST',
        data : "Model[method]="+method+"&Model[class]=" + classMethod.replace("\\","\\\\"),
        dataType:'json',
        success : function(data) {
           console.log(data);
        }
    });
}