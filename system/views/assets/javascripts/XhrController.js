var custom_tools = true;
var custom_tools_force_output = 1;

if(custom_tools) {
    $(document).ready(function() {
        var prev_hash = '#tab1';
        if(location.hash.indexOf('tab') > -1)
            prev_hash = location.hash;

        location.hash = prev_hash;
        $(window).bind('hashchange', function() {
            console.log(location.hash);
            if(location.hash.indexOf('tab') == -1) {
                location.hash = prev_hash;
            } else {
                prev_hash = location.hash;
            }
        });
    });
}

function JSONParse(input) {
    let result = {data: null, error: false, error_message: ''};
    try {
        result.data = JSON.parse(input);
    } catch (e) {
        result.error = true;
        result.error_message = e;
    }
    return result;
}

function XhrGetCall( url, target_id,status,answer, ctfo) {
    url = url || "";
    target_id = target_id || "";
    ctfo = (typeof ctfo == 'undefined') ? custom_tools_force_output : ctfo;
    _XhrCall("GET", url, "", target_id,status,answer, ctfo);
}
function XhrPostCall(url, data, target_id,status,answer,typeResponse, ctfo) {
    url = url || "";
    target_id = target_id || "";
    ctfo = (typeof ctfo == 'undefined') ? custom_tools_force_output : ctfo;
    _XhrCall("POST", url, data, target_id,status,answer,typeResponse, ctfo);
}

function _XhrCall(_method, _url, _data, _target_id,status,answer,typeResponse, _ctfo) {

    if(custom_tools) {
        var protocol = location.protocol;
        var slashes = protocol.concat("//");
        var host = slashes.concat(window.location.hostname);
        var xhr_call_message = "From '" + host + "' was called '" + _url + "'[" +_method+"] on target '"+ _target_id +"'<br>"+
            "sending the following data: "+ _data + "<br>";
    }
    var cur_time_of_request = Date.now();
    if(custom_tools && status === true) {
        var protocol = location.protocol;
        var slashes = protocol.concat("//");
        var host = slashes.concat(window.location.hostname);
        var elapsed_time_or_request = Date.now() - cur_time_of_request;
        xhr_call_message += ">>>>>>>>>>>>>>>>>>>> REQUEST <<<<<<<<<<<<<<<<<<<<<br>" +
            "Took <font color='cyan'>"+ (elapsed_time_or_request / 1000) +"</font> second(s).<br>" +
            "From '<font color='red'>" + host + "</font>' was called '<font color='red'>" + _url +
            "</font>'[<font color='cyan'>" +_method+"</font>] on target '"+ _target_id +"'<br>"+
            "sending the following data: <font color='yellow'><br>"+ _data + "</font><br>" +
            (_data.length && _method == "GET" ? "<font color='red'>!!! You are using data information in a GET type request !!!</font><br>" : "");

       // / $('#_tools_console').append(xhr_call_message);
    }

    $('#_tools_console').append(xhr_call_message);
    _XhrHandleAnswer(_target_id, answer,typeResponse, _ctfo);
    if(custom_tools){
       var answer_tag = ">>>>>>>>>>>>>>>>>>>>>> END <<<<<<<<<<<<<<<<<<<<<<<br>";
        $('#_tools_console').append(answer_tag);
    }
    if(custom_tools && status === false) {
        var error_tag = "<font color='red'>:: "+status+"</font";
        var error_text = document.createTextNode(answer);
        $('#_tools_console').append(error_tag);
        $('#_tools_console').append(error_text);
    }


}

function _XhrHandleAnswer(target_id, answer, typeResponse, ctfo) {
    if (typeResponse === "application/json") {
        let jsonObj = JSONParse(answer);

        if(!jsonObj.error) {
            answer = jsonObj.data;

            if(answer['use_debugger']) {
                if(answer['debugger'] === undefined) {
                    $('._tools').css('display', 'none');
                    $('#_tools_debugger').html('');
                } else {
                    $('._tools').css('display', 'block');
                    $('#_tools_debugger').append(answer['debugger']);
                    $('#_tools_debugger').append("<br>");
                }
            } else {
                $('._tools').css('display', 'none');
                $('#_tools_debugger').html('');
                $('#_tools_console').html('');
            }

            if (custom_tools && (ctfo == 1 || ctfo == 2)) { // force output to custom console
                if (answer['RAW']) {
                    var raw_tag = "<font color='green'>====================== RAW ======================</font><br>";
                    $('#_tools_console').append(raw_tag);
                    $('#_tools_console').append(document.createTextNode(answer['RAW']));
                    $('#_tools_console').append("<br>");
                }
                if (answer['target'] && answer['target'] instanceof Array) {
                    var target_tag = "<font color='fuchsia'>==================== Targets ====================</font><br>";
                    $('#_tools_console').append(target_tag);
                    answer['target'].forEach(function (key) {
                        var target_tag = "<font color='red'>></font> " + key['index'] + "[" + key['type'] + "] : ";
                        var text_node = document.createTextNode(key['data']);
                        $('#_tools_console').append(target_tag);
                        $('#_tools_console').append(text_node);
                        $('#_tools_console').append("<br>");
                    });
                }

                let answer_tag = "<font color='fuchsia'>=================== Response ====================</font><br>";
                $('#_tools_console').append(answer_tag);

                for (var key in answer) {
                    if (key != 'RAW' &&  key != 'target' && key != 'debugger' && key != 'use_debugger') {
                        var target_tag = "<font color='red'>></font> answer['" + key + "'] : ";
                        var text_node = document.createTextNode(JSON.stringify(answer[key]));

                        $('#_tools_console').append(target_tag);
                        $('#_tools_console').append(text_node);
                        $('#_tools_console').append("<br>");
                    }
                }
            }

            if (ctfo == 0 || ctfo == 1) {
                if (target_id.length) {
                    var var_target = $('#' + target_id);
                    if (var_target.length) {
                        if (answer['RAW']) {
                            _XhrCallAction(var_target, answer['RAW'], 'html');
                        }
                    } else {
                        if(answer['use_debugger']) {
                            var text_node = document.createTextNode("Target node '" + target_id + "' not found!");
                            $('._tools').css('display', 'block');
                            $('#_tools_console').append(text_node);
                        }
                    }
                } else {
                    if (answer['target'] && answer['target'] instanceof Array) {
                        answer['target'].forEach(function (key) {
                            var var_target = $('#' + key['index']);
                            if (var_target.length) {
                                if (key['data']) {
                                    _XhrCallAction(var_target, key['data'], key['type']);
                                }
                            } else {
                                if(answer['use_debugger']) {
                                    var text_node = document.createTextNode("Target node '" + key['index'] + "' not found!");
                                    $('._tools').css('display', 'block');
                                    $('#_tools_console').append(text_node);
                                }
                            }
                        });
                    }
                }
            }
        } else {
            let answer_tag = "<font color='fuchsia'>=================== JSON PARSE ERROR ====================</font><br>";
            let answer_error = "<font color='red'>"+ jsonObj.error_message +"</font><br>";
            let answer_separator = "---------------------------------------------------------";


            $('._tools').css('display', 'block');
            $('#_tools_debugger').append(answer_tag);
            $('#_tools_debugger').append(answer_error);
            $('#_tools_debugger').append(answer_separator);
            $('#_tools_debugger').append("<br>");
            $('#_tools_debugger').append(answer);
            $('#_tools_debugger').append("<br>");
        }
    }
}
/*
    List command JS (append,prepend)
    Type command html,text
 */
function _XhrCallAction($el, data, type) {
    if(type == 'html') $el.html(data);
    else if(type == 'html_append') $el.append(data);
    else if(type == 'html_prepend') $el.prepend(data);
    else if(type == 'text_append') $el.append(document.createTextNode(data));
    else if(type == 'text_prepend') $el.prepend(document.createTextNode(data));
    else $el.html(document.createTextNode(data));
}