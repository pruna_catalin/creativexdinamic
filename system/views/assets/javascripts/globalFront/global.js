$(document).ready(function () {
    $('.open-popup-link').magnificPopup({
        type: 'inline',
        midClick: true,
        mainClass: 'mfp-fade'
    });
    $("#login_front").on("submit", function () {

        var data = $(this).serialize();
        $.ajax({
            url: LiveURL + "index.php/login",
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function (result) {
                if(result.error === true){
                    $("#errorLogin").css("display","block");
                    $("#successLogin").css("display","none");
                    $("#errorLogin").html(result.message);
                }else{
                    $("#errorLogin").css("display","none");
                    $("#successLogin").css("display","block");
                    $("#successLogin").html(result.message);
                }

            }
        });
        return false;
    });


    $('.eventChat').on('show.bs.collapse', function (e) {
        $(".eventChat button").css("width","100%");
        $('#collapsibleIframe').css('height',"100%");

    });
    $('.eventChat').on('hide.bs.collapse', function (e) {
        $(".eventChat button").css("width","");
    });
    $.ajax({
        url: LiveURL + "index.php/chat",
        type: 'GET',
        data : "",
        dataType:'json',
        success : function(data) {
            console.log(data);
            if (data.success) {
                var iframe = document.createElement('iframe');
                var objTo = document.getElementById('collapsibleIframe');
                iframe.style.border = "0";
                iframe.style.height = "300px";
                iframe.style.width = "100%";

                iframe.srcdoc = data.content;
                objTo.appendChild(iframe);


            }
        }
    });
});

function validateForm(form) {
    var formElement = form;
    for (i = 0; i < formElement.length; i++) {
        if (formElement[i].nodeName.toLowerCase() !== "button") {
            if (formElement[i].value === "") {
                toastr.error(formElement[i].attributes['messageerror'].textContent);
                return false;
            } else {
                if (formElement[i].id === "same_password") {
                    if ($("#password").val() !== formElement[i].value) {
                        toastr.error(formElement[i].attributes['messageerror'].textContent);
                        return false;
                    }
                }
                if (formElement[i].nodeName.toLowerCase() === "select" && formElement[i].value === "0") {
                    toastr.error(formElement[i].attributes['messageerror'].textContent);
                    return false;
                }
            }
        }
    }
}