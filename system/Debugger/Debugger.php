<?php
use UMP\System\Debugger\SqlFormatter;
require_once (BASEPATH."/system/Debugger/DebugEmail.php");

	class ErrorLevel {
		const
			EL_NONE = 0,
			EL_NOTICE = 1,
			EL_DEPRECATED = 2,
			EL_PARSE = 3,
			EL_WARNING = 4,
			EL_EXCEPTION = 5,
			EL_ERROR = 6,
			EL_UNKNOWN = 7;
	}

class TException extends \Exception {
    public $TLevel, $TCode, $TMessage, $TFile, $TLine, $TTrace;
    function __construct($errstr, $errcode = 0, $errlevel = ErrorLevel::EL_EXCEPTION, $errtrace = NULL, $errfile = NULL, $errline = NULL) {
        $this->TLevel = $errlevel;
        $this->TCode = $errcode;
        $this->TMessage = $errstr;

        if($errfile == NULL) $this->TFile = $this->file;
        else $this->TFile = $errfile;

        if($errline == NULL) $this->TLine = $this->line;
        else $this->TLine = $errline;

        if($errtrace == NULL) $errtrace = $this->getTrace();

        array_unshift($errtrace,
            array('function'=> '_error_handler',
                'file'=> $this->TFile,
                'line'=> $this->TLine
            )
        );
        $this->TTrace = $errtrace;
    }
}

class Debugger {
    private static $_Logs = array();
    private static $_Timer = null;

    public static $ModeOn = false;
    public static $StackTrace = true;
    public static $OneError = false;
    public static $sendEmail = false ;
    public static $messageEmail = "";
    public static $toEmail = "";
    public static $nameEmail = "";
    public static $subjectEmail = "";
    public static $writeLogs = false;
    public static $Timers = array();

    public static $Profiles = array();

    public static function Log($message, $title = "",$type_message = "") {
        $log_message = "";
        if(is_array($message)) {
            $log_message = print_r($message, true);
        } else if(is_bool($message)) {
            $log_message = $message ? "TRUE" : "FALSE";
        } else if(is_object($message)) {
            $log_message = print_r($message, true)."";
        } else if(is_null($message)) {
            $log_message = "NULL";
        } else {
            if($type_message == "SQL"){
                $log_message = SqlFormatter::format($message,TRUE);
            }else{
                $log_message = $message;
            }

        }

		$log_message = $log_message;
        $caller = debug_backtrace();
        $caller = array_shift($caller);

        $file = ''; $line = '';
        if(!empty($caller)) {
            $file =  $caller['file'];
            $line =  $caller['line'];
        }
        self::log_message(ErrorLevel::EL_NOTICE, 0, self::GetBasePath($file, BASEPATH), $line, $log_message, $title);
    }
    public static function Email($to= "",$name= "",$subject= "",$message= "",$customTitle ="Info Debugger",$type=""){
        self::$sendEmail  = true;
        self::$toEmail = (empty($to)) ?   "prunacatalin.costin@gmail.com" : $to;
        self::$nameEmail = (empty($name)) ?  "Catalin Pruna" :  $name;
        self::$subjectEmail = (empty($subject)) ? "Debugger Email" :  $subject;
        if($type == "SQL"){
            self::Log($message,$customTitle,"SQL");
        }else{
            self::Log($message,$customTitle);
        }
    }
    public static function _exception_handler($ex) {
        if($ex instanceof TException) {
            self::_handler($ex->TLevel, $ex->TCode, $ex->TFile, $ex->TLine, $ex->TMessage, $ex->TTrace);
        } else {
            $trace = $ex->getTrace();
            array_unshift($trace,
                array('function'=> '_error_handler',
                    'file' => $ex->getFile(),
                    'line' => $ex->getLine()
                )
            );
            self::_handler(ErrorLevel::EL_EXCEPTION, $ex->getCode(), $ex->getFile(),
                $ex->getLine(), $ex->getMessage(), $trace);
        }

        if(self::$OneError) die;
    }

    public static function _error_handler($err_code, $err_message, $err_file, $err_line, $err_context) {
        $err_level = ErrorLevel::EL_NONE;
        switch ($err_code) {
            case E_ERROR: $err_level = ErrorLevel::EL_ERROR; break;
            case E_WARNING: $err_level = ErrorLevel::EL_WARNING; break;
            case E_PARSE: $err_level = ErrorLevel::EL_PARSE; break;
            case E_DEPRECATED: $err_level = ErrorLevel::EL_DEPRECATED; break;
            case E_NOTICE: $err_level = ErrorLevel::EL_NOTICE; break;
            default: $err_level = ErrorLevel::EL_UNKNOWN; break;
        }

        $trace = debug_backtrace();
        array_shift($trace);
        self::_exception_handler(new TException($err_message, $err_code, $err_level, $trace, $err_file, $err_line));
        if(self::$OneError) die;
    }

    private static function HuePicker($value, $total) {
        $color = 0x000000;
        if($total == 0) {
            $color |= (0xff << 8);
        } else {
            $perc = $value / $total;

            // 255 -   0 red
            // 255 - 255 yellow
            //   0 - 255 green
            if($perc < 0.5) {
                $color |= (intval(0xff * 2 * $perc) << 16);
                $color |= 0xff << 8;
            } else {
                $color |= 0xff << 16;
                $color |= (intval(0xff * 2 * (1-$perc)) << 8);
            }
        }

        return dechex($color);
    }

		public static function _exit_script_handler() {
			global $Xhr_Flag;

			if (self::$ModeOn) {
				$last_error = error_get_last();
				if(!empty($last_error)) {
					$err_code = $last_error['type'];
					$err_file = $last_error['file'];
					$err_line = $last_error['line'];
					$err_message = $last_error['message'];

					$trace = debug_backtrace();
					array_shift($trace);
					self::_exception_handler(new TException($err_message, $err_code, ErrorLevel::EL_ERROR, $trace, $err_file, $err_line));
				}

				$debug_content = "";
				$has_logs = !empty(self::$_Logs) || !empty(self::$Profiles);
				if($has_logs) {
					$debug_content = sizeof(self::$_Logs) > 0 ? implode("<br>", self::$_Logs) : "";

					foreach (self::$Profiles as $profile) {
						$epoint = $profile->_SMPoint;
						$etimer = $profile->_Timer;

						$total_time = $etimer->TotalTime();
						$debug_content .= ("Profiling started in '" . self::GetBasePath($epoint->_File, BASEPATH) . "' and took <b><font color='lime'>" . round($total_time, 3) . " </font></b>sec. <br>Entry point <font color='yellow'>" . $epoint->_Function . "</font> : <font color='#1E90FF'>" . $epoint->_Line . "</font><br>");

						$measures = $profile->_Measures;
						foreach ($measures as $i => $measure) {
							$hue = self::HuePicker($measure->_Duration, $total_time);
							$debug_content .= ("[" . ($i + 1) . "]");
							if (strlen($measure->_Title)) {
								$debug_content .= ("\t<b><font color='red'>" . $measure->_Title . "</font></b><br>");
							}
							$debug_content .= ("\tIn '" . self::GetBasePath($measure->_File, BASEPATH) . "'<br>\tRecord point <font color='yellow'>" . $measure->_Function . "</font> : <font color='#1E90FF'>" . $measure->_Line . "</font><br>");
							$debug_content .= ("\tRecord point reached after <font color='silver'>" . round($measure->_TotalDuration, 3) . "</font> sec and took <b><font color='#" . $hue . "'>" . round($measure->_Duration, 3) . "</font></b> sec<br>");
						}
						$debug_content .= ("<br>");
					}
					$debug_content .= ("</pre>");


					if ($Xhr_Flag) {
						Fill($debug_content, "debugger");
					}
				}

				if (!$Xhr_Flag) {
					$style = "
				._tools {box-sizing:border-box; width:100%; height:50%; background-color: #111;color:#fff; position:fixed; left:0px; bottom:2%; margin:0px; z-index: 1000; padding:0px; padding-left:20px; padding-right: 10px;box-shadow: 0px 0px 5px #000;border-radius: 10px; border: 1px solid black; display: ".($has_logs?'block':'none')."}
				
				._tools:hover {height:50%; background-color: #000;cursor: pointer;border: 1px solid red}
				
				._tools_menu {box-sizing:border-box;float:left;width:5%}
				._tools_page {box-sizing:border-box;float:left;width:95%;height:100%;padding: 10px 10px 10px 10px}
				._tools_page_tab {display:none; height:100%; overflow:hidden}
				._tools_page_tab:target {display:block; }
				
				._tools_page_pre {box-sizing:border-box;width:100%; height:100%; white-space: pre-wrap; border: 1px solid silver; background-color: #111;color:#fff; margin:0px; padding-left:20px; padding-right: 20px; border-radius: 4px; overflow-y:hidden} 
				._tools_page_pre:hover {cursor: pointer; overflow-y:scroll}";
					Fill("<style>" . $style . "</style>");

					$debug_tools = "
					<div class='_tools'>
						<div class='_tools_menu'>
							<div style='display:table'>
								<div style='display:table-row'><div style='display:table-cell'><a href='#tab1'>Debugger</a></div></div>
								<div style='display:table-row'><div style='display:table-cell'><a href='#tab2'>Console</a></div></div>	
							</div>
						</div>
						<div class='_tools_page'>
							<div id='tab1' class='_tools_page_tab'>
								<pre id='_tools_debugger' class='_tools_page_pre'>" . $debug_content . "</pre>
							</div>
							<div id='tab2' class='_tools_page_tab'>
								<pre id='_tools_console' class='_tools_page_pre'></pre>
							</div>	
						</div>
					</div>";

					Fill($debug_tools);
				}
			}

			Fill(self::$ModeOn, "use_debugger");

			DisplayResponse();
		}

		private static function _handler($_err_level, $_err_code, $_err_file, $_err_line, $_err_message, $_err_trace) {
			if(self::$StackTrace) {
				$inc = 0;
				$message = '';
				foreach($_err_trace as $trace) {
					if(array_key_exists('function', $trace) && array_key_exists('file', $trace)) {
						if($trace['function'] === '_error_handler') {
							self::log_message($_err_level, $_err_code, self::GetBasePath($trace['file'], BASEPATH), $trace['line'], $_err_message);
							continue;
						}

						$file_name = self::GetBasePath($trace['file'], BASEPATH);
						$message .= "[".$inc."] In ". $file_name;
						$message .= " (line <font color='red'>".$trace['line']."</font>)<br>";

						$function_class = empty($trace['class']) ? "" : $trace['class'];
						$function_params_values = $trace['args'];
						$function_params_list = self::function_get_params($trace['function'], $function_class);

						$params = array();
						foreach($function_params_list as $var => $def) {
							$raw_val = array_shift($function_params_values);
							$content = self::function_get_param_value($raw_val);
							$s = true;
							$def = self::function_get_default_param($def);
							array_push($params, ($s? "<a><span title='".$content."'>" : "" )."\$".$var.$def ."".($s? "</span></a>" : "" ));
						}

						if(!empty($trace['type']) && !empty($function_class)) {
							$struct_type = "Undefined";
							if($trace['type'] == '->') $struct_type = "<font color='blue'>object</font>";
							else if($trace['type'] == '::') $struct_type = "<font color='orange'>static</font>";

							$message .= " -> (<font color='orange'>".$struct_type."</font> <font color='yellow'>".
								$trace['class']."</font>)".$trace['type']."<font color='cyan'>".$trace['function']."</font>(<font color='purple'>".implode(", ", $params)."</font>)<br>";
						} else {
							$message .= " -> <font color='#1E90FF'>".$trace['function']."</font>(<font color='purple'>".implode(", ", $params)."</font>)<br>";
						}

						$inc++;
					}
				}

				if(!empty($message)) self::log_message(ErrorLevel::EL_NONE, 0, null, 0, $message);
			} else {
				self::log_message($_err_level, $_err_code, self::GetBasePath($_err_file, BASEPATH), $_err_line, $_err_message);
			}
		}

		private static function log_message($_err_level, $_err_code, $_err_file, $_err_line, $_err_message, $_err_title = "") {
			$log_title = '';
			$log_color = '';
			switch ($_err_level) {
				case ErrorLevel::EL_ERROR: $log_title = 'Error'; $log_color = 'red'; break;
				case ErrorLevel::EL_EXCEPTION: $log_title = 'Exception'; $log_color = '#456496'; break;
				case ErrorLevel::EL_WARNING: $log_title = 'Warning'; $log_color = 'yellow'; break;
				case ErrorLevel::EL_PARSE: $log_title = 'Parse'; $log_color = 'orange'; break;
				case ErrorLevel::EL_DEPRECATED: $log_title = 'Deprecated'; $log_color = 'purple'; break;
				case ErrorLevel::EL_NOTICE: $log_title = 'Notice'; $log_color = 'cyan'; break;
				case ErrorLevel::EL_UNKNOWN: $log_title = 'Unknown'; $log_color = 'maroon'; break;
			}

			$logged = "";

			if(!empty($log_title)) {
				$logged .= "<b><font color='".$log_color."'>";
				$logged .= "(".$log_title.") ";
				$logged .= $_err_title . "</font>";
				if($_err_level != ErrorLevel::EL_NOTICE)
					$logged .= "(Code ".$_err_code.")";
				$logged .= "</b><br>";
			} else {
				if(!empty($_err_title))
					$logged .= "<b><font color='".$log_color."'>". $_err_title . "</font></b><br>";
			}

			if(!empty($_err_file)) {
				$logged .= "In\t". $_err_file . "(line <font color='red'>".$_err_line."</font>)<br>";
			}

			if(!empty($_err_message)) {
				if($_err_level == ErrorLevel::EL_NONE)
					$logged .= $_err_message;
				else if($_err_level == ErrorLevel::EL_NOTICE)
					$logged .= "<font color='#778768'>".$_err_message."</font><br>";
				else
					$logged .= "Msg\t<font color='#825600'>".$_err_message."</font><br>";
			}

			if(!empty($logged)) {
				array_push(self::$_Logs, $logged);
			}
		}

		private static function function_get_params($funcName, $className = ""){
			$attribute_names = [];

			if(empty($className)) {
				if(function_exists($funcName)){
					$fx = new \ReflectionFunction($funcName);
					foreach ($fx->getParameters() as $param){
						$attribute_names[$param->name] = NULL;
						if ($param->isOptional()){
							if($fx->isInternal())
								$attribute_names[$param->name] = "[Optional]";
							else
								$attribute_names[$param->name] = $param->getDefaultValue();
						}
					}
				}
			} else {
				if(method_exists($className, $funcName)){
					$fx = new \ReflectionMethod($className, $funcName);

					foreach ($fx->getParameters() as $param){
						$attribute_names[$param->name] = NULL;
						if ($param->isOptional()){
							if($fx->isInternal())
								$attribute_names[$param->name] = "[Optional]";
							else
								$attribute_names[$param->name] = $param->getDefaultValue();
						}
					}
				}
			}

			return $attribute_names;
		}

		private static function function_get_default_param($mixed) {
			if(empty($mixed)) return "";
			$result = "=";
			if(is_string($mixed) || is_numeric($mixed)) { $result .= $mixed; }
			else if(is_bool($mixed)) { $result .= $mixed ? "true":"false"; }
			else if(is_array($mixed)) { $result .= "array()"; }
			else $result .= gettype($mixed);
			return $result;
		}

		private static function function_get_param_value($mixed) {
			$result = "";
			if(is_bool($mixed)) { $result .= $mixed ? "true":"false"; }
			else $result .= htmlentities(print_r($mixed, true));
			return $result;
		}

		private static function GetBasePath($path, $root) {
			if(!is_dir($root) || $path == '/') return $path;

			$result = array_filter(explode("/", str_replace("\\", "/", $path)),
				function ($_) { return !empty($_); });
			$base = array_filter(explode("/", str_replace("\\", "/", realpath($root))),
				function ($_) { return !empty($_); });
			$result_idx = array_values($result);
			$base_idx = array_values($base);

			while(sizeof($result_idx) > 0 && sizeof($base_idx) > 0) {
				if($result_idx[0] == $base_idx[0]) { array_shift($result_idx); array_shift($base_idx); }
				else break;
			}
			return (sizeof($result_idx) > 0 ? "" : "./") . implode("/", $result_idx);
		}
	}
