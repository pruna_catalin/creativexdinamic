<?php
/**
 * Created by Pruna Catalin.
 * Platform : Creative-FrameWork
 * Users: CreativeX && noValue
 * Date: 9/29/2017
 * Time: 1:05 PM
 */

class Timer {
    private $_StartTime = 0, $_CurrentTime = 0, $_PreviousTime = 0;

    function __construct() {
        $this->_StartTime = $this->_CurrentTime = $this->_PreviousTime = microtime(true);
    }

    function Elapsed() {
        $dt = $this->_CurrentTime - $this->_PreviousTime;
        $this->_PreviousTime = $this->_CurrentTime;
        return $dt;
    }
    function FormatTimer(){
        return round($this->Measure(), 4);
    }
    function Measure() {
        $this->_CurrentTime = microtime(true);
        return $this->_CurrentTime - $this->_StartTime;
    }
    function TotalTime() { return $this->_CurrentTime - $this->_StartTime; }
}

class measure_point {
    public $_Title = "", $_Function = "", $_File = "",
        $_Line = 0, $_Duration = 0, $_TotalDuration = 0;
}

class Profile {
    public $_SMPoint = null;
    public $_Timer = null;

    public $_Measures = array();

    function __construct() {
        $this->_SMPoint = self::CreateMeasurePoint();
        $this->_Timer = new Timer();

        array_push(Debugger::$Profiles, $this);
    }

    function R($title = "") {
        $MPoint = self::CreateMeasurePoint();
        $MPoint->_TotalDuration = $this->_Timer->Measure();
        $MPoint->_Duration = $this->_Timer->Elapsed();
        $MPoint->_Title = $title;
        array_push($this->_Measures, $MPoint);
    }

    private static function CreateMeasurePoint() {
        $MPoint = new measure_point();

        $dbg = debug_backtrace();
        array_shift($dbg);
        $call = array_shift($dbg);

        $MPoint->_File = $call['file'];
        $MPoint->_Line = $call['line'];

        $dbg = array_shift($dbg);
        $fct = "";
        if(!empty($dbg['class']) && !empty($dbg['type'])) {
            $fct .= "(";
            if($dbg['type'] == '::') $fct .= "static ";
            if($dbg['type'] == '->') $fct .= "object ";
            $fct .= $dbg['class'];
            $fct .= ")";
            $fct .= $dbg['type'];
        }
        $fct .= $dbg['function'];

        $MPoint->_Function = $fct;

        return $MPoint;
    }
}
