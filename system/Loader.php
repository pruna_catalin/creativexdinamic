<?php
/**
 * Created by PhpStorm.
 * User     : catalin.pruna
 * Contact  : prunacatalin.costin@gmail.com
 * Skype    : prunacatalin.costin@gmail.com
 * Date     : 13/02/2018
 * Time     : 1:37 PM
 */
define("_NAMESPACE_","UMP\\System\\");
define("_EXCEPTIONS_API_","ServerApi\\");
use UMP\System\Core\Controller\Controller;

$Global_Namespace = [
    'Debugger'      =>   'UMP\System\Debugger\Debugger',
    'Profile'       =>   'UMP\System\Debugger\Profile',
    'Timer'         =>   'UMP\System\Debugger\Profile',
    'SQLFormater'   =>   'UMP\System\Debugger\SqlFormatter',
    'Utils'         =>   'UMP\System\Tools\Utils',
    'Constants'     =>   'UMP\System\Config\Constants',
    'AdvSql'        =>   'UMP\System\Core\Driver\AdvSql',
    'Model'         =>   'UMP\System\Core\Model\Model',
    'DAO'           =>   'UMP\System\Core\Model\DAO\DAO',
    'Routes'        =>   'UMP\System\Core\Routes\Routes',
    'Parser'        =>   'UMP\System\Core\View\Parser',
    'Language'      =>   'UMP\System\I18N\Language',
    'Mapping'      =>    'UMP\Server_Api\Mapping'
];
$Global_Raw_Buffer = "";
$Global_Xhr_Buffer = array();

$Xhr_Flag = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

function Fill($input, $section = "") {
    global $Global_Raw_Buffer, $Global_Xhr_Buffer, $Xhr_Flag;

    if($Xhr_Flag) {
        if(empty($section)) {
                $Global_Raw_Buffer .= print_r($input, true);
        } else {
            if(array_key_exists($section, $Global_Xhr_Buffer)) {
                if(is_array($input) && is_array($Global_Xhr_Buffer[$section])) {
                    $Global_Xhr_Buffer[$section] = array_merge($Global_Xhr_Buffer[$section], $input);
                } else {
                    if(!is_string($Global_Xhr_Buffer[$section])) {
                        $Global_Xhr_Buffer[$section] = print_r($Global_Xhr_Buffer[$section], true);
                    }
                    $Global_Xhr_Buffer[$section] .= print_r($input, true);
                }
            } else {
                $Global_Xhr_Buffer[$section] = $input;
            }
        }
    } else {
    	if($section == 'use_debugger') return;

    	$Global_Raw_Buffer .= print_r($input, true);
    }
}

function FillJSON($input) {

	if(is_array($input)) {
	    if(isset($input['params'])){
            \Debugger::Log($input['params']);
        }else{
            foreach($input as $key => $item) {
                Fill($item, $key);
            }
        }
	}
}

function DisplayResponse() {
    global $Global_Raw_Buffer, $Global_Xhr_Buffer, $Xhr_Flag;
    if($Xhr_Flag ) {
    	$Global_Xhr_Buffer['RAW'] = $Global_Raw_Buffer;
        header('Content-Type: application/json');
    	echo json_encode($Global_Xhr_Buffer);
    } else {
        echo $Global_Raw_Buffer;
    }
}
class Loader
{
    public static function register($use_controller = true)
    {

        chdir(BASEPATH);
        spl_autoload_register(array(__CLASS__, '_autoload'));
        \Debugger::$ModeOn = true; // start Debugger
        #\Debugger::$OneError = true; // stop at first error find :)
        set_exception_handler(array("\Debugger", "_exception_handler"));
        set_error_handler(array("\Debugger", '_error_handler'));
        register_shutdown_function(array("\Debugger", '_exit_script_handler'));
        if($use_controller){
        	$objectController = new Controller();
			$objectController->callController();
		} // start automatic load Controller
        #$profile = new \Profile();

        #$profile->R("Page loading time");
    }

    private static function _autoload($classPath)
    {
        global $Global_Namespace;

        if(array_key_exists($classPath, $Global_Namespace)) {
            $class = str_replace( 'UMP\\', '', $Global_Namespace[$classPath]);
            $classToPath = str_replace('\\', '/', $class);
            $cleanURl  = str_replace('\\', '/',BASEPATH);
            $className = $Global_Namespace[$classPath];
            if(preg_match("/(.*)(\/[^\/]+$)/",$classToPath,$find)){
                $className = "/".strtolower($find[1])."/".str_replace("/","",$find[2]);
            }

			if(strpos($class,_EXCEPTIONS_API_) === 1) {
				$cleanURl = str_replace("../", "", $cleanURl);

			}
            if (!self::require_file($cleanURl .$className . '.php')) {
            	\Debugger::Log("CleanURl : ".strpos($class,_EXCEPTIONS_API_). " className: ".$className);
            	\Debugger::Log("Global Class " . $cleanURl . $className . " not found!");
            }
        }else{
            if (strpos($classPath, _NAMESPACE_) === 0) {
                $class = str_replace( 'UMP\\', '', $classPath);
                $classToPath = str_replace('\\', '/', $class);
                $className = $classToPath;
                $cleanURl  = str_replace('\\', '/',BASEPATH);
                if(preg_match("/(.*)(\/[^\/]+$)/",$classToPath,$find)){
                    $className = "/".strtolower($find[1])."/".str_replace("/","",$find[2]);
                }


                if (!self::require_file($cleanURl . $className . '.php')) {
                    \Debugger::Log("Class " . $cleanURl.$className . " not found!");
                }
            }
        }

    }

    private static function require_file($path)
    {
        $filePath = "";
        if (is_array($path)) {
            $filePath = implode(DIRECTORY_SEPARATOR, $path);
        } else
            $filePath = $path;
        if (file_exists($filePath)) {
            require_once $filePath;
            return true;
        } else {
            return false;
        }
    }

}
