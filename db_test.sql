-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2018 at 03:49 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `s_group`
--

CREATE TABLE `s_group` (
  `id` int(11) NOT NULL,
  `id_permision` int(255) NOT NULL COMMENT 'FK ->id  from s_permision',
  `name` varchar(255) NOT NULL COMMENT 'name of group ',
  `folder` varchar(255) DEFAULT NULL COMMENT 'name folder',
  `create_by` int(255) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(255) NOT NULL,
  `modified_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `s_group`
--

INSERT INTO `s_group` (`id`, `id_permision`, `name`, `folder`, `create_by`, `create_at`, `modified_by`, `modified_at`) VALUES
(1, 1, 'testgroup', 'administrator', 1, '2018-04-26 10:53:45', 1, '2018-04-26 10:53:45');

-- --------------------------------------------------------

--
-- Table structure for table `s_permision`
--

CREATE TABLE `s_permision` (
  `id` int(255) NOT NULL,
  `permision_list` text NOT NULL,
  `create_by` int(255) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(255) NOT NULL,
  `modified_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `s_permision`
--

INSERT INTO `s_permision` (`id`, `permision_list`, `create_by`, `create_at`, `modified_by`, `modified_at`) VALUES
(1, '{}', 1, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `s_profile`
--

CREATE TABLE `s_profile` (
  `id_user` int(11) NOT NULL COMMENT 'fk->id s_user'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `s_profile`
--

INSERT INTO `s_profile` (`id_user`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Table structure for table `s_user`
--

CREATE TABLE `s_user` (
  `id` int(11) NOT NULL,
  `id_group` int(11) NOT NULL COMMENT 'fk->id s_group',
  `id_profile` int(11) NOT NULL COMMENT 'fk->id s_profile',
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `reload_permisions` int(1) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `s_user`
--

INSERT INTO `s_user` (`id`, `id_group`, `id_profile`, `username`, `password`, `email`, `active`, `reload_permisions`, `create_by`, `create_at`, `modified_by`, `modified_at`) VALUES
(1, 1, 1, 'test', '5f4dcc3b5aa765d61d8327deb882cf99', 'test@yahoo.com', 0, 0, 1, '2018-03-13 00:00:00', 1, '2018-03-16 00:00:00'),
(2, 1, 2, 'test2', '5f4dcc3b5aa765d61d8327deb882cf99', 'test2@yahoo.com', 0, 0, 1, '2018-03-13 00:00:00', 1, '2018-03-16 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `s_group`
--
ALTER TABLE `s_group`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_permision` (`id_permision`) USING BTREE;

--
-- Indexes for table `s_permision`
--
ALTER TABLE `s_permision`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `s_profile`
--
ALTER TABLE `s_profile`
  ADD PRIMARY KEY (`id_user`) USING BTREE;

--
-- Indexes for table `s_user`
--
ALTER TABLE `s_user`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `id_group` (`id_group`) USING BTREE,
  ADD KEY `fk_profile` (`id_profile`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `s_group`
--
ALTER TABLE `s_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `s_permision`
--
ALTER TABLE `s_permision`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `s_user`
--
ALTER TABLE `s_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
