<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 12/6/2018
 * Time: 7:33 PM
 */
class Api{

	private $sandbox_url                    = "protected/permision/";

	//Live Configuration
	public $live_url                       = "protected/permision/";

	//Payment method Params
	private $url                            = "";
	public 	$group 				 = "admin";
	public 	$folder 		     = "admin";
	public 	$route 				 = "";
	public  $post_fields         = [];
	public  $headers             = [];
	public  $user_agent          = "";
	public  $params         	 = [];
	public function __construct($params,$sandbox = false){
		$this->params					= $params;
		$this->group               		= (isset($params['permision']['value'])) ? $params['permision']['value'] : $this->group;
		$this->folder               	= (isset($params['params']['Folder'])) ? $params['params']['Folder'] : $this->folder;
		$this->route               		= (isset($params['params']['Case'])) ? $params['params']['Case'] : $this->route;
		$this->url                      = ($sandbox) ? $this->sandbox_url.$this->group.".xml"       : $this->live_url.$this->group.".xml";
	}
	public function call(){
		if(file_exists($this->url)){
			$client = simplexml_load_string(file_get_contents($this->url) , 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOBLANKS);
			$result = $this->checkPermision($client);
			echo json_encode($result);
		}else{
			echo json_encode(["message"=>"Not route found","error"=>true,"route"=>""]);
		}
	}
	private function checkPermision($routes){
		$result = ["message"=>"Not route found","error"=>true,"route"=>""];
		foreach($routes as $route){
			if($route->hash == $this->route){
				if($route->folder == $this->folder){
					$result = ["message"=>"Passed","error"=>false,"route"=>$route];
				}else{
					$result = ["message"=>"Block Folder","error"=>true,"route"=>""];
				}
			}
		}
		return $result;
	}
}

$test = new Api($_POST);
$test->call();
