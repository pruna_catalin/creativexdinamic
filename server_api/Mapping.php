<?php
/**
 * Created by PhpStorm.
 * User: Catalin
 * Date: 12/28/2018
 * Time: 10:49 PM
 */

use UMP\System\Core\Routes\Routes;
require_once(dirname(__FILE__) . "/../system/config/routes.php");
class Mapping{
	private $route = [];
	public function __construct(){
		$this->route = Routes::getInstance()->registerRoute();
	}
	private function CreateListFromFile(){
		$path =  dirname(__FILE__)."/protected/permision/";
		$files = \Utils::getDirectory($path,"\w+",["xml"]);
		$content = ["file"=>""];
		foreach($files as  $file){
			$filename = pathinfo($file, PATHINFO_FILENAME);
			$content['file'] = $file;
			$content[$filename] = simplexml_load_string(\Utils::readFile($file),'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOBLANKS);
		}
		return $content;
	}
	public function EditRoute($group){
		$createListFromFile = $this->CreateListFromFile();
		$createListFromRoute = $this->route;
		$list = [];
		$fileList = [];
		foreach($createListFromRoute->NoSecurity as $route) {
			$convertToArray = json_encode($createListFromFile[$group]);
			$json = json_decode($convertToArray);
			if ($route['security']) {
				foreach ($json->route as $routeFile) { // 2 * 3  =  6 plm ...
					if ($routeFile->hash == $route['route']) {
						$fileList[] = $routeFile->hash;
						if ($route['method'] == "GET") {
							if ($routeFile->request_get) {
								$list['GET'][$routeFile->hash] = ["route" => $routeFile->hash, "controller" => $routeFile->controller, "action" => $routeFile->action, "folder" => $routeFile->folder, "method" => "GET", "type" => $routeFile->method, "checked" => true];
							} else {
								$list['GET'][$routeFile->hash] = ["route" => $routeFile->hash, "controller" => $routeFile->controller, "action" => $routeFile->action, "folder" => $routeFile->folder, "method" => "GET", "type" => $routeFile->method, "checked" => false];
							}
						}
						if ($route['method'] == "POST") {
							if ($routeFile->request_post) {
								$list['POST'][$routeFile->hash] = ["route" => $routeFile->hash, "controller" => $routeFile->controller, "action" => $routeFile->action, "folder" => $routeFile->folder, "method" => "POST", "type" => $routeFile->method, "checked" => true];
							} else {
								$list['POST'][$routeFile->hash] = ["route" => $routeFile->hash, "controller" => $routeFile->controller, "action" => $routeFile->action, "folder" => $routeFile->folder, "method" => "POST", "type" => $routeFile->method, "checked" => false];
							}
						}
					}
				}
				if (!in_array($route['route'], $fileList)) {
					$items = explode("@", $route['methods']);
					if ($route['method'] == "GET") {
						$list['GET'][$route['route']] = ["route" => $route['route'], "controller" => $items[0], "action" => $items[0], "folder" => $route['folder'], "method" => "GET", "type" => ($route['ajax']) ? "XHR" : "normal", "checked" => false];
					} else {
						$list['POST'][$route['route']] = ["route" => $route['route'], "controller" => $items[0], "action" => $items[0], "folder" => $route['folder'], "method" => "GET", "type" => ($route['ajax']) ? "XHR" : "normal", "checked" => false];
					}
				}
			}
		}
		return $list;
	}

	public function CreateRoute($group){

	}


}
